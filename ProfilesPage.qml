/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8


ProfilesPageForm {
    id: profilesPageForm

    buttonLoad.onClicked: {
        console.log("button Load was clicked")
        confirmLoadSaveDialog.beginLoad()
    }
    buttonSave.onClicked: {
        console.log("button Save was clicked")
        confirmLoadSaveDialog.defaultProfile = comboBoxProfileNumber.currentText
        confirmLoadSaveDialog.beginSave(7)
    }

    //If new UI items for profile settings are added,
    //their signals should be handled here and delegated to profileData c++ object.

    comboBoxCameraName.onActivated: {
        profileData.onCameraChanged(index)
    }
    comboBoxProfileNumber.onActivated: {
        profileData.onProfileChanged(index)
    }

    comboBoxAEProgram.onActivated: {
        profileData.onItemChanged("AEProgram", index)
    }
    comboBoxAv.onActivated: {
        profileData.onItemChanged("Av", index)
    }
    comboBoxCableMethod.onActivated: {
        profileData.onItemChanged("CableMethod", index)
    }
    comboBoxControlMode.onActivated: {
        profileData.onItemChanged("ControlMode", index)
    }
    comboBoxFocus.onActivated: {
        profileData.onItemChanged("Focus", index)
    }
    comboBoxIsoSpeed.onActivated: {
        profileData.onItemChanged("IsoSpeed", index)
    }
    comboBoxQuality.onActivated: {
        profileData.onItemChanged("Quality", index)
    }
    comboBoxShootAfter.onActivated: {
        profileData.onItemChanged("ShootAfter", index)
    }
    comboBoxShootBefore.onActivated: {
        profileData.onItemChanged("ShootBefore", index)
    }
    comboBoxShootCnt.onActivated: {
        profileData.onItemChanged("ShootCnt", index)
    }
    comboBoxSize.onActivated: {
        profileData.onItemChanged("Size", index)
    }
    comboBoxTv.onActivated: {
        profileData.onItemChanged("Tv", index)
    }
    comboBoxWhiteBalance.onActivated: {
        profileData.onItemChanged("WhiteBalance", index)
    }
    comboBoxZoom.onActivated: {
        profileData.onItemChanged("Zoom", index)
    }

}
