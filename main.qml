/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1

import "dialogs"
import "components"

ApplicationWindow {
    id: appWindow
    visible: true
    property int pageMargin: 6
    font: Qt.application.font
    // Startup size seems to take effect only on desktop environments. 900 and 1024 are two standard screen heights.
    // Height 940 seems to be enough in Win and Lin to show all (profile) options at the same time.
    // Screenheight-50 is hopefully compact enough in smaller screens to leave room for title bar, panel etc.
    // (desktopAvailableHeight didn't work reliably on a desktop).
    width: tools.getSettingInt("windowwidth", 640)
    height: tools.getSettingInt("windowheight", Screen.height < 901 ? Screen.desktopAvailableHeight - 50 : 940)
    title: tools.getSettingString("appname", "CAMremoteControl");
    Material.theme: tools.getSettingBool("dark", false) ? Material.Dark : Material.Light
    Universal.theme: tools.getSettingBool("dark", false) ? Universal.Dark : Universal.Light

    function prepareForClosing() {
        console.log("preparing for closing")
        tools.setSettingString("startuppage",tabBar.currentItem.text)
        tools.setSettingInt("windowwidth", appWindow.width)
        tools.setSettingInt("windowheight", appWindow.height)
        profileData.saveToSettings()
    }

    Screen.onPrimaryOrientationChanged: {
        // Window height&width aren't updated automatically when the device is flipped so we have to do it here.
        // Just using current desktopAvailable geometry at the moment of calling this handler
        // doesn't work, they give wrong numbers. Binding works. We do it here so that there's no binding before the first orientation change.
        // Also, there should be no need to create binding on windowed desktop computers. On Android the window is always Maximized.
        if (appWindow.visibility === Window.Maximized) {
            appWindow.height = Qt.binding(function() {return (appWindow.visibility === Window.Maximized)? Screen.desktopAvailableHeight: appWindow.height})
            appWindow.width = Qt.binding(function() {return (appWindow.visibility === Window.Maximized)? Screen.desktopAvailableWidth: appWindow.width})
        }

    }

    onVisibilityChanged: {
        console.log("Application window visibility changed: ", visibility)
        // If the app exchanges data with CAMremote, it may be good to pause it when the window isn't visible.
        // See also qApplication.onApplicationStateChanged
        switch(visibility) {
        case Window.Windowed: //2
        case Window.Minimized: //3, Save power, stop getting data from CAMremote if applicable
        case Window.Maximized: //4
        case Window.FullScreen: //5
        case Window.Hidden: //0, Save power, stop getting data from CAMremote if applicable
        case Window.AutomaticVisibility: //1
            break
        }
    }
    Connections {
        target: qApplication
        onApplicationStateChanged: {
            console.log("QApplication appstate changed: ", state)
            // TODO: See appWindow visibility above.
            switch(state) {
            case Qt.ApplicationHidden://1, shouldn't happen in this app
            case Qt.ApplicationSuspended: //0
                // happens only on mobile when app is put to background (phase 2)
                appWindow.prepareForClosing()
                break
            case Qt.ApplicationInactive: //2, usual
                // happens on mobile when app is put to background (phase 1)
                break
            case Qt.ApplicationActive: //4, normal active state
                break
            }
        }
    }
    Connections {
        target: qApplication
        onAboutToQuit: {
            console.log("application is about to quit")
            prepareForClosing()
        }
    }

    Component.onCompleted: {
        console.log("app window completed")
        // Temporary objects to help adding pages dynamically. Add new pages to this object.
        // The object name is the UI string for the page, also used in settings.
        var pagesList = [
                    {
                        text:"Camera control",
                        icon: "qrc:/icons/ic_photo_camera_black_24dp.png",
                        pagename: "CameraControlPage.qml"
                    },
                    {
                        text: "Camera profiles",
                        icon: "qrc:/icons/ic_build_black_24dp.png",
                        pagename: "ProfilesPage.qml"
                    },
                    {
                        text: "Monitor RC channels",
                        icon: "qrc:/icons/ic_settings_input_antenna_black_24dp.png",
                        pagename: "MonitoringPage.qml"
                    },
                    {
                        text: "Timer",
                        icon: "qrc:/icons/ic_timer_black_24dp.png",
                        pagename: "TimerPage.qml"
                    },
                    {
                        text: "Settings",
                        icon: "qrc:/icons/ic_phonelink_setup_black_24dp.png",
                        pagename: "ApplicationSettingsPage.qml"
                    },
                    {
                        text:"About",
                        icon: "qrc:/icons/ic_info_black_24dp.png",
                        pagename: "InfoPage.qml"
                    }
                ]


        // Hidden tabs, can be set in default or user settings
        var hiddenTabNames = tools.getSettingStringList("hiddentabs", [], true)
        // Available tabs, customized for specific releases in read only customizations
        // (if not set there, UI has no pages at all!). Order is set there, too.
        var availableTabNames = tools.getSettingStringList("availabletabs", [], true)
        tools.setSettingStringList("hiddentabs", hiddenTabNames)

        // Go through the list of available pages and add to the tab bar and to the main view those pages
        // which are not hidden
        var pageComponent
        for (var i in availableTabNames){
            var ix = availableTabNames[i]
            for (var tabNumber in pagesList) {
                if ( (pagesList[tabNumber].text === ix) && hiddenTabNames.indexOf(ix) < 0){
                    console.log("adding ", pagesList[tabNumber].text)
                    tabBar.model.append(pagesList[tabNumber])
                    pageComponent = Qt.createComponent(pagesList[tabNumber].pagename)
                    pageComponent.createObject(mainPagesStackLayout)
                }

            }
        }
        console.log("Add tabs:")
        // Start with the tab page which was saved
        tabBar.currentIndex = 0 // default to the first one (if not set, none is selected)
        var startupPage = tools.getSettingString("startuppage", "");
        for (var j = 0; j < tabBar.model.count; j++) {
            if (tabBar.model.get(j)["text"] === startupPage) {
                console.log("found startup page ", j, startupPage)
                tabBar.currentIndex = j
            }
        }
        tabBar.currentItem.toggle()

        if (tools.isFirstTime()) {
            //setupWizard.visible = true
        }
        console.log("app window completed END")
    }


    GridLayout {
        id: mainLayout
        anchors.fill: parent
        flow: appWindow.height >= 400 && appWindow.width > appWindow.height && appWindow.width >= 790 ? GridLayout.LeftToRight : GridLayout.TopToBottom

        GridLayout {
            id: selectorLayout
            flow: mainLayout.flow === GridLayout.TopToBottom ? GridLayout.LeftToRight : GridLayout.TopToBottom
            Layout.fillWidth: flow === GridLayout.LeftToRight ? true : false
            Layout.alignment: Qt.AlignTop//flow === GridLayout.LeftToRight ? Qt.AlignTop : Qt.AlignLeft

            // Tab bar has indicators for main pages; populated in ApplicationWindow onCompleted
            TabBarRelocatable {

                id: tabBar
                Layout.alignment: Qt.AlignTop
                Layout.maximumWidth: ApplicationWindow.window.width -6
                Layout.maximumHeight: ApplicationWindow.window.height
                orientation: selectorLayout.flow === GridLayout.LeftToRight ? ListView.Horizontal : ListView.Vertical

                onCurrentIndexChanged: {
                    console.log("selector bar new index", currentIndex)
                }
            }

            Item {
                id: fillerBetweenTabsAndMenu
                visible: selectorLayout.flow === GridLayout.TopToBottom ? false : true
                Layout.fillWidth: true
                // this can be used for debugging
                //anchors.left: parent.left
                //z: 1
                //Row {
                //Label {
                //    id: debugLabel1
                //    text: ""
                //}
                //Label {
                //    id: debugLabel2
                //    text: ""
                //}
                //}
            }
            // Chromium/Firefox style menu button, might only be needed if tabs aren't enough
            ToolButton {
                visible: tools.getSettingBool("showmenubutton", true)
                Layout.alignment: selectorLayout.flow === GridLayout.TopToBottom ? Qt.AlignLeft : Qt.AlignRight

                height: implicitHeight
                implicitHeight: 40
                id: menuButton
                contentItem: Image {
                    id: settingsImg
                    fillMode: Image.PreserveAspectFit
                    sourceSize.height: 30//menuButton.height -10
                    source: "qrc:/icons/ic_menu_black_24dp.png"
                }

                Menu {
                    id: mainMenu
                    y: selectorLayout.flow === GridLayout.TopToBottom ? 0: menuButton.height
                    x: selectorLayout.flow === GridLayout.TopToBottom ? menuButton.width :0
                    closePolicy: Popup.CloseOnPressOutsideParent | Popup.CloseOnEscape
                    MenuItem {
                        text: "Setup wizard"
                        onTriggered: {
                            setupWizard.open()
                        }
                    }
                }
                onClicked: {
                    if (mainMenu.visible) {
                        mainMenu.close()
                    } else {
                        mainMenu.open()
                    }
                }
            } // menu button*/
        } //layout

        // The main view; page is selected with the tabbar.
        // (SwipeView could also be used.)
        // Populated with pages in ApplicationWindow onCompleted.
        StackLayout {
            id: mainPagesStackLayout
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            //anchors.fill: parent
            currentIndex: tabBar.currentIndex
        }

    } // main layout

    ConnectingDialog {
        id: connectingDialog
        x: Math.floor((appWindow.width - width) / 2)
        y: Math.floor((appWindow.height - height) / 3)
    }

    ConfirmLoadSaveDialog {
        id: confirmLoadSaveDialog
        x: Math.floor((appWindow.width - width) / 2)
        y: Math.floor((appWindow.height - height) / 3)
    }

    ProfileSetupWizard {
        id: setupWizard
        visible: false
        anchors.fill: parent
    }
}
