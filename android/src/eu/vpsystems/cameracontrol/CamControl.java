package eu.vpsystems.cameracontrol.androidinterface;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.content.Context;
import android.util.Log;

public class CamControl extends org.qtproject.qt5.android.bindings.QtActivity
{
    private static WifiManager m_wifiManager;
    private static CamControl m_instance;

    public CamControl()
    {
        Log.e("AndroidInterface/constr", "going strong");
        m_instance = this;
    }

    public static void setActiveConnection(String s){
    }

    public static String getActiveSSID() {
        Log.e("AndroidInterface/getActiveSSID", "begin");
        String s = "";
        if (m_instance == null) {
            Log.e("AndroidInterface/getActiveSSID", "instance null");
        }
        if (m_wifiManager == null) {
            Object o = m_instance.getSystemService(Context.WIFI_SERVICE);
            Log.e("AndroidInterface/getActiveSSID", "15");
            m_wifiManager = (WifiManager)o;
        }
        WifiInfo m_wifiInfo = m_wifiManager.getConnectionInfo();
        s = m_wifiInfo.getSSID();
        return s;
     }
}
