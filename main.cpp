/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QQuickStyle>
#include <QFont>

#include "profiledata.h"
#include "connection.h"
#include "tools.h"
#include "monitoring.h"
#include "cameracontroller.h"

int main(int argc, char *argv[])
{
    qSetMessagePattern("%{function} %{line}: %{message}");
    // expose enum of CameraController to QML
    qmlRegisterUncreatableType<CameraController>("eu.vpsystems.camremote", 1, 0, "Camera", "Not to be created as QML type");
    Tools* tools{new Tools()};
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling, Tools::getSettingBool("scaleui", true));
    QQuickStyle::setStyle(Tools::getSettingString("uistyle", "Default"));

    QApplication* app{new QApplication(argc, argv)};

    QFont f{QGuiApplication::font()};
    f.setPointSize(QGuiApplication::font().pointSize() + Tools::getSettingInt("fontsizeplus", 0));
    QGuiApplication::setFont(f);

    QQmlApplicationEngine* engine{new QQmlApplicationEngine()};
    QQmlContext* ctx{engine->rootContext()};
    Connection* conn{new Connection()};
    ProfileData* data{new ProfileData(ctx, conn)};
    Monitoring* monitoring{new Monitoring(conn)};
    CameraController* camera{new CameraController(conn)};

    conn->getNetworks();

    qmlRegisterUncreatableType<CameraController>("eu.vpsystems.software", 1, 0, "CameraController", "Not to be created as QML type");
    ctx->setContextProperty("qApplication", app);
    ctx->setContextProperty("profileData", data);
    ctx->setContextProperty("connection", conn);
    ctx->setContextProperty("tools", tools);
    ctx->setContextProperty("monitoringData", monitoring);
    ctx->setContextProperty("camera", camera);
    engine->load(QUrl(QStringLiteral("qrc:/main.qml")));

    qDebug() << "EXECUTE APP";
    int exitcode = app->exec(); // 1 means "restart"
    qDebug() << "APP WAS EXECUTED";
    while (exitcode == 1) {
        qDebug() << "APP WILL BE RESTARTED";
        ctx = 0;

        delete tools;
        delete monitoring;
        delete data;
        delete conn;
        delete engine;
        delete app;

        tools = new Tools();
        QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling, Tools::getSettingBool("scaleui", true));
        app = new QApplication(argc, argv);
        engine = new QQmlApplicationEngine();
        ctx = engine->rootContext();
        conn = new Connection();
        data = new ProfileData(ctx, conn);
        monitoring = new Monitoring(conn);

        ctx->setContextProperty("qApplication", app);
        ctx->setContextProperty("profileData", data);
        ctx->setContextProperty("connection", conn);
        ctx->setContextProperty("tools", tools);
        ctx->setContextProperty("monitoringData", monitoring);
        engine->load(QUrl(QStringLiteral("qrc:/main.qml")));
        qDebug() << "APP WAS RECREATED";

        exitcode = app->exec();
    }
    qDebug() << "APP WAS EXECUTED AND QUITTED";

    delete engine;
    delete monitoring;
    delete data;
    delete conn;
    delete app;
    delete tools;

    return exitcode;
}
