QT += qml quick widgets quickcontrols2
CONFIG += c++11

SOURCES += main.cpp \
    firmware/camprops.c \
    profileitem.cpp \
    connection.cpp \
    tools.cpp \
    monitoring.cpp \
    profiledata.cpp \
    cameracontroller.cpp \
    ctrlmodeprofileitem.cpp

RESOURCES += qml.qrc \
    extraresources.qrc \
    icons.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    firmware/application.h \
    firmware/camfsm.h \
    firmware/camtasks.h \
    firmware/types.h \
    profileitem.h \
    connection.h \
    tools.h \
    monitoring.h \
    profiledata.h \
    cameracontroller.h \
    ctrlmodeprofileitem.h

android {
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
QT += androidextras
OTHER_FILES += \
    android/AndroidManifest.xml
}

DISTFILES += \
    android/src/eu/vpsystems/cameracontrol/CamControl.java
