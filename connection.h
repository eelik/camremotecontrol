/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QHostAddress>
#include <QNetworkConfigurationManager>

class Connection : public QObject
{
    Q_OBJECT
    Q_ENUMS(MessageType)

    Q_PROPERTY(bool connectedStatus MEMBER m_connected NOTIFY connectedStatusChanged)
    Q_PROPERTY(QString wifiSSID MEMBER m_wifiSSID NOTIFY wifiSSIDChanged)



public:

    enum MessageType: uint {
        GetProfiles = 0x03,
        SetProfiles = 0x04,
        SetTime = 0x06,
        GetRadio = 0x05,
        StartImage = 20,
        StopImage,
        ServoUp,
        ServoDown,
        ServoLeft,
        ServoRight,
        CameraShoot,
        CameraRec,
        CameraStopRec,
        CameraZoomIn,
        CameraZoomOut,
        CameraTvPlus,
        CameraTvMinus,
        CameraAvPlus,
        CameraAvMinus,
        CameraIsoPlus,
        CameraIsoMinus,
        CameraMfPlus,
        CameraMfMinus
    };

    explicit Connection(QObject *parent = 0);  

    Q_INVOKABLE void sendMessage(MessageType type, QByteArray content = QByteArray());
    Q_INVOKABLE void setTime();

private:
    bool m_connected;
    QString m_wifiSSID;

    QByteArray m_profiles;
    QByteArray m_monitoringData;
    QTcpSocket m_sock;
    QNetworkConfigurationManager m_netMgr;
    QNetworkConfiguration m_netCfg;
    QList<QString> networksStringList;
    QHostAddress m_camremoteIp;
    quint16 m_camremotePort;

    QTimer m_networkUpdateTimer;

    QByteArray m_outgoingMessageData;
    int m_messageType;
    QByteArray m_incomingMessageData;

    QMap<MessageType, QByteArray> m_messageMap;



signals:
    void tryingToConnect();
    void profilesAcquired(QByteArray profiles);
    void monitoringDataAcquired(QByteArray monitoringData);
    void networksAcquired(QList<QString> networks);
    void activeNetworkSet(QString network);
    void connected();
    void connectionError(QString error);
    void messageSent();
    void connectionCancelled();
    void connectedStatusChanged(bool connected);
    void okReceived();
    void wifiSSIDChanged();

public slots:


    /**
     * @brief Connects to CAMremote asynchronously; sends the signal connected()
     * when ready.
     */
    bool connectToCamremote();
    void cancelConnecting();
    bool isUnconnected() const;
    bool isConnected() const;

    // maybe not needed...
    void getNetworks();
    void setActiveNetwork(QString name);
    QString slotGetSSID() const;

    // for taking commands from UI
    void getProfilesFromCamremote();
    void setProfilesToCamremote(const QByteArray& profiledata);

    void getMonitoringDataFromCamremote();
    // slots which receive asyncronous networking signals and possibly
    // inform the UI with signals (q.v.)
    void slotNetworksUpdated();
    void slotSocketError(QAbstractSocket::SocketError);
    void slotConnectedToCamremote();
    void slotSocketStateChanged(QAbstractSocket::SocketState);
    void slotSocketReadyRead();

private:
    void populateMessageMap() const;
    void sendPendingMessageToCamremote();
    QByteArray createMessage(MessageType type, const QByteArray& content = QByteArray()) const;

};

#endif // CONNECTION_H
