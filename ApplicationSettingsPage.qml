/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1

Control {
    id: applicationSettingsPage
    contentItem: Flickable {
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.AutoFlickDirection
        contentHeight: contentItem.childrenRect.height + 10
        ColumnLayout {
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10

            GroupBox {
                Layout.fillWidth: true
                ColumnLayout {

                    Label {
                        id: ipAddressLabel
                        text: "CAMremote IP address:"
                    }

                    TextField {
                        TextMetrics {
                            id: metrics
                            text: "000.000.000.000"
                            font: ipAddressInput.font
                        }

                        Layout.preferredWidth: metrics.width + rightPadding + leftPadding + 4
                        id: ipAddressInput
                        text: tools.getIp()
                        onAccepted: {
                            console.log("New IP address is: ", text)
                            tools.setIp(text)
                        }
                    }
                    CheckBox {
                        id: ctrlModesCheckBox
                        visible: false
                        text: "Show all Control modes"
                        checked: tools.getSettingBool("showallctrlmodes", false)
                        onCheckedChanged: {
                            tools.setSettingBool("showallctrlmodes", checked)
                        }
                    }
                }
            }
            GroupBox {
                id: groupBox
                title: "User interface (changes require restart)"
                Layout.fillWidth: true
                ColumnLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right


                    CheckBox {
                        id: scalingCheckBox
                        text: "Enable UI scaling"
                        checked: tools.getSettingBool("scaleui", false)
                        onCheckedChanged: {
                            tools.setSettingBool("scaleui", checked)
                        }
                    }
                    Label {
                        id: uistyleLabel
                        text: "UI style"
                    }
                    RowLayout {
                        ComboBox {
                            id: themeComboBox
                            model: ["Default", "Material", "Universal"]
                            Component.onCompleted: {
                                themeComboBox.currentIndex = find(tools.getSettingString("uistyle", "Default"))
                            }
                            onActivated: {
                                tools.setSettingString("uistyle", themeComboBox.currentText)
                            }
                        }
                        CheckBox {
                            id: darkStyleCheckBox
                            text: "Dark"
                            checked: tools.getSettingBool("dark", false)
                            visible: ["Material","Universal"].indexOf(themeComboBox.currentText) > -1
                            onCheckedChanged: {
                                tools.setSettingBool("dark", checked)
                            }
                        }
                    }
                    Label {text: "Font size (small-normal-large)"}
                    Slider {
                        stepSize: 1
                        from: -3
                        to: 3
                        snapMode: Slider.SnapAlways
                        value: tools.getSettingInt("fontsizeplus", 0)
                        onValueChanged: tools.setSettingInt("fontsizeplus", value)                    }

                    Button {
                        id: restartButton
                        Layout.alignment: Qt.AlignRight
                        text: "Quit app now"
                        onClicked: {
                            appWindow.prepareForClosing()
                            tools.quitApp()
                        }
                    }

                }
            }
            Button {
                text: "Restore defaults"
                onClicked: {
                    defaultsDialog.open()
                }
                Dialog {
                    id: defaultsDialog
                    focus: true
                    modal: true
                    closePolicy:  Popup.CloseOnEscape
                    parent: ApplicationWindow.overlay
                    x: Math.floor((ApplicationWindow.window.width - width) / 2)
                    y: Math.floor((ApplicationWindow.window.height - height) / 3)
                    Component.onCompleted: {
                        parent = ApplicationWindow.overlay
                        footer.standardButton(Dialog.Ok).text = "OK"
                        footer.standardButton(Dialog.Cancel).text = "Cancel"
                    }
                    standardButtons: Dialog.Ok|Dialog.Cancel

                    contentItem:
                        ColumnLayout {
                        Label {
                            horizontalAlignment: Text.AlignHCenter
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            Layout.fillWidth: true
                            Layout.maximumWidth: 350
                            Layout.minimumWidth: 150
                            wrapMode: Text.WordWrap
                            text: "Restore the default settings? Your changed settings here and elsewhere in the application will be lost. Application restart is required for some changes to take effect."
                        }
                    }
                    onAccepted: {
                        tools.restoreDefaultSettings()
                    }
                }

            }
        }
    }
}
