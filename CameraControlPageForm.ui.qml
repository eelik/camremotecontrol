import QtQuick 2.8
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import "components"

Control {
    id: form
    property alias buttonServoDown: buttonServoDown
    property alias buttonServoLeft: buttonServoLeft
    property alias buttonServoUp: buttonServoUp
    property alias buttonShoot: buttonShoot
    property alias switchRec: switchRec
    property alias buttonZoomOut: buttonZoomOut
    property alias buttonZoomIn: buttonZoomIn
    property alias buttonTvMinus: buttonTvMinus
    property alias buttonTvPlus: buttonTvPlus
    property alias buttonAvMinus: buttonAvMinus
    property alias buttonAvPlus: buttonAvPlus
    property alias buttonIsoMinus: buttonIsoMinus
    property alias buttonIsoPlus: buttonIsoPlus
    property alias buttonMfMinus: buttonMfMinus
    property alias buttonMfPlus: buttonMfPlus
    property alias noConnectionItem: noConn
    property alias gridLayoutServoPad: gridLayoutServoPad
    property alias buttonServoRight: buttonServoRight

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent

        NoConnection {
            id: noConn
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        GridLayout {
            id: gridLayoutButtonPad
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredHeight: mainLayout.height
            Layout.preferredWidth: mainLayout.width
            flow: form.height > form.width ? GridLayout.TopToBottom : GridLayout.LeftToRight

            GridLayout {
                id: gridLayoutServoPad
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                columns: 3
                rows: 2

                ButtonWithMouseArea {
                    id: buttonServoDown
                    implicitWidth: metrics.servoButtonTextWidth + leftPadding + rightPadding + 2
                    text: qsTr("Down")
                    Layout.row: 1
                    Layout.column: 1
                }

                ButtonWithMouseArea {
                    id: buttonServoRight
                    implicitWidth: buttonServoDown.implicitWidth
                    text: qsTr("Right")
                    Layout.row: 0
                    Layout.column: 2
                    Layout.rowSpan: 2
                }

                ButtonWithMouseArea {
                    id: buttonServoLeft
                    implicitWidth: buttonServoDown.implicitWidth

                    text: qsTr("Left")
                    Layout.rowSpan: 2
                    Layout.row: 0
                    Layout.column: 0
                }

                ButtonWithMouseArea {
                    id: buttonServoUp
                    implicitWidth: buttonServoDown.implicitWidth
                    text: qsTr("Up")
                    Layout.row: 0
                    Layout.column: 1
                }
            }
            GridLayout {
                id: gridLayoutCameraPad
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                columnSpacing: 10
                columns: 2
                property int buttonWidth: buttonShoot.paddingRight + buttonShoot.paddingLeft
                                          + metrics.camButtonTextWidth > switchRec.implicitWidth ? buttonShoot.paddingRight + buttonShoot.paddingLeft + metrics.camButtonTextWidth : switchRec.implicitWidth

                RoundButton {
                    id: buttonShoot
                    text: qsTr("Shoot")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Switch {
                    id: switchRec
                    text: qsTr("Rec")
                    Layout.fillWidth: false
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                }

                Button {
                    id: buttonZoomOut
                    text: qsTr("Zoom -")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonZoomIn
                    text: qsTr("Zoom +")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonTvMinus
                    text: qsTr("Tv -")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonTvPlus
                    text: qsTr("Tv +")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonAvMinus
                    text: qsTr("Av -")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonAvPlus
                    text: qsTr("Av +")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonIsoMinus
                    text: qsTr("ISO -")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonIsoPlus
                    text: qsTr("ISO +")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonMfMinus
                    text: qsTr("MF -")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }

                Button {
                    id: buttonMfPlus
                    text: qsTr("MF +")
                    Layout.preferredWidth: gridLayoutCameraPad.buttonWidth
                }
            }
        }

    }
}
