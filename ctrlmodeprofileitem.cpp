/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//#include <QRegularExpression>
#include <QDebug>
#include <QSettings>

#include "ctrlmodeprofileitem.h"
#include "tools.h"

CtrlModeProfileItem::CtrlModeProfileItem(UINT8 *valueLocation, int size, ProfileData *parent):
    ProfileItem("ControlMode", valueLocation, size, parent)
{
}

void CtrlModeProfileItem::setModelFromCamitem(const CAMITEM *a_camitem)
{
    // First, add the items as in other models
    ProfileItem::setModelFromCamitem(a_camitem);
    //qDebug() << this->name << "camitem" << (int*)camitem;
    //if (camitem == NULL) return;

    // Create a list which has the beginnings of the wanted control mode names
    QString nameStr;
    bool showAllCtrlModes = Tools::getSettingBool("showallctrlmodes", false);
    //qDebug() << showAllCtrlModes;
    QStringList visibleModeNameBeginnings;
    QStringList hiddenModeNameBeginnings;
    // VisibleRole of each item is already set to true by default, so only if
    // all are NOT shown we have to change them.
    if (!showAllCtrlModes) {

        // Restricting the modes list can happen either by giving the list of hidden modes (exclusion)
        // or the list of visible modes (inclusion).
        // If the settings file has ctrlmodes list, use only those modes listed.
        // Otherwise hide certain predefined modes which are usually not needed.
        if (Tools::settings()->contains("ctrlmodes")) {
            visibleModeNameBeginnings = Tools::getSettingStringList("ctrlmodes", QStringList());
            qDebug () << "don't show all ctrlmodes, only " << visibleModeNameBeginnings;

            // Go through all items of this model and change the VisibleRole boolean
            //qDebug() << this->rowCount();
            for (int i = this->rowCount() - 1; i > -1; i--) {
                QStandardItem* it = this->item(i);
                nameStr = it->data(ProfileItem::TextRole).toString();
                bool showThisItem = false;

                // If the start of the item's text string begins with some item in the list, mark it as visible
                foreach(QString reString, visibleModeNameBeginnings) {
                    if (nameStr.startsWith(reString)) {
                        showThisItem = true;
                    }
                }
                //qDebug() << "add VisibleRole" << showThisItem << "to" << it->data(ProfileItem::TextRole).toString();
                it->setData(showThisItem, ProfileItem::VisibleRole);
            }
        } else { // The list of inclusion didn't exist, therefore use the exclusion list
            // The resource file has a sensible list of modes which shouldn't be visible to end users
            // (mostly used for development or testing).
            QSettings ctrlmodeini(":/settings/ctrlmodes.ini", QSettings::IniFormat);
            hiddenModeNameBeginnings = ctrlmodeini.value("hiddenctrlmodes", QStringList()).toStringList();

            for (int i = this->rowCount() - 1; i > -1; i--) {
                QStandardItem* it = this->item(i);
                nameStr = it->data(ProfileItem::TextRole).toString();
                bool showThisItem = true;

                // If the start of the item's text string begins with some item in the list, mark it as visible
                foreach(QString reString, hiddenModeNameBeginnings) {
                    if (nameStr.startsWith(reString)) {
                        showThisItem = false;
                    }
                }
                //qDebug() << "add VisibleRole" << showThisItem << "to" << it->data(ProfileItem::TextRole).toString();
                it->setData(showThisItem, ProfileItem::VisibleRole);
            }
        }
    }
    this->sort(0);

}
