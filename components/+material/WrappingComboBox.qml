/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

WrappingComboBox_base {
    id: comboBox

    delegate: MenuItem {
        id: control
        width: comboBox.popupWidth
        text: comboBox.textRole ? (Array.isArray(comboBox.model) ? modelData[comboBox.textRole] : model[comboBox.textRole]) : modelData
        font.weight: comboBox.currentIndex === index ? Font.DemiBold : Font.Normal
        Material.foreground: comboBox.currentIndex === index ? comboBox.Material.accent : comboBox.Material.foreground
        highlighted: comboBox.highlightedIndex === index
        // 'visible' alone doesn't remove this from list, still takes empty space, so hide by setting height to 0
        implicitHeight: model.show ? (Math.max(background ? background.implicitHeight :
                                                            0, Math.max(contentItem.implicitHeight, indicator
                                                                        ? indicator.implicitHeight : 0) + topPadding + bottomPadding)) : 0
        visible: model.show
        hoverEnabled: comboBox.hoverEnabled
        Component.onCompleted: {
            contentItem.visible = model.show && control.text
            contentItem.wrapMode = comboBox.wrap ? Text.Wrap: Text.NoWrap
            contentItem.elide = Text.ElideNone
            contentItem.lineHeight = 0.8
        }
    }
}
