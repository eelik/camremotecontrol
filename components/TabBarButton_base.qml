/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtGraphicalEffects 1.0
import QtQml.Models 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3


TabButton {
    id: tb

    property string fgColor
    property string bgColor
    property real fgOpacity

    property string image: model.icon
    property bool lessImportant: false
    property bool dark: false
    property real updatedWidth: ListView.view.updatedWidth
    property int orientation:  ListView.view.orientation
    property ListView listView: ListView.view
    text: model.text

    FontMetrics {
        id: metrics
        font: tb.font
    }

    topPadding: 8
    bottomPadding: topPadding
    leftPadding: topPadding + 8
    rightPadding: leftPadding
    implicitHeight: metrics.height + 10 > 40 ? metrics.height + 10 : 40
    implicitWidth: ListView.view.updatedWidth

    Component.onCompleted: {
        // Each button updates max width, in vertical it's max of buttons, in horizontal fixed
        ListView.view.updateWidth(orientation === ListView.Vertical ? contentItem.image.implicitWidth + contentItem.text.implicitWidth + tb.leftPadding + tb.rightPadding : tb.implicitHeight + 20)
    }

    onClicked: {
        ListView.view.currentIndex = model.index
    }
    contentItem: RowLayout {
        property alias text: tbText
        property alias image: img
        id: tbContent
        Image {
            id: img
            Layout.alignment: Qt.AlignHCenter
            opacity: fgOpacity//parent.checked ? (tb.lessImportant ? 0.9 : 1.0) : (tb.lessImportant ? 0.7 : 0.9)
            fillMode: Image.PreserveAspectFit
            sourceSize.height: tb.lessImportant ? tb.height - tb.topPadding*2 - 6 : tb.height - tb.topPadding*2
            source: tb.image

        }

        Text {
            id: tbText

            onVisibleChanged: {
                if (!visible) {
                    listView.updatedWidth = 10
                    listView.updateWidth(tb.implicitHeight + 20)
                }
            }

            Layout.alignment: Qt.AlignHCenter
            visible: tb.orientation === ListView.Vertical
            text: tb.text
            font: tb.font
            opacity: fgOpacity//enabled ? 1 : 0.3
            color: fgColor//!tb.checked ? fgUncheckedColor : tb.down ? fgCheckedPressedColor : fgCheckedColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        ColorOverlay {
            anchors.fill: img
            source: img
            opacity: fgOpacity
            color: fgColor//!tb.checked ? fgUncheckedColor : tb.down ? fgCheckedPressedColor : fgCheckedColor
        }
    }
}
