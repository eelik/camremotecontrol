/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtGraphicalEffects 1.0
import QtQml.Models 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

ListView {
    id: control
    implicitWidth: orientation === ListView.Horizontal ? contentWidth : updatedWidth
    implicitHeight: orientation === ListView.Vertical ? contentHeight  : 40 //See onContentHeightChanged, delegate.implicitHeight
    spacing: control.spacing
    //orientation: ListView.Horizontal // set elsewhere, used in delegate
    boundsBehavior: Flickable.StopAtBounds
    flickableDirection: Flickable.AutoFlickIfNeeded
    snapMode: ListView.SnapToItem

    highlightMoveDuration: 0
    highlightRangeMode: ListView.ApplyRange

    //updated with delegate button widths, finally max of them
    property real updatedWidth: 10

    //styling
    property string bgCheckedColor
    property string bgUncheckedColor
    property string bgCheckedPressedColor
    property string bgUncheckedPressedColor
    property string fgCheckedColor
    property string fgUncheckedColor
    property string fgCheckedPressedColor
    property string fgUncheckedPressedColor

    model: ListModel {
        id: tbModel
    }

    onOrientationChanged: {
        // button looses highlight when orientation is changed
        if (control.currentItem) control.currentItem.toggle()
    }

    onContentHeightChanged: {
        // This happens for some reason when orientation is changed from vertical to horizontal,
        // height must be set manually and then rebind
        if (contentHeight == -1) {
            control.implicitHeight = 40//currentItem.implicitHeight //40
            control.implicitHeight = Qt.binding(function() { return control.orientation === ListView.Vertical? control.contentHeight : 40 })
        }
    }

    function updateWidth(width) {
        control.updatedWidth = Math.max(width, control.updatedWidth)
    }
    delegate: TabBarButton{}
}
/*
TabBarRelocatable_base {
    id: control

    //styling
//    bgCheckedColor: Default.tabButtonColor
//    bgUncheckedColor: Default.tabButtonColor
//    bgCheckedPressedColor: Default.tabButtonCheckedPressedColor
//    bgUncheckedPressedColor: Default.tabButtonPressedColor

//    fgCheckedColor: Default.textLightColor
//    fgUncheckedColor: Default.textColor
//    fgCheckedPressedColor: Default.textDarkColor
//    fgUncheckedPressedColor: Default.textLightColor

    //delegate: TabBarButton {}

}

*/
