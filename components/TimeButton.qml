/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import QtQuick 2.8
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

Button {
    implicitWidth: metrics.width + leftPadding + rightPadding
    id: button1
    onClicked: {
        if (!tumblerPopup.visible) {
            tumblerPopup.open()
        } else {
            tumblerPopup.close()
        }
    }

    property var hhModel: undefined
    property var mmModel: undefined

    TextMetrics {
        id: metrics
        text: "00:00"
        font: button1.font
    }

    RowLayout {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 0
        Label {
            id: t1
            text: hhModel.currentText
        }
        Label {
            text: ":"
        }
        Label {
            id: t2
            text: mmModel.currentText
        }
    }

    Popup {
        margins: 1
        x: button1.width
        y: Math.floor(button1.height/2 - height/2)
        id: tumblerPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        RowLayout {
            //spacing: 2
            Tumbler {
                id: tumbler1
                implicitHeight: metrics.height * 7
                implicitWidth: metrics.width
                visibleItemCount: 3
                model: button1.hhModel
                currentIndex: hhModel.currentIndex
                onCurrentIndexChanged: {
                    console.log(button1.hhModel.name)
                    profileData.onItemChanged(button1.hhModel.name, currentIndex, true)
                }
            }
            Label {text: ":"}
            Tumbler {
                id: tumbler2
                implicitHeight: metrics.height * 7
                implicitWidth: metrics.width
                visibleItemCount: 3
                model: button1.mmModel
                currentIndex: mmModel.currentIndex
                onCurrentIndexChanged: {
                    console.log(button1.mmModel.name)
                    profileData.onItemChanged(button1.mmModel.name, currentIndex, true)
                }
            }
        }
    }
}
