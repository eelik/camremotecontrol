/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1
import QtGraphicalEffects 1.0


RoundButton {
    id: button
    text: ""
    //implicitWidth: height
    //Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
    property string source
    //implicitWidth: txt.width + image.width
    //width: implicitWidth
    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             contentItem.implicitHeight + topPadding + bottomPadding)
    onImplicitWidthChanged: {
        console.log(source, implicitWidth, "contitm", contentItem.implicitWidth, "text", txt.implicitWidth, "img", image.implicitWidth)
    }

    contentItem: RowLayout {
        Item {
            // Extra item needed because ColorOverlay can't be a sibling of the image inside the layout
            implicitHeight: image.height
            implicitWidth: image.width
            Image {
                id: image
                fillMode: Image.PreserveAspectFit
                //anchors.centerIn: parent
                sourceSize.height: button.background.implicitHeight - 6
                height: sourceSize.height
                source: button.source
                // This must not be a sibling directly inside a layout!

            }
            ColorOverlay {
                anchors.fill: image
                source: image
                // In dark styles background is dark, this makes the image white
                color: tools.isDarkTheme() ? "#ffffffff" : "00000000"
            }
        }
        //Item {
        Label {
            id: txt
            //Layout.alignment: Qt.AlignHCenter
            visible: button.text
            text: button.text
            font: button.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        //}

    }

}
