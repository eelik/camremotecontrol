/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtGraphicalEffects 1.0
import QtQml.Models 2.2
import QtQuick.Controls 2.1
import QtQuick.Controls.impl 2.1
import QtQuick.Layouts 1.3

TabButton {

    property bool wide:  true//(ListView.view.orientation === ListView.Horizontal) ? false : true
    id: tb
    property string image
    property bool lessImportant: false

    FontMetrics {
        id: metrics
        font: tb.font
    }

    topPadding: 8
    bottomPadding: topPadding
    leftPadding: topPadding + 8
    rightPadding: leftPadding
    implicitHeight: metrics.height + 10 > 40 ? metrics.height + 10 : 40
    implicitWidth: ListView.width
    onClicked: {
        ListView.view.currentIndex = ObjectModel.index
    }
    contentItem: RowLayout {
        id: tbContent
        Image {
            id: img
            Layout.alignment: Qt.AlignHCenter
            //opacity: parent.checked ? (tb.lessImportant ? 0.9 : 1.0) : (tb.lessImportant ? 0.7 : 0.9)
            fillMode: Image.PreserveAspectFit
            sourceSize.height: tb.lessImportant ? tb.height - tb.topPadding*2 - 6 : tb.height - tb.topPadding*2
            source: tb.image

        }

        Text {
            Layout.alignment: Qt.AlignHCenter
            visible: tb.wide
            text: tb.text
            font: tb.font
            //elide: Text.ElideRight
            opacity: enabled ? 1 : 0.3
            color: !tb.checked ? Default.textLightColor : tb.down ? Default.textDarkColor : Default.textColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        ColorOverlay {
            anchors.fill: img
            source: img
            color: !tb.checked ? Default.textLightColor : tb.down ? Default.textDarkColor : Default.textColor//tb.checked ? "#00000000" : "#ffffffff"
        }
    }


    background: Rectangle {
        implicitWidth: tbContent.implicitWidth
        implicitHeight: tbContent.implicitHeight
        color: tb.down
            ? (tb.checked ? Default.tabButtonCheckedPressedColor : Default.tabButtonPressedColor)
            : (tb.checked ? "transparent" : Default.tabButtonColor)
    }
}
