/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Item {
    id: wizard
    //anchors.fill: parent
    signal close()
    signal open()

    onClose: {
        wizard.visible = false
        wizardLoader.sourceComponent = undefined
        tabbedMainView.visible = true
    }
    onOpen: {
        console.log("onOpen")
        wizardLoader.sourceComponent = wizardComponent
        //wizardLoader.sourceComponent = testComponent
        //tabbedMainView.visible = false
        wizard.visible = true
    }

    Timer {
        id: cancelTimer
        interval: 0; running: false; repeat: false
        onTriggered: {wizard.close()}
    }

    Loader {
        id: wizardLoader
        anchors.fill: parent
        sourceComponent: undefined
        onLoaded: {
            console.log("wizard component loaded from source: ", wizardLoader.sourceComponent)
        }

    }
    Component {
        id: testComponent
        Text {
            text: "wizard should be here"
        }
    }

    Component {
        id: wizardComponent
        Page {
            id: setupWizard
            //anchors.fill: parent
            property alias buttonCancel: buttonCancel

            states: [
                State {
                    name: "firstPageBeginState"
                    PropertyChanges {
                        target: buttonPrevious
                        visible: false
                    }
                    PropertyChanges {
                        target: buttonFinish
                        visible: false
                    }
                    PropertyChanges {
                        target: buttonNext
                        enabled: false
                    }
                },
                State {
                    name: "firstPageReadyState"
                    PropertyChanges {
                        target: buttonPrevious
                        visible: false
                    }
                    PropertyChanges {
                        target: buttonFinish
                        visible: false
                    }
                    PropertyChanges {
                        target: buttonNext
                        enabled: true
                    }
                },
                State {
                    name: "secondPageBeginState"
                    PropertyChanges {
                        target: buttonPrevious
                        visible: true
                    }
                    PropertyChanges {
                        target: buttonFinish
                        visible: true
                    }
                    PropertyChanges {
                        target: buttonNext
                        visible: false
                    }
                }
            ]

            StackView {
                id: stackView
                onBusyChanged: {
                    if(busy) {
                        console.log("transition going on")
                        buttons.enabled = false
                        buttons.opacity = 0
                    } else {
                        console.log("transition no transition going on")
                        buttons.enabled = true
                        buttons.opacity = 1
                    }
                }
                Item {
                    id: firstPage
                }
                Item {
                    id: secondPage
                }
            }
            footer: ToolBar {
                id: buttons
                background: Rectangle{}
                RowLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottomMargin: 10
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    ToolButton {
                        id: buttonCancel
                        text: "Cancel"
                        onClicked: {
                            console.log("wizard cancel clicked")
                            cancelTimer.running = true
                        }
                    }
                    ToolButton {
                        id: buttonPrevious
                        text: "<Previous"
                    }
                    Item {
                        //Layout.fillWidth: true
                        implicitWidth: 100
                    }
                    ToolButton {
                        id: buttonNext
                        text: "Next>"
                    }
                    ToolButton {
                        id: buttonFinish
                        text: "Finish"
                    }
                }

            }
        }
    }
}
