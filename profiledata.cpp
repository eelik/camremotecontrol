/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <QDebug>
#include <QSettings>

#include <cstring>

#include "tools.h"
#include "profiledata.h"
#include "profileitem.h"
#include "ctrlmodeprofileitem.h"
#include "firmware/camfsm.h"
#include "connection.h"

// Structs from firmware/camprops.c
// Features for different cameras
extern CAMDESCRIPTOR CameraDesc[];
// values/texts for Shoot at Step items
extern CAMITEM ServoDelay[];
extern CAMITEM ServoCount[];

// values/texts for CAMPROFILEV01 fixed items
extern CAMITEM StartupDelay[];
extern CAMITEM IvmRepeat[];
extern CAMITEM IvmDuration[];
extern CAMITEM IvmInterval[];
extern CAMITEM Uvlo[];
extern CAMITEM Pause[];
extern CAMITEM TimeRadioWakeup[];
//extern CAMITEM TimeLapse[];

ProfileData::ProfileData(QQmlContext* ctx, Connection *a_connection, QObject *parent) :
    QObject(parent),
    m_connection{a_connection},
    context{ctx}
{
    connect(a_connection, &Connection::profilesAcquired, this, &ProfileData::slotProfilesAcquired);
    connect(this, &ProfileData::profilesLoaded, this, &ProfileData::onProfilesLoaded);

    this->currentProfile = this->m_rawProfiles;
    this->m_rawProfile01 = reinterpret_cast<CAMPROFILEV01*>(m_rawProfiles);
    initFixedProfileItems();
    initProfileItems();
    initTimerProfileItems();

    // Default values for a profile are now set; copy them from profile 0 to other profiles
    for (int i = 1; i < 7; i++) {
        std::memcpy(this->currentProfile + i, this->currentProfile, sizeof(CAMPROFILEV00));
    }
    loadFromSettings();
}

ProfileData::~ProfileData()
{
    //save();
}

/**
 * @brief Data::initFixedProfileItems Initializes the ProfileItems which are always the same.
 * See initProfileItems() for further info.
 */
void ProfileData::initFixedProfileItems()
{
    QList<UINT16> values;
    QList<QString> names;

    // ProfileNumber is a special item, instead of using a special class we just use
    // ProfileItem and don't use its e.g. load() and save() methods. See onProfileChanged
    // and slotProfilesAcquired for special handling.
    ProfileItem* item{new ProfileItem("ProfileNumber", NULL, 0, this)};
    // Id ISN'T profile number (char*)(&(currentProfile->Id)), sizeof(currentProfile->Id)
    for (ushort i = 0; i < 7; i++) {
        values.append(i);
        names.append(QString(QByteArray::number(i+1)) );
    }
    item->setModelFromLists(values, names);

    // The second item is the available camera models, it's also fixed once created
    values.clear();
    names.clear();
    CAMDESCRIPTOR* desc;
    desc = CameraDesc;
    QStringList availableCameras = Tools::getSettingStringList("availablecameras", QStringList(), true);
    qDebug() << availableCameras;
    if (availableCameras.size() == 0) {
        while (desc->CamID != 0) { // only the last in the array is 0
            values.append(desc->CamID);
            names.append(desc->ModelName);
            desc++;
        }
    } else {
        QString notSel("- Not Selected -");
        while (desc->CamID != 0) { // only the last in the array is 0
            if (notSel == QString(desc->ModelName) || availableCameras.contains(desc->ModelName)) {
                values.append(desc->CamID);
                names.append(desc->ModelName);
            }
            desc++;
        }
    }
    item = new ProfileItem("CameraName", reinterpret_cast<UINT8*>(&(currentProfile->CamModel)), sizeof(currentProfile->CamModel), this);
    item->setModelFromLists(values, names);
    item->save(0, this->currentProfile);

    // These are always the same regardless of the camera model

    item = new ProfileItem("ShootCnt", reinterpret_cast<UINT8*>(&(currentProfile->ServoShootCnt)), sizeof(currentProfile->ServoShootCnt), this);
    item->setModelFromCamitem(ServoCount);
    item->save(0, this->currentProfile);

    item = new ProfileItem("ShootBefore", reinterpret_cast<UINT8*>(&(currentProfile->ServoShootBefore)), sizeof(currentProfile->ServoShootBefore), this);
    item->setModelFromCamitem(ServoDelay);
    item->save(0, this->currentProfile);

    item = new ProfileItem("ShootAfter", reinterpret_cast<UINT8*>(&(currentProfile->ServoShootAfter)), sizeof(currentProfile->ServoShootAfter), this);
    item->setModelFromCamitem(ServoDelay);
    item->save(0, this->currentProfile);
}

/**
 * @brief Data::initProfileItems Initializes the ProfileItems which can change.
 *
 * These are the names of the profile data items and corresponding UI data models.
    The model name visible to QML is this model name in lowercase + "Model", e.g. "cameranameModel".
    For the possible items See CAMDESCRIPTOR and CAMPROFILEV00. The name here doesn't have to be
    the same as in those structs, but it should be clear.
    The members of CAMPROFILEV00 are used to initialize the items.

    ProfileNumber // (not in CAMDESCRIPTOR or CAMPROFILEV00)
    CameraName // name changed for clarity (originally CamModel)
    // These models are changed according to the camera model
    CableMethod // name changed for clarity (CtrlType)
    AEProgram
    Tv
    Av
    IsoSpeed
    Zoom
    Focus
    Size
    Quality
    WhiteBalance
    ControlMode
    // These models remain always the same
    ShootCnt // ServoShootCnt
    ShootBefore // ServoShootBefore
    ShootAfter // ServoShootAfter

    (Add new ones here if needed.
     Make necessary changes to initProfileItems, initFixedProfileItems, onCameraChanged and onProfileChanged.)

 */
void ProfileData::initProfileItems()
{
    // These will change according to the selected camera model.

    new ProfileItem("CableMethod", reinterpret_cast<UINT8*>(&(currentProfile->CtrlType)), sizeof(currentProfile->CtrlType), this);
    new ProfileItem("AEProgram", reinterpret_cast<UINT8*>(&(currentProfile->AEProgram)), sizeof(currentProfile->AEProgram), this);
    new ProfileItem("Tv", reinterpret_cast<UINT8*>(&(currentProfile->Tv)), sizeof(currentProfile->Tv), this);
    new ProfileItem("Av", reinterpret_cast<UINT8*>(&(currentProfile->Av)), sizeof(currentProfile->Av), this);
    new ProfileItem("IsoSpeed", reinterpret_cast<UINT8*>(&(currentProfile->IsoSpeed)), sizeof(currentProfile->IsoSpeed), this);
    new ProfileItem("Zoom", reinterpret_cast<UINT8*>(&(currentProfile->Zoom)), sizeof(currentProfile->Zoom), this);
    new ProfileItem("Focus", reinterpret_cast<UINT8*>(&(currentProfile->Focus)), sizeof(currentProfile->Focus), this);
    new ProfileItem("Size", reinterpret_cast<UINT8*>(&(currentProfile->Size)), sizeof(currentProfile->Size), this);
    new ProfileItem("Quality", reinterpret_cast<UINT8*>(&(currentProfile->Quality)), sizeof(currentProfile->Quality), this);
    new ProfileItem("WhiteBalance", reinterpret_cast<UINT8*>(&(currentProfile->WhiteBalance)), sizeof(currentProfile->WhiteBalance), this);

    new CtrlModeProfileItem(reinterpret_cast<UINT8*>(&(currentProfile->ControlMode)), sizeof(currentProfile->ControlMode), this);

    // At startup select the first camera model ("Not selected" in the corresponding CAMDESCRIPTOR, see camprops.c).
    qDebug() << "Now ProfileItems are initialized, set the default camera 0, save to profile, don't signal";

    resetDependentItemModels();
    //qDebug();
}

/**
 * @brief ProfileData::initTimerProfileItems inits models for the controller version which has timers.
 *
 * It uses CAMPROFILEV01 instead of V00 but the profile is located overriding one of the V00 profiles (it has the same size
 * and partly the same structure). Changing the V00 profile and V01 profile through these items conflict with each other
 * and only one of them should be used. At the moment the GUI prevents using one of them - either 7 profiles OR one V01
 * profile is in use.
 */
void ProfileData::initTimerProfileItems()
{
    ProfileItem* item;
    item = new ProfileItem("StartupDelay", reinterpret_cast<UINT8*>(&(m_rawProfile01->StartupDelay)), sizeof(m_rawProfile01->StartupDelay), this);
    item->setModelFromCamitem(StartupDelay);
    item->save(0, this->m_rawProfile01);

    item = new ProfileItem("Repeat", reinterpret_cast<UINT8*>(&(m_rawProfile01->Repeat)), sizeof(m_rawProfile01->Repeat), this);
    item->setModelFromCamitem(IvmRepeat);
    item->save(0, this->m_rawProfile01);

    item = new ProfileItem("Duration", reinterpret_cast<UINT8*>(&(m_rawProfile01->Duration)), sizeof(m_rawProfile01->Duration), this);
    item->setModelFromCamitem(IvmDuration);
    item->save(0, this->m_rawProfile01);

    item = new ProfileItem("Interval", reinterpret_cast<UINT8*>(&(m_rawProfile01->Interval)), sizeof(m_rawProfile01->Interval), this);
    item->setModelFromCamitem(IvmInterval);
    item->save(0, this->m_rawProfile01);

    item = new ProfileItem("Uvlo", reinterpret_cast<UINT8*>(&(m_rawProfile01->Uvlo)), sizeof(m_rawProfile01->Uvlo), this);
    item->setModelFromCamitem(Uvlo);
    item->save(0, this->m_rawProfile01);

    item = new ProfileItem("Pause", reinterpret_cast<UINT8*>(&(m_rawProfile01->Pause)), sizeof(m_rawProfile01->Pause), this);
    item->setModelFromCamitem(Pause);
    item->save(0, this->m_rawProfile01);

    //item = new ProfileItem("TimeLapse", reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeLapse)), sizeof(m_rawProfile01->TimeLapse), this);
    //item->setModelFromCamitem(TimeLapse);
    //item->save(0, this->m_rawProfile01);

    item = new ProfileItem("TimeRadioWakeup", reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeRadioWakeup)), sizeof(m_rawProfile01->TimeRadioWakeup), this);
    //item->setModelFromCamitem(TimeRadioWakeup);
    item->save(0, this->m_rawProfile01);

    QStringList hourList;
    QList<UINT16> hourValueList;
    for (UINT16 i = 0; i < 24; i++) {
        hourValueList << i;
        hourList << QString("%1").arg(i, 2, 10, QChar('0'));
    }
    QStringList minuteList;
    QList<UINT16> minuteValueList;
    for (UINT16 i = 0; i < 60; i++) {
        minuteValueList << i;
        minuteList << QString("%1").arg(i, 2, 10, QChar('0'));
    }
    qDebug() << hourList;
    qDebug() << minuteList;

    // Start/End time addresses in CAMPROFILEV01

    int timeSize = sizeof(m_rawProfile01->TimeSet1Start)/2;
    UINT8* t1Start = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet1Start));
    UINT8* t1End = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet1End));
    UINT8* t2Start = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet2Start));
    UINT8* t2End = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet2End));
    UINT8* t3Start = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet3Start));
    UINT8* t3End = reinterpret_cast<UINT8*>(&(m_rawProfile01->TimeSet3End));

    // Start/End time sets, hours
    item = new ProfileItem("TimeSet1StartHh", t1Start, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    item = new ProfileItem("TimeSet1EndHh", t1End, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    item = new ProfileItem("TimeSet2StartHh", t2Start, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    item = new ProfileItem("TimeSet2EndHh", t2End, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    item = new ProfileItem("TimeSet3StartHh", t3Start, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    item = new ProfileItem("TimeSet3EndHh", t3End, timeSize, this);
    item->setModelFromLists(hourValueList, hourList);

    // Start/End time sets, minutes, address is the second byte of the 2-byte value
    item = new ProfileItem("TimeSet1StartMm", t1Start + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);

    item = new ProfileItem("TimeSet1EndMm", t1End + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);

    item = new ProfileItem("TimeSet2StartMm", t2Start + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);

    item = new ProfileItem("TimeSet2EndMm", t2End + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);

    item = new ProfileItem("TimeSet3StartMm", t3Start + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);

    item = new ProfileItem("TimeSet3EndMm", t3End + 1, timeSize, this);
    item->setModelFromLists(minuteValueList, minuteList);


}

/**
 * @brief Data::onItemChanged Called when the item was changed from the UI
 * @param name Name from the UI
 * @param index
 */
void ProfileData::onItemChanged(QString a_name, int a_index, bool a_v01profile)
{
    qDebug() << a_name << a_index;

    // Find the ProfileItem for the name
    ProfileItem* item{nullptr};
    foreach (QString keyname, profileItems.keys()) {
        if (keyname.toLower() == a_name.toLower()){
             item = profileItems[keyname];
        }
    }

    // Set the current index for the ProfileItem.
    // This came from the UI, so save to profile but don't signal.
    Q_ASSERT(item);
    if (a_v01profile) {
        item->save(a_index, this->m_rawProfile01);
    } else {
        item->save(a_index, this->currentProfile);
    }
    qDebug() << "END";
}

/*
 * Called when the profile # was changed from the UI.
 */
void ProfileData::onProfileChanged(int a_index)
{
    qDebug() << "Old profile:" << this->currentProfile << "New:" << a_index << &(this->m_rawProfiles[a_index]);
    // The values in the previous profile's data have already been changed on the fly if the user changed them.
    // So they are already saved and there's no need to save any values from the previous profile.

    // TODO: save the new profile # to the first profile
    m_rawProfiles[0].Id = static_cast<UINT16>(a_index);
    this->currentProfile = &(m_rawProfiles[a_index]);
    // this isn't updated automatically from profile data (load() method of "ProfileNumber" can't be used)
    profileItems["ProfileNumber"]->m_selectedIndex = a_index;

    profileItems["CameraName"]->load(this->currentProfile);
    // Some models depend on selected camera
    resetDependentItemModels();

    CAMPROFILEV00* profile = this->currentProfile;

    // currently active texts/indexes are loaded from profile data,
    // loading also notifies UI
    profileItems["CableMethod"]->load(profile);
    profileItems["AEProgram"]->load(profile);
    profileItems["Tv"]->load(profile);
    profileItems["Av"]->load(profile);
    profileItems["IsoSpeed"]->load(profile);
    profileItems["Zoom"]->load(profile);
    profileItems["Focus"]->load(profile);
    profileItems["Size"]->load(profile);
    profileItems["Quality"]->load(profile);
    profileItems["WhiteBalance"]->load(profile);
    profileItems["ControlMode"]->load(profile);
    qDebug() << "Independent profileItems are now loaded...";
    profileItems["ShootCnt"]->load(profile);
    profileItems["ShootBefore"]->load(profile);
    profileItems["ShootAfter"]->load(profile);


    //qDebug() << "END";
}

/*
 * When camera model is changed, either explicitly from the UI or
 * when another profile is selected, these models must be changed accordingly
 */
void ProfileData::resetDependentItemModels()
{
    qDebug();
    CAMDESCRIPTOR* desc;
    desc = CameraDesc; // CameraDesc is external in camprops.c
    // find the Camera Descriptor struct for the model
    for ( ; (desc->CamID != currentProfile->CamModel) && (desc->CamID != 0); desc++);
    // These models are dependent on the camera model and require recreating.
    // Always signal the change because it wasn't directly initiated from the UI.
    // Also save the new values to profile.
    profileItems["CableMethod"]->setModelFromCamitem(desc->CtrlType);
    profileItems["AEProgram"]->setModelFromCamitem(desc->AEProgram);
    profileItems["Tv"]->setModelFromCamitem(desc->Tv);
    profileItems["Av"]->setModelFromCamitem(desc->Av);
    profileItems["IsoSpeed"]->setModelFromCamitem(desc->IsoSpeed);
    profileItems["Zoom"]->setModelFromCamitem(desc->Zoom);
    profileItems["Focus"]->setModelFromCamitem(desc->Focus);
    profileItems["Size"]->setModelFromCamitem(desc->Size);
    profileItems["Quality"]->setModelFromCamitem(desc->Quality);
    profileItems["WhiteBalance"]->setModelFromCamitem(desc->WhiteBalance);

    profileItems["ControlMode"]->setModelFromCamitem(desc->ControlMode);
}

/*
 * Every time the selected camera is changed, the default values
 * must be selected for the datamodels which depend on the selected camera.
 * Default value for each item is the first one (with index 0).
 */
void ProfileData::saveDependentModelsWithIndex0()
{
    //qDebug();
    CAMPROFILEV00* profile = this->currentProfile;
    profileItems["CableMethod"]->save(0, profile);
    profileItems["AEProgram"]->save(0, profile);
    profileItems["Tv"]->save(0, profile);
    profileItems["Av"]->save(0, profile);
    profileItems["IsoSpeed"]->save(0, profile);
    profileItems["Zoom"]->save(0, profile);
    profileItems["Focus"]->save(0, profile);
    profileItems["Size"]->save(0, profile);
    profileItems["Quality"]->save(0, profile);
    profileItems["WhiteBalance"]->save(0, profile);

    profileItems["ControlMode"]->save(0, profile);
}

/*
 * When camera model is changed, the selected values/indexes are changed.
 * This methods notifies the listeners about these changes.
 */
void ProfileData::notifyDependentModels() const
{
    profileItems["CableMethod"]->notify();
    profileItems["AEProgram"]->notify();
    profileItems["Tv"]->notify();
    profileItems["Av"]->notify();
    profileItems["IsoSpeed"]->notify();
    profileItems["Zoom"]->notify();
    profileItems["Focus"]->notify();
    profileItems["Size"]->notify();
    profileItems["Quality"]->notify();
    profileItems["WhiteBalance"]->notify();

    profileItems["ControlMode"]->notify();
}

/*
 * Called when the user selects a camera from the UI, or when
 * the cameramodel value is loaded from the profile data
 */
void ProfileData::onCameraChanged(int a_index)
{
    qDebug() << a_index;

    CAMPROFILEV00* profile{this->currentProfile};
    profileItems["CameraName"]->save(a_index, profile);
    CAMDESCRIPTOR* desc{CameraDesc}; // CameraDesc is external in camprops.c
    // find the Camera Descriptor struct for the model
    for ( ; (desc->CamID != currentProfile->CamModel) && (desc->CamID != 0); desc++);

    this->resetDependentItemModels();
    this->saveDependentModelsWithIndex0();
    this->notifyDependentModels();

    qDebug() << "END";
}



void ProfileData::save(int a_numberOfProfilesToSave) const
{

    const char* ptrProfiles{reinterpret_cast<const char*>(this->m_rawProfiles)};
    int size{(static_cast<int>(sizeof(CAMPROFILEV00)) * a_numberOfProfilesToSave)};
    QByteArray bytes{ptrProfiles, size};
    auto debug_default_profile = m_rawProfiles[0].Id;
    // Send to CAMremote
    qDebug() << "saving to camremote, default profile is "<< debug_default_profile;
    m_connection->setProfilesToCamremote(bytes);
}

void ProfileData::saveToSettings() const
{
    const char* ptrProfiles{reinterpret_cast<const char*>(this->m_rawProfiles)};
    int size{sizeof(CAMPROFILEV00) * 7};
    QByteArray bytes{ptrProfiles, size};
    UINT16 debug_default_profile{m_rawProfiles[0].Id};
    qDebug() << "saving to settings, default profile is "<< debug_default_profile;
    Tools::settings()->setValue("profiledata", bytes);
}

/*
 * Starts loading profiles data from camremote or creates new empty data.
 */
void ProfileData::load(bool a_fromRemote) {
    qDebug();
    // Optionally from local file.
    // Initialized with zeros if there's no saved data.
    if (!a_fromRemote) {
        qDebug() << "load from local file";
        loadFromSettings();
    } else {
        qDebug() << "load from device through net";
        // Starts loading. Signal from Connection is connected to slotProfilesAcquired
        m_connection->getProfilesFromCamremote();
    }

}

void ProfileData::loadFromSettings()
{
    QByteArray arr{Tools::settings()->value("profiledata", QByteArray(sizeof(CAMPROFILEV00) * 7, '\x0')).toByteArray()};
    slotProfilesAcquired(arr);
}

/*
 * Receives a signal from Connection when profiles data has been received
 */
void ProfileData::slotProfilesAcquired(const QByteArray a_profiles)
{
    qDebug() << "Profiles were aqcuired from device or from settings: ";
    qDebug() << *a_profiles;
    // Copy data to this object from the Connection
    std::memcpy(reinterpret_cast<char*>(m_rawProfiles), a_profiles.data(), sizeof(CAMPROFILEV00) * 7);
    // TODO: dig this default profile index out from the first profile
    UINT16 default_profile{m_rawProfiles[0].Id};
    qDebug() << "default profile is " << default_profile;
    profileItems["ProfileNumber"]->m_selectedIndex = default_profile;
    // causes models and UI data reload
    onProfileChanged(default_profile);
    // Notify UI that profile# index has changed
    // (it's not notified in onProfileChanged although other model changes are)
    profileItems["ProfileNumber"]->notify();
    // received by UI state machine
    qDebug() << "emit profilesLoaded";
    emit profilesLoaded(default_profile);
}

/**
 * @brief ProfileData::onProfilesLoaded after loading profiles init V01 Timer items
 *
 * (Other items are updated in onProfileChanged which isn't semantically relevant for V01 items)
 * This slot should be connected in constructor.
 */
void ProfileData::onProfilesLoaded()
{
    qDebug() << "start";
    profileItems["StartupDelay"]->load(this->m_rawProfile01);
    profileItems["Repeat"]->load(this->m_rawProfile01);
    profileItems["Duration"]->load(this->m_rawProfile01);
    profileItems["Interval"]->load(this->m_rawProfile01);
    profileItems["Uvlo"]->load(this->m_rawProfile01);
    profileItems["Pause"]->load(this->m_rawProfile01);
    profileItems["TimeRadioWakeup"]->load(this->m_rawProfile01);
    qDebug() << "middle";
    //sets
    profileItems["TimeSet1StartHh"]->load(this->m_rawProfile01);
    profileItems["TimeSet1EndHh"]->load(this->m_rawProfile01);
    profileItems["TimeSet2StartHh"]->load(this->m_rawProfile01);
    profileItems["TimeSet2EndHh"]->load(this->m_rawProfile01);
    profileItems["TimeSet3StartHh"]->load(this->m_rawProfile01);
    profileItems["TimeSet3EndHh"]->load(this->m_rawProfile01);

    profileItems["TimeSet1StartMm"]->load(this->m_rawProfile01);
    profileItems["TimeSet1EndMm"]->load(this->m_rawProfile01);
    profileItems["TimeSet2StartMm"]->load(this->m_rawProfile01);
    profileItems["TimeSet2EndMm"]->load(this->m_rawProfile01);
    profileItems["TimeSet3StartMm"]->load(this->m_rawProfile01);
    profileItems["TimeSet3EndMm"]->load(this->m_rawProfile01);

    qDebug() << "end";
}
