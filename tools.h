/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TOOLS_H
#define TOOLS_H

#include <QObject>
#include <QSettings>
#include <QVariant>

class QSettings;

class Tools : public QObject
{
    Q_OBJECT

    static QSettings* m_settings;
    static QSettings* m_readonly_settings;
    static bool m_firstTime;
    static QVariant getSettingVariant(QString name, QVariant default_val);

public:
    explicit Tools(QObject *parent = 0);
    ~Tools();

    Q_INVOKABLE static QSettings* settings();
    Q_INVOKABLE static void restartApp();
    Q_INVOKABLE static void quitApp();
    Q_INVOKABLE static QString getIp();
    Q_INVOKABLE static int getPort();
    Q_INVOKABLE static void setIp(QString ip);
    Q_INVOKABLE static void setPort(int port);
    Q_INVOKABLE static bool isFirstTime();
    Q_INVOKABLE static void restoreDefaultSettings(bool customizedDefaults = true);

    Q_INVOKABLE static void setSettingBool(QString name, bool value);
    Q_INVOKABLE static bool getSettingBool(QString name, bool default_val);
    Q_INVOKABLE static void setSettingString(QString name, QString value);
    Q_INVOKABLE static QString getSettingString(QString name, QString default_val);
    Q_INVOKABLE static void setSettingInt(QString name, int value);
    Q_INVOKABLE static int getSettingInt(QString name, int default_val);
    Q_INVOKABLE static bool isDarkTheme();
    Q_INVOKABLE static void setSettingStringList(QString name, QStringList value);
    Q_INVOKABLE static QStringList getSettingStringList(QString name, QStringList default_val, bool interpretEmpty = false);

    Q_INVOKABLE static QString getStringFromFile(QString name);
    Q_INVOKABLE static QString getGPL();
    Q_INVOKABLE static QString getLGPL();
    Q_INVOKABLE static QString getMIT();
    Q_INVOKABLE static QString getAboutCRC();
    Q_INVOKABLE static QString getAboutWLC();
    Q_INVOKABLE static QString getAboutQt();
    Q_INVOKABLE static QString getAboutIcons();
};

#endif // TOOLS_H
