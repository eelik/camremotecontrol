/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <QFile>
#include <QDebug>
#include <QResource>
#include <QSettings>
#include <QApplication>
#include <QByteArray>

#include "tools.h"

Tools::Tools(QObject *parent) : QObject(parent) {
    qDebug() << "creating tools instance";

    m_settings = new QSettings(QSettings::IniFormat, QSettings::UserScope,
                                            "VP-Systems", "CAMremoteControl");

    m_readonly_settings = new QSettings(":/settings/read_only_customizations.ini", QSettings::IniFormat);
    if (isFirstTime()) {
        restoreDefaultSettings(true);
    }
}

Tools::~Tools() {
    settings()->setValue("firsttime", m_firstTime);
    settings()->sync();
    //qDebug() << "destroying tools instance";
    delete m_settings;
    m_settings = nullptr;
}


/* Restores the default settings from resource files, optionally also customized defaults.
 */
void Tools::restoreDefaultSettings(bool customizedDefaults) {
    // Developer can change the default settings in these two resource files.
    // The first is meant for the sane default values for general releases and is expected to
    // be changed rarely.
    // The second is meant for customized builds (for example for one customer) and can also
    // be used for development testing. Customized settings override the others. Clear the file
    // if there's no need to customize.
    // The .ini file in the platform-specific location (e.g. AppData/Roaming/VP-Systems/CAMremoteControl.ini)
    // must be empty or at least "firsttime" key must be true for default and customized
    // settings to take effect.
    QSettings default_settings(":/settings/default_settings.ini", QSettings::IniFormat);
    QStringList default_keys = default_settings.allKeys();
    qDebug() << "All default settings keys:" << default_keys;
    foreach(QString key, default_keys) {
        settings()->setValue(key, default_settings.value(key));
    }
    if (customizedDefaults) {
        QSettings customized_settings(":/settings/customized_default_settings.ini", QSettings::IniFormat);
        QStringList customized_keys = customized_settings.allKeys();
        qDebug() << "All default settings keys:" << customized_keys;
        foreach(QString key, customized_keys) {
            settings()->setValue(key, customized_settings.value(key));
        }
    }
}

// not used until a qt bug has been fixed
void Tools::restartApp() {
    qApp->exit(1);
}

void Tools::quitApp() {
    qApp->exit(0);
}

QString Tools::getLGPL() {
    //qDebug();
    return (Tools::getStringFromFile(":/license/lgpl-3.0-standalone.html"));
}

QString Tools::getGPL() {
    //qDebug();
    return (Tools::getStringFromFile(":/license/gpl-3.0-standalone.html"));
}

QString Tools::getMIT(){
    //qDebug();
    //return (Tools::getStringFromFile(":/license/mit-license.html").arg(getSettingString("copyrightyears", "2016-2018")));
    return (Tools::getStringFromFile(":/license/nowarranty.html"));
}

QString Tools::getAboutCRC(){
    qDebug();
    QString s;
    QTextStream ts(&s);

    ts << "<p>Copyright (C) %1 VP-systems. All rights reserved.</p>" <<
          "<p>CAMremoteControl is an application for controlling CAMremote, " <<
          "a remote control device for cameras and camcorders. " <<
          "See http://vp-systems.eu/camremote.html for further information about CAMremote " <<
          "and http://vp-systems.eu/camremotecontrol.html about this application.</p>";
    return s.arg(getSettingString("copyrightyears", "2016-2018"));
}

QString Tools::getAboutWLC() {
    QString s;
    QTextStream ts(&s);

    ts << "<p>Copyright (C) %1 VP-systems. All rights reserved.</p>" <<
          "<p>WLCremoteControl is an application for controlling WLCremote, " <<
          "a remote control device for photography. ";
    return s.arg(getSettingString("copyrightyears", "2016-2018"));
}

QString Tools::getAboutQt(){
    //qDebug();
    return "<p>This application uses the Qt framework under the LGPL 3.0 license. See below for the terms of the LGPL and the GPL. See https://www.qt.io/licensing/ and http://doc.qt.io/qt-5/licensing.html for further information.</p>";
}
QString Tools::getAboutIcons(){
    //qDebug();
    return "<p>This application uses Google Material icons. See https://design.google.com/icons/.</p>";
}

QString Tools::getStringFromFile(QString name) {
    //qDebug();
    QResource r{name};
    QByteArray b{reinterpret_cast<const char*>(r.data())};
    QFile file(name);
    if(!file.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "could not open file for reading";
    }
    QString text{QLatin1String(b)};
    //qDebug() << text;
    return text;
}

int Tools::getPort() {
    int remotePort = settings()->value("camremoteport", 50000).toInt();
    return remotePort;
}

void Tools::setPort(int port) {
    settings()->setValue("camremoteport", port);
}

QString Tools::getIp() {
    QString remoteIp = settings()->value("camremoteip", "127.0.0.1").toString();
    qDebug() << "Remote IP address is: " << remoteIp;
    return remoteIp;
}

void Tools::setIp(QString ip) {
    settings()->setValue("camremoteip", ip);
}

bool Tools::isFirstTime() {
    bool firstTime = settings()->value("firsttime", true).toBool();
    //if (firstTime) {
    // The value is set to memory and saved only when the application
    // quits (Tools object is destroyed) so that firstTime is true
    // for the whole first time use of the application
    m_firstTime = false;
    //settings()->setValue("firsttime", false);
    //}
    return firstTime;
}

bool Tools::getSettingBool(QString name, bool default_val) {
    return getSettingVariant(name, default_val).toBool();//settings()->value(name, default_val ).toBool();
}
QString Tools::getSettingString(QString name, QString default_val) {
    return getSettingVariant(name, default_val).toString();//settings()->value(name, default_val ).toString();
}
int Tools::getSettingInt(QString name, int default_val) {
    return getSettingVariant(name, default_val).toInt();//settings()->value(name, default_val ).toInt();
}

void Tools::setSettingBool(QString name, bool value) {
    settings()->setValue(name, value);
}
void Tools::setSettingString(QString name, QString value) {
    settings()->setValue(name, value);
}
void Tools::setSettingInt(QString name, int value) {
    settings()->setValue(name, value);
}

bool Tools::isDarkTheme() {
    if (Tools::getSettingString("uistyle", "Default")  == "Default") {
        return false;
    } else {
        return Tools::getSettingBool("dark", false);
    }
}

void Tools::setSettingStringList(QString name, QStringList value) {
    settings()->setValue(name, value);
}

QStringList Tools::getSettingStringList(QString name, QStringList default_val, bool interpretEmpty) {
    QStringList l = getSettingVariant(name, default_val).toStringList();//settings()->value(name, default_val).toStringList();
    // key= with no contents is normally interpreted as QString("") and the list isn't empty, here it's interpreted as empty
    // (saving empty list creates key=@Invalid() in the .ini file which is ugly and unintuitive for manual editing)
    if (interpretEmpty && l.size() == 1 && l.at(0) == "") {
        l.clear();
    }
    return l;
}

QSettings* Tools::m_settings = 0;
bool Tools::m_firstTime = true;
QSettings* Tools::m_readonly_settings = 0;

QSettings* Tools::settings() {
    if (!m_settings) {
        m_settings = new QSettings(QSettings::IniFormat, QSettings::UserScope,
                                   "VP-Systems", "CAMremoteControl");
    }
    return m_settings;
}

QVariant Tools::getSettingVariant(QString name, QVariant default_val)
{
    if (m_readonly_settings->contains(name)) {
        return m_readonly_settings->value(name, default_val);
    }
    return settings()->value(name, default_val );
}
