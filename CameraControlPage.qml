/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Controls 2.0
import eu.vpsystems.camremote 1.0 // Camera for its enum, registered in main.cpp

CameraControlPageForm {

    FontMetrics {
        id: metrics
        font: buttonServoDown.font
        property real servoButtonTextWidth: boundingRect("RIGHTI").width
        property real camButtonTextWidth: boundingRect("ZOOM M").width
    }
    onVisibleChanged: {
        console.log("camcontrol page visible changed:", visible)
        if (visible) {
            //label.visible = false
        } else {
        }
    }

    function sendCommand(command) {
        console.log("doesn't yet send commands")
    }
    function sendServoCommand(command, x, y) {
        camera.sendServoCommand(command, x, y)
    }

    // ------------------------ Pan/tilt servo control buttons ---------------------------
    buttonServoDown.onPressedPosition:  {
        console.log("servo down pressed", Camera.ServoDown)
        label.text = "" + mousePosition.x + " " + mousePosition.y
        sendServoCommand(Camera.ServoDown, mousePosition.x, mousePosition.y)
    }
    buttonServoDown.onReleased:  {
        console.log("servo down released")

    }
    buttonServoDown.onMousePositionChanged: {
        console.log("servo down position", mousePosition.x, mousePosition.y)
        label.text = "" + mousePosition.x + " " + mousePosition.y
    }

    buttonServoUp.onPressedPosition:  {
        console.log("servo up pressed")
    }
    buttonServoUp.onReleased:  {
        console.log("servo up released")

    }
    buttonServoUp.onMousePositionChanged: {
        console.log("servo up position", mousePosition.x, mousePosition.y)
    }

    buttonServoLeft.onPressedPosition:  {
        console.log("servo left pressed")

    }
    buttonServoLeft.onReleased:  {
        console.log("servo left released")
    }
    buttonServoLeft.onMousePositionChanged: {
        console.log("servo left position", mousePosition.x, mousePosition.y)
    }

    buttonServoRight.onPressedPosition:  {
        console.log("servo Right pressed")
    }
    buttonServoRight.onReleased:  {
        console.log("servo Right released")
    }
    buttonServoRight.onMousePositionChanged: {
        console.log("servo Right position", mousePosition.x, mousePosition.y)
    }

    // ---------------------------- Camera control buttons -------------------------------------
    buttonShoot.onClicked: {
        sendCommand(camera.CameraShoot)
    }
    switchRec.onCheckedChanged: {
        //sendCommand(camera.)
    }

    buttonZoomIn.onClicked: {
        sendCommand(camera.CameraZoomIn)
    }
    buttonZoomOut.onClicked: {
        sendCommand(camera.CameraZoomOut)
    }
    buttonTvMinus.onClicked: {
        sendCommand(camera.CameraTvMinus)
    }
    buttonTvPlus.onClicked: {
        sendCommand(camera.CameraTvPlus)
    }
    buttonAvMinus.onClicked: {
        sendCommand(camera.CameraAvMinus)
    }
    buttonAvPlus.onClicked: {
        sendCommand(camera.CameraAvPlus)
    }
    buttonIsoMinus.onClicked: {
        sendCommand(camera.CameraIsoMinus)
    }
    buttonIsoPlus.onClicked: {
        sendCommand(camera.CameraIsoPlus)
    }
    buttonMfMinus.onClicked: {
        sendCommand(camera.CameraMfMinus)
    }
    buttonMfPlus.onClicked: {
        sendCommand(camera.CameraMfPlus)
    }
}
