/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQml.StateMachine 1.0 as DSM
import "components"

Control {
    id: monitoringPage
    contentItem: Flickable {
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.AutoFlickDirection
        contentHeight: contentItem.childrenRect.height + 10
        ColumnLayout {
            id: columnLayout2
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

            NoConnection {
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                Layout.fillWidth: true
            }

            GroupBox {
                id: groupBoxChannels
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.leftMargin: 5
                Layout.rightMargin: 5
                Layout.fillWidth: true
                title: qsTr("Channels 0...3")

                GridLayout {
                    id: layoutChannels
                    anchors.fill: parent
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                    Layout.fillWidth: true
                    rowSpacing: 0
                    rows: 3
                    columns: 6

                    Repeater {
                        id: repeaterChannelsText
                        model: 4
                        Label {
                            Layout.row: index < 2 ? index : index - 2
                            Layout.column: index < 2 ? 0 : 3
                            Layout.leftMargin: 5
                            text: ""+(index+1)+":"
                        }
                    }
                    Repeater {
                        id: repeaterChannelsNumericValue
                        model: 4
                        MonitoringLabel {
                            Layout.row: index < 2 ? index : index - 2
                            Layout.column: index < 2 ? 1 : 4
                        }
                    }

                    Repeater {
                        id: repeaterChannelsGauge
                        model: 4
                        ProgressBar {
                            from: 1.00
                            to: 2.00
                            Layout.fillWidth: true
                            Layout.minimumWidth: 20
                            Layout.rightMargin: 10
                            Layout.row: index < 2 ? index : index - 2
                            Layout.column: index < 2 ? 2 : 5
                        }
                    }
                }
            }//GroupBox

            GroupBox {
                id: groupBoxSbus
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.leftMargin: 5
                Layout.rightMargin: 5
                Layout.fillWidth: true
                title: qsTr("S.BUS on channel 0")

                GridLayout {
                    id: layoutSbus
                    anchors.fill: parent
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                    Layout.fillWidth: true
                    rowSpacing: 0
                    rows: 8
                    columns: 6

                    Repeater {
                        id: repeaterSbusText
                        model: 16
                        Label {
                            Layout.row: index < 8 ? index : index - 8
                            Layout.column: index < 8 ? 0 : 3
                            Layout.leftMargin: 5
                            text: ""+(index+1)+":"
                        }
                    }
                    Repeater {
                        id: repeaterSbusNumericValue
                        model: 16
                        MonitoringLabel {
                            Layout.row: index < 8 ? index : index - 8
                            Layout.column: index < 8 ? 1 : 4
                        }

                    }
                    Repeater {
                        id: repeaterSbusGauge
                        model: 16
                        ProgressBar {
                            from: 1.00
                            to: 2.00
                            Layout.fillWidth: true
                            Layout.minimumWidth: 20
                            Layout.rightMargin: 10
                            Layout.row: index < 8 ? index : index - 8
                            Layout.column: index < 8 ? 2 : 5
                        }
                    }
                }
            } //GroupBox

            RowLayout {
                id: valueTypeChoice

                Component.onCompleted: {
                    if (tools.getSettingBool("monitoringpercent", false)) {
                        radioPercent.checked = true
                    }
                }

                RadioButton {
                    id: radioMillisecond
                    checked: true
                    text: "ms"
                }

                RadioButton {
                    id: radioPercent
                    text: "% (0% 1ms, 100% 2ms)";
                    Layout.preferredWidth: monitoringPage.width - radioMillisecond.width -10
                    Component.onCompleted: {
                        contentItem.wrapMode = Text.WordWrap
                    }
                    onCheckedChanged: {
                        if (checked) {
                            tools.setSettingBool("monitoringpercent", true)
                        } else {
                            tools.setSettingBool("monitoringpercent", false)
                        }
                    }
                }
            } //RowLayout valueTypeChoice
        }
    } //Flickable
    Connections {
        target: monitoringData
        onValuesUpdated: {
            //console.log("monitoring values")
            var i
            var val
            for (i in sbus){
                val = sbus[i]
                repeaterSbusGauge.itemAt(i).value = val
                if (radioPercent.checked) {
                    val = (val < 1.0) ? 1.0 : val
                    val = (val - 1) * 100
                    val = val.toFixed(0)
                }
                else {
                    val = val.toFixed(2)
                }
                repeaterSbusNumericValue.itemAt(i).text = val.toString()
            }
            for (i in ch) {
                val = ch[i]
                repeaterChannelsGauge.itemAt(i).value = val
                if (radioPercent.checked) {
                    val = (val < 1.0) ? 1.0 : val
                    val = (val - 1) * 100
                    val = val.toFixed(0)
                }
                else {
                    val = val.toFixed(2)
                }
                repeaterChannelsNumericValue.itemAt(i).text = val.toString()

            }
            //resistorGauge.value = resistor
        }
    }
    Connections {
        target: connection
        onConnectedStatusChanged: {
            console.log("connection.onConnectedStatusChanged, ", connected)
            if (!connected) {
                var i
                for (i = 0; i < repeaterSbusGauge.model; i++ ) {
                    repeaterSbusGauge.itemAt(i).value = 0
                }
                for (i = 0; i < repeaterChannelsGauge.model; i++ ) {
                    repeaterChannelsGauge.itemAt(i).value = 0
                }
                //resistorGauge.value = 0
            }
        }
    }

    signal signalStartMonitoring
    signal signalStopMonitoring
    onVisibleChanged: {
        console.log("monitoring page visible changed:", visible)
        if (visible) {
            // Start asking data from device. Do it through a zero-timer which puts the command last
            // in the event queue, so that the state machine reaches the correct state first.
            monitoringPageStateMachine.running = true
            zeroTimer.running = true // fires startMonitoring() after state machine has really started
            return
        } else {
            // stop asking data from device
            signalStopMonitoring()
        }
    }
    Timer {
        id: zeroTimer
        interval: 0; running: false; repeat: false
        onTriggered: {monitoringPage.signalStartMonitoring()}
    }

    DSM.StateMachine {
        id: monitoringPageStateMachine
        running: false
        initialState: notMonitoring

        onFinished: {
            console.log("monitoring page state machine finished")
            monitoringData.stopUpdatingValues()
        }
        DSM.FinalState {
            id: finalState
        }

        DSM.SignalTransition {
            signal: connection.connectionError
            targetState: finalState
        }
        DSM.SignalTransition {
            signal: monitoringPage.signalStopMonitoring
            targetState: finalState
        }

        DSM.State {
            id: notMonitoring
            onEntered: {
                console.log("entered state 'notMonitoring'")
            }
            DSM.SignalTransition {
                signal: monitoringPage.signalStartMonitoring
                targetState: notMonitoring
                onTriggered: {
                    console.log()
                    connection.connectToCamremote()
                }
            }
            DSM.SignalTransition {
                signal: connection.connected
                targetState: monitoring
            }
            DSM.SignalTransition {
                signal: connection.connectionError
                targetState: finalState
            }
            DSM.SignalTransition {
                signal: monitoringPage.signalStopMonitoring
                targetState: finalState
            }
        }

        DSM.State {
            id: monitoring
            onEntered: {
                console.log("entered state 'monitoring'")
                monitoringData.startUpdatingValues(tools.getSettingInt("monitoringrate", 200))
            }
            DSM.SignalTransition {
                signal: connection.connectionError
                targetState: finalState
            }
            DSM.SignalTransition {
                signal: monitoringPage.signalStopMonitoring
                targetState: finalState
            }
        }

    } // StateMachine
}//Control
