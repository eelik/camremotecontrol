/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

Item {
    id: item1
    //implicitWidth: 300

    property int standardButtons

    ColumnLayout {
        id: columnLayout1
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        spacing: 10
        Label {
            id: text1
            text: qsTr("Connecting...")
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            Layout.maximumWidth: 350
            Layout.minimumWidth: 150
            wrapMode: Text.WordWrap
        }

        BusyIndicator {
            id: busyIndicator1
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    states: [
        State {
            name: "failedState"

            PropertyChanges {
                target: item1
                visible: true
            }
            PropertyChanges {
                target: busyIndicator1
                opacity: 0
            }

            PropertyChanges {
                target: text1
                text: qsTr("Connection failed")
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Ok
            }
        },
        State {
            name: "connectingState"

            PropertyChanges {
                target: item1
                visible: true
            }
            PropertyChanges {
                target: item1
                standardButtons: Dialog.Cancel
            }

            PropertyChanges {
                target: text1
                text: qsTr("Connecting...")
            }

            PropertyChanges {
                target: busyIndicator1
                opacity: 1
            }
        },
        State {
            name: "noState"
            PropertyChanges {
                target: item1
                visible: false
            }
        }

    ]
}
