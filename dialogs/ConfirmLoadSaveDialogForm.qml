/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import QtQuick 2.4
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

Item {
    id: item1
    //implicitWidth: 300

    property string defaultProfile: "0"

    property int standardButtons: Dialog.Ok|Dialog.Cancel

    ColumnLayout {
        id: columnLayout
        anchors.top: parent.top
        
        anchors.right: parent.right
        anchors.left: parent.left

        RowLayout {
            id: rowLayout2
    
            Label {
                id: text1
                text: qsTr(" \n \n ")
            
                horizontalAlignment: Text.AlignHCenter
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.maximumWidth: 350
                Layout.minimumWidth: 150
                wrapMode: Text.WordWrap
            }
            
            Text {
                opacity: 0
                width: 0
                text: "\n\n "
            }
        }
        
        BusyIndicator {
            id: busyIndicator
            Layout.alignment: Qt.AlignHCenter
        }
    }
    states: [
        State {
            name: "confirmLoad"

            PropertyChanges {
                target: text1
                text: "Settings will be loaded from the device. Local settings will be overwritten. Continue?"
            }

            PropertyChanges {
                target: busyIndicator
                opacity: 0
                enabled: false
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Cancel|Dialog.Ok
            }
        },
        State {
            name: "confirmSave"

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Cancel|Dialog.Ok
            }
            PropertyChanges {
                target: text1
                text: "Settings in the device will be overwritten. " + (defaultProfile === "" ? "" :("Profile number " + defaultProfile + " will be the default profile. ")) + "Continue?"
            }

            PropertyChanges {
                target: busyIndicator
                opacity: 0
                enabled: false
            }
        },
        State {
            name: "saving"

            PropertyChanges {
                target: busyIndicator
                opacity: 1
                enabled: true
            }

            PropertyChanges {
                target: text1
                text: "Saving..."
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.NoButton
            }
        },
        State {
            name: "loading"

            PropertyChanges {
                target: text1
                text: "Loading..."
            }

            PropertyChanges {
                target: busyIndicator
                opacity: 1
                enabled: true
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.NoButton
            }
        },
        State {
            name: "saved"

            PropertyChanges {
                target: text1
                text: "Settings were sent to the device."
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Ok
            }

            PropertyChanges {
                target: busyIndicator
                enabled: false
                opacity: 0
            }
        },
        State {
            name: "loaded"

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Ok
            }

            PropertyChanges {
                target: text1
                text: "Settings loaded."
            }

            PropertyChanges {
                target: busyIndicator
                enabled: false
                opacity: 0
            }
        },
        State {
            name: "saveFailed"

            PropertyChanges {
                target: text1
                text: "Saving failed."
            }

            PropertyChanges {
                target: busyIndicator
                enabled: false
                opacity: 0
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Ok
            }
        },
        State {
            name: "loadFailed"
            PropertyChanges {
                target: text1
                text: "Loading failed."
            }

            PropertyChanges {
                target: busyIndicator
                opacity: 0
                enabled: false
            }

            PropertyChanges {
                target: item1
                standardButtons: Dialog.Ok
            }
        }
    ]
}
