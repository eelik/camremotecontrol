/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQml.StateMachine 1.0 as DSM

Dialog {

    id: dialogWindow
    modal:true
    focus: true
    closePolicy:  Popup.CloseOnEscape
    property string state: "noState"

    footer: DialogButtonBox {
        standardButtons: dialogContent.standardButtons
        Component.onCompleted: {
            onStandardButtonsChanged()
        }
        onStandardButtonsChanged: {
            if (standardButton(Dialog.Ok)) standardButton(Dialog.Ok).text = "OK"
            if (standardButton(Dialog.Cancel)) standardButton(Dialog.Cancel).text = "Cancel"
        }
    }

    /*
     * The logic of this dialog is actually started here. When connection backend has been commanded to connect,
     * it sends this signal if not already connected.
     */
    Connections {
        target: connection
        onTryingToConnect: {
            console.log("connectingDialog received signal 'tryingToConnect'")
            connectingStateMachine.running = true
        }
    }

    // The visible content is in the declarative .ui.qml file
    contentItem: ConnectingDialogForm {
        state: dialogWindow.state // the content has states to change the visible UI
        id: dialogContent
    }

    /*
     * The state machine steers the logic and interaction between connection backend, user input and UI.
     * It opens the dialog window if needed.
     * Possible cancelling is done through the backend and the items which need connection get signals
     * from the backend, not from this dialog.
     */
    DSM.StateMachine {
        id: connectingStateMachine
        initialState: connecting
        onFinished: {
            console.log("connecting dialog state machine finished")
            openDialogTimer.running = false
            if (dialogWindow.visible === true) dialogWindow.close()
            dialogWindow.state = "no"
        }

        DSM.State {

            id: connecting
            initialState: initiatingConnection

            signal signalNoConnection()
            signal signalConnected()

            Timer {
                id: openDialogTimer
                interval: 200; running: false; repeat: false
                onTriggered: {
                    console.log("openDialogTimer (200ms) triggered")
                    if ((connection.isConnected() === false) && dialogWindow.visible === false) {
                        //dialogWindow.state = "connectingState"
                        dialogWindow.open()
                    }
                }
            }

            Timer {
                id: connectionOkTimer
                interval: 0; running: false; repeat: false
                onTriggered: {
                    console.log("connectionOkTimer (0) triggered")
                    connecting.signalConnected()
                }
            }

            onEntered: {
                console.log("entered state 'connecting'")
                console.log("is connected? ", connection.isConnected(), "is unconnected?", connection.isUnconnected())
            }

            DSM.State {
                id: initiatingConnection
                onEntered: {
                    console.log("entered state 'initiatingConnection'")
                    console.log("is connected? ", connection.isConnected(), "is unconnected?", connection.isUnconnected())
                    // The connection.connected()
                    // signal isn't necessarily caught in every situation, so we check the connection status here.
                    if (connection.isConnected() === false) {
                        // Connection isn't established yet, so open the dialog.
                        // It's opened only after some delay, connecting may succeed before that
                        // and the user doesn't have to see the dialog.
                        // The dialog content (ConnectingDialogForm) state is changed first.
                        dialogWindow.state = "connectingState"
                        openDialogTimer.running = true
                    } else {
                        // Is connected already. Start a zero-timer which puts the signal last in the event queue,
                        // it will be caught later and the state will be changed to final.
                        connectionOkTimer.running = true
                    }
                }

                DSM.SignalTransition {
                    signal: dialogWindow.rejected
                    targetState: finalState
                    onTriggered: {
                        console.log("buttonCancel when connecting was triggered")
                        connection.cancelConnecting()
                    }
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: connectionFailed
                    onTriggered: {
                        console.log("signal connection.connectionError triggered")
                    }
                }
                DSM.SignalTransition {
                    signal: connection.connected
                    targetState: finalState
                    onTriggered: {
                        console.log("signal connection.connected triggered")
                    }
                }
                DSM.SignalTransition {
                    signal: connecting.signalConnected // zero-timer from this state machine
                    targetState: finalState
                    onTriggered: {
                        console.log("signal connecting.signalConnected (zero timer) triggered")

                    }
                }
                DSM.SignalTransition {
                    signal: connection.connectionCancelled
                    targetState: finalState
                    onTriggered: {
                        console.log("signal connection.connectionCancelled triggered")

                    }
                }

            } // state initiatingConnection

            // Wait for user acknowledging failed connection
            DSM.State {
                id: connectionFailed
                onEntered: {
                    console.log("entered state 'connectionFailed'")
                    dialogWindow.state = "failedState"
                    if (dialogWindow.visible === false) dialogWindow.open()
                    //dialogWindow.state = "failedState"
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: finalState
                }
            }
        } // State "connecting"
        DSM.FinalState {
            id: finalState

        }
    } // state machine
}
