import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import QtQml.StateMachine 1.0 as DSM

Dialog {
    id: deviceTimeDialog
    modal: true
    focus: true
    closePolicy:  Popup.CloseOnEscape

    property string time: "00:00"

    /*
     * The logic of this dialog is started with this functions.
     */
    function beginSave() {
        setTimeStateMachine.initialState = saving;
        setTimeStateMachine.running = true;
    }
    footer: DialogButtonBox {
        standardButtons: dialogContent.standardButtons
        Component.onCompleted: {
            onStandardButtonsChanged()
        }
        onStandardButtonsChanged: {
            if (standardButton(Dialog.Ok)) standardButton(Dialog.Ok).text = "OK"
            if (standardButton(Dialog.Cancel)) standardButton(Dialog.Cancel).text = "Cancel"
        }
    }

    contentItem: Item {
        id: dialogContent
        property int standardButtons: Dialog.Ok|Dialog.Cancel
        Timer {
            interval: 200; running: true; repeat: true
            onTriggered: {
                var date = new Date()
                timeLabel.text = date.toDateString() + "\n" + date.toTimeString()//"" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()
            }
        }

        ColumnLayout {
            id: columnLayout
            anchors.top: parent.top

            anchors.right: parent.right
            anchors.left: parent.left


            RowLayout {
                id: rowLayout2

                Label {
                    id: text1
                    text: qsTr(" \n \n ")

                    horizontalAlignment: Text.AlignHCenter
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.maximumWidth: 350
                    Layout.minimumWidth: 150
                    wrapMode: Text.WordWrap
                }

                Text {
                    opacity: 0
                    width: 0
                    text: "\n\n "
                }
            }
            Label {
                horizontalAlignment: Text.AlignHCenter
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillWidth: true
                id: timeLabel
            }

            BusyIndicator {
                id: busyIndicator
                Layout.alignment: Qt.AlignHCenter
            }
        }
        // States for the UI (not the logic, which is in DSM.StateMachine)
        states: [
            State {
                name: "confirmSave"

                PropertyChanges {
                    target: dialogContent
                    standardButtons: Dialog.Ok|Dialog.Cancel
                }

                PropertyChanges {
                    target: text1
                    text: "Set the device time to the current time?"
                }
                PropertyChanges {
                    target: timeLabel
                    visible: true

                }
                PropertyChanges {
                    target: busyIndicator
                    opacity: 0
                    enabled: false
                }
            },
            State {
                name: "saving"

                PropertyChanges {
                    target: busyIndicator
                    opacity: 1
                    enabled: true
                }

                PropertyChanges {
                    target: text1
                    text: "Setting the time..."
                }

                PropertyChanges {
                    target: dialogContent
                    standardButtons: Dialog.NoButton
                }
                PropertyChanges {
                    target: timeLabel
                    visible: false
                }
            },
            State {
                name: "saved"

                PropertyChanges {
                    target: text1
                    text: "Time was set."
                }

                PropertyChanges {
                    target: dialogContent
                    standardButtons: Dialog.Ok
                }

                PropertyChanges {
                    target: busyIndicator
                    enabled: false
                    opacity: 0
                }
                PropertyChanges {
                    target: timeLabel
                    visible: false
                }
            },
            State {
                name: "saveFailed"

                PropertyChanges {
                    target: text1
                    text: "Setting time failed."
                }

                PropertyChanges {
                    target: busyIndicator
                    enabled: false
                    opacity: 0
                }
                PropertyChanges {
                    target: dialogContent

                    standardButtons: Dialog.Ok
                }
                PropertyChanges {
                    target: timeLabel
                    visible: false
                }
            }
        ]
    }

    /*
     * The state machine steers the logic and interaction between the backend, user input and UI.
     * It opens the dialog window if there's there's connection to CAMremote
     * and saves the time through the backend.
     */
    DSM.StateMachine {
        id: setTimeStateMachine
        running: false
        initialState: saving

        DSM.FinalState {
            id: finalState
            onEntered: {
                console.log("final state entered")
            }
        }

        onFinished: {
            console.log("setTimeStateMachine finished")
            deviceTimeDialog.close()
        }

        // The user clicked "Save"
        DSM.State {
            id: saving
            initialState: waitingForConnectionBeforeSaving

            onEntered: {
                console.log("entered state 'saving'")
                //connection.connectToCamremote() // answers with a signal
            }

            DSM.State {
                id: waitingForConnectionBeforeSaving
                onEntered: {
                    console.log("entering state 'waitingForConnectionBeforeSaving', send 'connect'")
                    connection.connectToCamremote()
                }

                DSM.SignalTransition {
                    signal: connection.connected
                    targetState: confirmingSave
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: finalState
                }
                DSM.SignalTransition {
                    signal: connection.connectionCancelled
                    targetState: finalState
                }
            }

            // Waiting for user to click ok or cancel
            DSM.State {
                id: confirmingSave

                onEntered: {
                    console.log("entered state 'confirmingSave'")
                    deviceTimeDialog.contentItem.state = "confirmSave" //dialogWindow.state = "confirmSave"
                    deviceTimeDialog.open()
                }
                DSM.SignalTransition {
                    signal: deviceTimeDialog.accepted //dialogWindow.okClicked
                    targetState: doingSave
                }
                DSM.SignalTransition {
                    signal: deviceTimeDialog.rejected //dialogWindow.cancelClicked
                    targetState: finalState
                }
            } // State "saving"/"confirming"

            // waiting for backend to send 'save' message to camremote
            DSM.State {
                id: doingSave
                onEntered: {
                    console.log("entered state 'doingSave'")
                    // Here the actual saving to camremote is done
                    deviceTimeDialog.contentItem.state = "saving" // dialogWindow.state = "saving"
                    deviceTimeDialog.open()
                    connection.setTime()
                }

                DSM.SignalTransition {
                    signal: connection.messageSent
                    targetState: notifySavedSuccess
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: notifySaveFailed
                }
                // Possibly add timed transition?
            } // State "saving"/"doingSave"

            // Waiting for user to acknowledge successful saving
            DSM.State {
                id: notifySavedSuccess
                onEntered: {
                    console.log("entered state 'notifySavedSuccess'")

                    deviceTimeDialog.contentItem.state = "saved" // dialogWindow.state = "saved"
                    deviceTimeDialog.open()
                }
                DSM.SignalTransition {
                    signal: deviceTimeDialog.accepted
                    targetState: finalState

                }
            }
            // Waiting for user to acknowledge failed saving
            DSM.State {
                id: notifySaveFailed
                onEntered: {
                    console.log("entered state 'notifySaveFailed'")
                    deviceTimeDialog.contentItem.state = "saveFailed" // dialogWindow.state = "saveFailed"
                }
                DSM.SignalTransition {
                    signal: deviceTimeDialog.accepted//signal: dialogWindow.okClicked
                    targetState: finalState
                }
            } // State "saving"/"notifySaveFailed"

        }  // State "saving"
    } //StateMachine

}
