/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQml.StateMachine 1.0 as DSM

Dialog {
    id: dialogWindow
    modal: true
    focus: true
    closePolicy:  Popup.CloseOnEscape
    property string defaultProfile
    property int numberOfProfilesToSave

    /*
     * The logic of this dialog is started with these functions.
     */
    function beginSave(numberOfProfilesToSave) {
        loadSaveStateMachine.initialState = saving;
        dialogWindow.numberOfProfilesToSave = numberOfProfilesToSave
        loadSaveStateMachine.running = true;
    }
    function beginLoad() {
        loadSaveStateMachine.initialState = loading;
        loadSaveStateMachine.running = true;
    }

    footer: DialogButtonBox {
        standardButtons: dialogContent.standardButtons
        Component.onCompleted: {
            onStandardButtonsChanged()
        }
        onStandardButtonsChanged: {
            if (standardButton(Dialog.Ok)) standardButton(Dialog.Ok).text = "OK"
            if (standardButton(Dialog.Cancel)) standardButton(Dialog.Cancel).text = "Cancel"
        }
    }

    // The visible content is in the declarative .ui.qml file
    contentItem: ConfirmLoadSaveDialogForm {
        id: dialogContent
        defaultProfile: dialogWindow.defaultProfile
    }

    /*
     * The state machine steers the logic and interaction between the backend, user input and UI.
     * It opens the dialog window if there's there's connection to CAMremote
     * and saves/loads the profiles through the backend.
     */
    DSM.StateMachine {
        id: loadSaveStateMachine
        running: false
        initialState: saving

        DSM.FinalState {
            id: finalState
        }

        onFinished: {
            console.log("loadSaveStateMachine finished")
            dialogWindow.close()
        }

        // The user clicked "Save"
        DSM.State {
            id: saving
            initialState: waitingForConnectionBeforeSaving

            onEntered: {
                console.log("entered state 'saving'")
                //connection.connectToCamremote() // answers with a signal
            }

            DSM.State {
                id: waitingForConnectionBeforeSaving
                onEntered: {
                    console.log("entering state 'waitingForConnectionBeforeSaving', send 'connect'")
                    connection.connectToCamremote()
                }

                DSM.SignalTransition {
                    signal: connection.connected
                    targetState: confirmingSave
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: finalState
                }
                DSM.SignalTransition {
                    signal: connection.connectionCancelled
                    targetState: finalState
                }
            }

            // Waiting for user to click ok or cancel
            DSM.State {
                id: confirmingSave

                onEntered: {
                    console.log("entered state 'confirmingSave'")
                    dialogContent.state = "confirmSave" //dialogWindow.state = "confirmSave"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: doingSave
                }
                DSM.SignalTransition {
                    signal: dialogWindow.rejected
                    targetState: finalState
                }
            } // State "saving"/"confirming"

            // waiting for backend to send 'save' message to camremote
            DSM.State {
                id: doingSave
                onEntered: {
                    console.log("entered state 'doingSave'")
                    // Here the actual saving to camremote is done
                    dialogContent.state = "saving" // dialogWindow.state = "saving"
                    dialogWindow.open()
                    profileData.save(dialogWindow.numberOfProfilesToSave)
                    profileData.saveToSettings()
                }

                DSM.SignalTransition {
                    signal: connection.messageSent
                    targetState: notifySavedSuccess
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: notifySaveFailed
                }
                // Possibly add timed transition like in doingLoad?
            } // State "saving"/"doingSave"

            // Waiting for user to acknowledge successful saving
            DSM.State {
                id: notifySavedSuccess
                onEntered: {
                    console.log("entered state 'notifySavedSuccess'")
                    dialogContent.state = "saved" // dialogWindow.state = "saved"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: finalState
                }
            }
            // Waiting for user to acknowledge failed saving
            DSM.State {
                id: notifySaveFailed
                onEntered: {
                    console.log("entered state 'notifySaveFailed'")
                    dialogContent.state = "saveFailed" // dialogWindow.state = "saveFailed"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: finalState
                }
            } // State "saving"/"notifySaveFailed"

        }  // State "saving"


        // The user clicked "Load".
        // This state is almost identical to "Save".
        DSM.State {
            id: loading
            initialState: waitingForConnectionBeforeLoading

            onEntered: {console.log("entered state 'loading'")}

            DSM.State {
                id: waitingForConnectionBeforeLoading
                onEntered: {
                    console.log("entered state 'waitingForConnectionBeforeLoading, send 'connect'")
                    connection.connectToCamremote() // answers with a signal
                }
                DSM.SignalTransition {
                    signal: connection.connected
                    targetState: confirmingLoad
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: finalState
                }
                DSM.SignalTransition {
                    signal: connection.connectionCancelled
                    targetState: finalState
                }
            }

            // Waiting for user to click ok or cancel
            DSM.State {
                id: confirmingLoad

                onEntered: {
                    console.log("entered state 'confirmingLoad'")
                    dialogContent.state = "confirmLoad" //dialogWindow.state = "confirmLoad"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: doingLoad
                }
                DSM.SignalTransition {
                    signal: dialogWindow.rejected
                    targetState: finalState
                }
            } // State "loading"/"confirming"

            // waiting for backend to send 'Load' message to camremote
            DSM.State {
                id: doingLoad
                onEntered: {
                    console.log("entered state 'doingLoad'")
                    // Here the actual loading to camremote is done
                    profileData.load()
                    dialogContent.state = "loading"//dialogWindow.state = "loading"
                    dialogWindow.open()
                }

                DSM.SignalTransition {
                    signal: profileData.profilesLoaded
                    targetState: notifyLoadedSuccess
                }
                DSM.SignalTransition {
                    signal: connection.connectionError
                    targetState: notifyLoadFailed
                }
                // The answer should come from the server within couple of seconds
                DSM.TimeoutTransition {
                    targetState: notifyLoadFailed
                    timeout: 5000
                }
            } // State "loading"/"doingLoad"

            // Waiting for user to acknowledge successful loading
            DSM.State {
                id: notifyLoadedSuccess
                onEntered: {
                    console.log("entered state 'notifyLoadedSuccess'")
                    dialogContent.state = "loaded" //dialogWindow.state = "loaded"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: finalState
                }
            }
            // Waiting for user to acknowledge failed loading
            DSM.State {
                id: notifyLoadFailed
                onEntered: {
                    console.log("entered state 'notifyLoadFailed'")
                    dialogContent.state = "loadFailed" //dialogWindow.state = "loadFailed"
                    dialogWindow.open()
                }
                DSM.SignalTransition {
                    signal: dialogWindow.accepted
                    targetState: finalState
                }
            } // State "loading"/"notifyLoadFailed"

        }  // State "loading"
    }

}
