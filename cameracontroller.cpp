/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cameracontroller.h"
#include "connection.h"
#include "firmware/types.h"

CameraController::CameraController(Connection *a_conn, QObject *parent) :
    QObject(parent),
    m_connection{a_conn}
{}

/*
 * Sends a command to camera using Connection. The actual protocol is implemented in Connection.
 */
void CameraController::sendCommand(CameraController::Command a_command) const {
    m_connection->sendMessage(static_cast<Connection::MessageType>(a_command));
}

/*
 * Sends a command to camera using Connection. The actual protocol except for
 * coordinates is implemented in Connection.
 */
void CameraController::sendServoCommand(CameraController::Command a_command, float a_x, float a_y) const {
    qDebug() << "servo command" << static_cast<int>(a_command) << a_x << a_y;
    UINT16 coords[] = {static_cast<UINT16>(a_x), static_cast<UINT16>(a_y)};
    char* ptr = reinterpret_cast<char*>(coords);
    if (a_command >= CameraController::ServoUp && a_command <= CameraController::ServoRight) {
        m_connection->sendMessage(static_cast<Connection::MessageType>(a_command), QByteArray().append(ptr, 4));
    }
}
