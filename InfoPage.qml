/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2
import QtQml.StateMachine 1.0 as DSM

Control {
    id: infoPage
    property Component infoPageComponent: infoPageTextArea
    property Component pageholder: pageholder
    property string text: "About..."
    property Loader pageLoader: aboutTextLoader
    
    onVisibleChanged: {
        console.log("info page visible changed:", visible)
        
        if (visible) {
            // 'About' page and text are loaded only when needed
            // to save some memory and startup time
            console.log("load info")
            infoPage.text = (tools.getSettingString("appname", "CAMremoteControl") === "CAMremoteControl" ?tools.getAboutCRC():tools.getAboutWLC() ) +
                    "</br>" + tools.getAboutQt() + "</br>" + tools.getAboutIcons() +
                    "<br/><hr/></br>" + tools.getMIT() + "<br/><hr/></br>" + tools.getLGPL() +
                    "<br/><hr/></br>" + tools.getGPL()
            infoPage.pageLoader.sourceComponent = infoPage.infoPageComponent
        } else {
            //unload the About component
            infoPage.pageLoader.sourceComponent = infoPage.placeholder
        }
    }
    
    Loader {
        id: aboutTextLoader
        sourceComponent: placeholder
        anchors.fill: parent
    }
    // loaded when needed
    Component {
        id: infoPageTextArea
        Flickable {
            boundsBehavior: Flickable.StopAtBounds
            contentWidth: width
            contentHeight: text3.height
            
            Label {
                id: text3
                padding: 10
                text: infoPage.text
                anchors.left: parent.left
                anchors.right: parent.right
                // StyledText may be faster but doesn't save
                // much memory compared to RichText and doesn't show
                // the html properly
                textFormat: Text.RichText
                wrapMode: Text.WordWrap
            }
        }
    }
    Component {
        id: placeholder
        Text {padding: 10;text: "About CAMremoteControl..."}
    }
    
    
}
