/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef DATA_H
#define DATA_H

#include <QObject>
#include <QStandardItemModel>
#include <QQmlContext>

#include "profileitem.h"


class Connection;

class ProfileData : public QObject
{
    Q_OBJECT

    friend class ProfileItem;
protected:
    CAMPROFILEV00 m_rawProfiles[7];
    CAMPROFILEV01* m_rawProfile01;
    Connection* m_connection;

    // these are used in ProfileItems (maybe should be changed so that public isn't needed...)
    QQmlContext* context;
    CAMPROFILEV00* currentProfile;
    QHash<QString, ProfileItem*> profileItems;

public:
    explicit ProfileData(QQmlContext* ctx, Connection* connection, QObject *parent = 0);
    ~ProfileData();

public slots:
    void onItemChanged(QString name, int index, bool v01profile=false);
    virtual void onCameraChanged(int index);
    virtual void onProfileChanged(int index);
    void slotProfilesAcquired(const QByteArray profiles);
    virtual void save(int numberOfProfilesToSave) const;
    virtual void saveToSettings() const;
    virtual void load(bool fromRemote = true);
    virtual void loadFromSettings();
    void onProfilesLoaded();

signals:
    void profilesLoaded(int active);

protected:
    virtual void initProfileItems();
    virtual void initFixedProfileItems();
    virtual void initTimerProfileItems();
    virtual void resetDependentItemModels();
    virtual void saveDependentModelsWithIndex0();
    virtual void notifyDependentModels() const;


};

#endif // DATA_H
