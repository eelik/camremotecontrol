/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import QtQuick 2.8
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0

import "components"


Control{
    //Flickable {
    //font: ApplicationWindow.window.font
    id: flickable1
    property alias comboBoxProfileNumber: comboBoxProfileNumber
    property alias comboBoxCameraName: comboBoxCameraName
    property alias comboBoxCableMethod: comboBoxCableMethod
    property alias comboBoxAEProgram: comboBoxAEProgram
    property alias comboBoxTv: comboBoxTv
    property alias comboBoxAv: comboBoxAv
    property alias comboBoxIsoSpeed: comboBoxIsoSpeed
    property alias comboBoxZoom: comboBoxZoom
    property alias comboBoxFocus: comboBoxFocus
    property alias comboBoxSize: comboBoxSize
    property alias comboBoxQuality: comboBoxQuality
    property alias comboBoxWhiteBalance: comboBoxWhiteBalance
    property alias comboBoxControlMode: comboBoxControlMode
    property alias comboBoxShootCnt: comboBoxShootCnt
    property alias comboBoxShootBefore: comboBoxShootBefore
    property alias comboBoxShootAfter: comboBoxShootAfter
    property alias buttonSave: buttonSave
    property alias buttonLoad: buttonLoad
    contentItem: Flickable{
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.AutoFlickDirection
        contentHeight: contentItem.childrenRect.height + 10



        GridLayout {
            id: gridLayoutMain
            anchors.rightMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.right: parent.right
            columns: 1

            GroupBox {
                font: ApplicationWindow.window.font
                id: groupBoxProfile
                Layout.fillWidth: true
                title: qsTr("Profiles")

                GridLayout {
                    id: gridLayoutProfile
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    rows: 1
                    columns: 2

                    RowLayout {
                        id: rowLayout1
                        //width: 100
                        //height: 100

                        Label {
                            id: labelProfileNumber
                            text: qsTr("Profile")
                        }

                        ComboBox {
                            id: comboBoxProfileNumber
                            implicitWidth: 80
                            currentIndex: profilenumberModel.currentIndex
                            model: profilenumberModel

                        }
                    }

                    RowLayout {
                        id: rowLayout2
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

                        ButtonWithImage {
                            id: buttonLoad
                            Layout.fillHeight: true
                            source: "/icons/ic_file_download_black_24dp.png"
                        }

                        ButtonWithImage {
                            id: buttonSave
                            Layout.fillHeight: true
                            source: "/icons/ic_file_upload_black_24dp.png"
                        }
                    }
                }
            }

            GroupBox {
                id: groupCameraSettings
                Layout.fillWidth: true

                title: qsTr("Camera settings")

                GridLayout {
                    id: gridLayoutCamera
                    anchors.fill: parent

                    columns: 2

                    Label {
                        id: labelCameraName
                        text: qsTr("Camera")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    WrappingComboBox {
                        id: comboBoxCameraName
                        currentIndex: cameranameModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: cameranameModel
                        wrap: false
                        extraWidth: Math.floor((gridLayoutCamera.width - comboBoxCameraName.width)/1.5)
                    }

                    Label {
                        id: labelCableMethod
                        text: qsTr("Method")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxCableMethod.visible
                    }

                    ComboBox {
                        id: comboBoxCableMethod
                        currentIndex: cablemethodModel.currentIndex
                        Layout.fillWidth: true
                        model: cablemethodModel
                        visible: count
                    }

                    Label {
                        id: labelAEProgram
                        text: qsTr("Program")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxAEProgram.visible
                    }

                    ComboBox {
                        id: comboBoxAEProgram
                        currentIndex: aeprogramModel.currentIndex
                        Layout.fillWidth: true
                        model: aeprogramModel
                        visible: count
                    }

                    Label {
                        id: labelTv
                        text: qsTr("Tv")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxTv.visible
                    }

                    ComboBox {
                        id: comboBoxTv
                        currentIndex: tvModel.currentIndex
                        Layout.fillWidth: true
                        model: tvModel
                        visible: count
                    }

                    Label {
                        id: labelAv
                        text: qsTr("Av")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxAv.visible
                    }

                    ComboBox {
                        id: comboBoxAv
                        currentIndex: avModel.currentIndex
                        Layout.fillWidth: true
                        model: avModel
                        visible: count
                    }

                    Label {
                        id: labelIsoSpeed
                        text: qsTr("ISO")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxIsoSpeed.visible
                    }

                    ComboBox {
                        id: comboBoxIsoSpeed
                        currentIndex: isospeedModel.currentIndex
                        Layout.fillWidth: true
                        model: isospeedModel
                        visible: count
                    }

                    Label {
                        id: labelZoom
                        text: qsTr("Zoom")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxZoom.visible
                    }

                    ComboBox {
                        id: comboBoxZoom
                        currentIndex: zoomModel.currentIndex
                        Layout.fillWidth: true
                        model: zoomModel
                        visible: count
                    }

                    Label {
                        id: labelFocus
                        text: qsTr("Focus")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxFocus.visible
                    }

                    WrappingComboBox {
                        id: comboBoxFocus
                        currentIndex: focusModel.currentIndex
                        Layout.fillWidth: true
                        model: focusModel
                        visible: count
                    }

                    Label {
                        id: labelSize
                        text: qsTr("Img. size")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxSize.visible
                    }

                    ComboBox {
                        id: comboBoxSize
                        currentIndex: sizeModel.currentIndex
                        Layout.fillWidth: true
                        model: sizeModel
                        visible: count
                    }

                    Label {
                        id: labelQuality
                        text: qsTr("Quality")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxQuality.visible
                    }

                    ComboBox {
                        id: comboBoxQuality
                        currentIndex: qualityModel.currentIndex
                        Layout.fillWidth: true
                        model: qualityModel
                        visible: count
                    }

                    Label {
                        id: labelWhiteBalance
                        text: qsTr("Wh. bal.")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxWhiteBalance.visible
                    }

                    ComboBox {
                        id: comboBoxWhiteBalance
                        currentIndex: whitebalanceModel.currentIndex
                        Layout.fillWidth: true
                        model: whitebalanceModel
                        visible: count
                    }
                }
            }

            GroupBox {
                id: groupDeviceSettings
                Layout.fillWidth: true
                title: qsTr("Device settings")

                GridLayout {
                    id: gridLayoutDevice
                    anchors.fill: parent

                    columns: 2

                    Label {
                        id: labelControlMode
                        text: qsTr("Ctrl mode")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        visible: comboBoxControlMode.visible
                    }

                    WrappingComboBox {
                        id: comboBoxControlMode
                        currentIndex: controlmodeModel.currentIndex
                        Layout.fillWidth: true
                        model: controlmodeModel
                        visible: count
                        extraWidth: Math.floor((gridLayoutDevice.width - comboBoxControlMode.width)/1.5)
                    }

                    Label {
                        id: labelShootCnt
                        text: qsTr("Shots")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxShootCnt
                        currentIndex: shootcntModel.currentIndex
                        Layout.fillWidth: true
                        model: shootcntModel
                    }

                    Label {
                        id: labelShootBefore
                        text: qsTr("Delay bef.")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxShootBefore
                        currentIndex: shootbeforeModel.currentIndex
                        Layout.fillWidth: true
                        model: shootbeforeModel
                    }

                    Label {
                        id: labelShootAfter
                        text: qsTr("Delay after")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxShootAfter
                        currentIndex: shootafterModel.currentIndex
                        Layout.fillWidth: true
                        model: shootafterModel
                    }
                }
            }
        }

    }
}

