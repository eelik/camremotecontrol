/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAMTASKS_H_
#define CAMTASKS_H_

#define CAMTASK_INIT							0x00
#define CAMTASK_OPENSESSION				0x01
#define CAMTASK_CLOSESESSION				0x02
#define CAMTASK_CHECKEVENT					0x03
#define CAMTASK_EVENTWAIT					0x04
#define CAMTASK_CTRLMODE_EVENTWAIT		0x05
#define CAMTASK_OPEN_LENSE					0x06
#define CAMTASK_CTRLMODE_START			0x07
#define CAMTASK_SHOOT						0x08
#define CB_WIFICAM0	-16383
#define IDC_EDIT1	102
#define IDC_EDIT2	103
#define IDC_COMBOBOX1	104
#define IDC_EDIT3	105
#define IDC_EDIT4	106
#define IDC_COMBOBOX2	107
#define IDC_EDIT5	108
#define IDC_EDIT6	109
#define IDC_COMBOBOX3	110
#define CB_WIFICAM1_VENDOR	104
#define IDC_BUTTON1	105
#define VAL_WIFICAM1_PW	103
#define VAL_WIFICAM1_NAME	102
#define CAMTASK_SIMPLE_SHOOT				0x09
#define CAMTASK_CTRLMODE_END				0x0A
#define CAMTASK_CMD_DONE					0x0B
#define CAMTASK_ZOOMOUT						0x0C
#define CAMTASK_ZOOMIN						0x0D
#define CAMTASK_APERTURE_UP				0x0E
#define CAMTASK_APERTURE_DOWN				0x0F
#define CAMTASK_APERTURE_AUTO				0x10
#define CAMTASK_SHUTTERSPEED_UP			0x11
#define CAMTASK_SHUTTERSPEED_DOWN		0x12
#define CAMTASK_DEBUG						0x13
#define CAMTASK_MAGIC						0x14
#define CAMTASK_CLOSE_LENSE				0x15
#define CAMTASK_VIDEO_OUT_ON				0x16
#define CAMTASK_VIDEO_OUT_OFF				0x17
#define CAMTASK_LCD_ON						0x18
#define CAMTASK_LCD_OFF						0x19
#define CAMTASK_SET_CAM_SETTINGS			0x1A
#define CAMTASK_FOCUS						0x1B
#define CAMTASK_PROFILE_NEXT				0x1C
#define CAMTASK_PROFILE_PREV				0x1D
#define CAMTASK_RECORD_VIDEO				0x1E
#define CAMTASK_RECORD_STOP				0x1F
#define CAMTASK_RECORD_TOGGLE				0x20
#define CAMTASK_POWER_OFF					0x21
#define CAMTASK_FOCUSOFF					0x22
#define CAMTASK_FOCUS_UP1					0x23
#define CAMTASK_FOCUS_UP2					0x24
#define CAMTASK_FOCUS_DOWN1				0x25
#define CAMTASK_FOCUS_DOWN2				0x26
#define CAMTASK_BURST_SHOOT				0x27
#define CAMTASK_BURST_STOP					0x28
#define CAMTASK_GET_VIEWFINDER_DATA		0x29
#define CAMTASK_HALF_PRESS_ON				0x2A
#define CAMTASK_HALF_PRESS_OFF			0x2B
#define CAMTASK_FULL_PRESS_ON				0x2C
#define CAMTASK_FULL_PRESS_OFF			0x2D
#define CAMTASK_LIVEVIEW_ON				0x2E
#define CAMTASK_LIVEVIEW_OFF				0x2F
#define CAMTASK_DOF_ON						0x30
#define CAMTASK_DOF_OFF						0x31
#define CAMTASK_MOVE_UP						0x32
#define CAMTASK_MOVE_DOWN					0x33
#define CAMTASK_MOVE_LEFT					0x34
#define CAMTASK_MOVE_RIGHT					0x35
#define CAMTASK_WB_NEXT						0x36
#define CAMTASK_WB_PREV						0x37
#define CAMTASK_AFPOINT_NEXT				0x38
#define CAMTASK_AFPOINT_PREV				0x39
#define CAMTASK_ISO_UP						0x3A
#define CAMTASK_ISO_DOWN					0x3B
#define CAMTASK_TVCOMP_UP					0x3C
#define CAMTASK_TVCOMP_DOWN				0x3D
#define CAMTASK_ZOOM_OFF					0x3E
#define CAMTASK_MOVIEMODE_ON				0x3F
#define CAMTASK_MOVIEMODE_OFF				0x40
#define CAMTASK_ADVANCED_SHOOT			0x41
#define CAMTASK_MAGNIFICATION				0x42
#define CAMTASK_POWER_ON					0x43
#define CAMTASK_SHUTTERSPEED				0x44
#define CAMTASK_APERTURE					0x45
#define CAMTASK_ISO							0x46
#define CAMTASK_CAMSTAT						0x47
#define CAMTASK_DIALPOS						0x48
#define CAMTASK_HALF_PRESS_ON_NOAF		0x49
#define CAMTASK_DRIVEMODE					0x4A
#define CAMTASK_FULL_PRESS_ON_NOAF		0x4B
#define CAMTASK_BATTERY_LEVEL				0x4C
#define CAMTASK_ZOOM							0x4D
#define CAMTASK_LIVEVIEW					0x4E
#define CAMTASK_AVAILABLE_SHOTS			0x4F
#define CAMTASK_LANC_PARAM					0x50
#define CAMTASK_LANC_PARAM1				0x51
#define CAMTASK_SET_REMOTEMODE			0x52
#define CAMTASK_SET_EVENTMODE				0x53
#define CAMTASK_FOCUS_LOCK					0x54
#define CAMTASK_FOCUS_UNLOCK				0x55
#define CAMTASK_PROFILE_ACTIVATE			0x56
#define CAMTASK_GET_HDD_CAPACITY			0x57
#define CAMTASK_APPINFO						0x58
#define CAMTASK_WB							0x59
#define CAMTASK_POWER_TOGGLE				0x5A
#define CAMTASK_POWER_OFF_ON				0x5B
#define CAMTASK_LIVEVIEW_TOGGLE			0x5C
#define CAMTASK_LCD_TOGGLE					0x5D
#define CAMTASK_IDLE							0x5E

#define CAMTASK_BN_UP						0x5F
#define CAMTASK_BN_DOWN						0x60
#define CAMTASK_BN_LEFT						0x61
#define CAMTASK_BN_RIGHT					0x62
#define CAMTASK_BN_SET						0x63
#define CAMTASK_BN_MENU						0x64
#define CAMTASK_BN_MODE						0x65

#define CAMTASK_AF_ON						0x66
#define CAMTASK_MF_ON						0x67
#define CAMTASK_AF_MF_TOGGLE				0x68

#define CAMTASK_GET_FEATURES				0x69
#define CAMTASK_APERTURE_SET				0x6A
#define CAMTASK_SHUTTERSPEED_SET			0x6B


#define CAMTASK_ZOOMIN1						0x61
#define CAMTASK_ZOOMIN2						0x62
#define CAMTASK_ZOOMIN3						0x63
#define CAMTASK_ZOOMIN4						0x64
#define CAMTASK_ZOOMIN5						0x65
#define CAMTASK_ZOOMIN6						0x66
#define CAMTASK_ZOOMIN7						0x67
#define CAMTASK_ZOOMIN8						0x68

#define CAMTASK_ZOOMOUT1					0x69
#define CAMTASK_ZOOMOUT2					0x6A
#define CAMTASK_ZOOMOUT3					0x6B
#define CAMTASK_ZOOMOUT4					0x6C
#define CAMTASK_ZOOMOUT5					0x6D
#define CAMTASK_ZOOMOUT6					0x6E
#define CAMTASK_ZOOMOUT7					0x6F
#define CAMTASK_ZOOMOUT8					0x70


// Temporary for Lance testings:
#define CAMTASK_RECORD_TOGGLE_1			0x71
#define CAMTASK_RECORD_TOGGLE_2			0x72
#define CAMTASK_RECORD_TOGGLE_3			0x73
#define CAMTASK_RECORD_TOGGLE_4			0x74
#define CAMTASK_RECORD_TOGGLE_5			0x75
#define CAMTASK_RECORD_TOGGLE_6			0x76
#define CAMTASK_RECORD_TOGGLE_7			0x77
#define CAMTASK_RECORD_TOGGLE_8			0x78

// Temporary for PANA tests
#define CAMTASK_TEST1_0						0xA0
#define CAMTASK_TEST1_1						0xA1
#define CAMTASK_TEST1_2						0xA2
#define CAMTASK_TEST1_3						0xA3
#define CAMTASK_TEST1_4						0xA4
#define CAMTASK_TEST1_5						0xA5
#define CAMTASK_TEST1_6						0xA6
#define CAMTASK_TEST1_7						0xA7
#define CAMTASK_TEST1_8						0xA8
#define CAMTASK_TEST1_9						0xA9

#define CAMTASK_TEST2_0						0xB0
#define CAMTASK_TEST2_1						0xB1
#define CAMTASK_TEST2_2						0xB2
#define CAMTASK_TEST2_3						0xB3
#define CAMTASK_TEST2_4						0xB4
#define CAMTASK_TEST2_5						0xB5
#define CAMTASK_TEST2_6						0xB6
#define CAMTASK_TEST2_7						0xB7
#define CAMTASK_TEST2_8						0xB8
#define CAMTASK_TEST2_9						0xB9

#define CAMTASK_TEST3_0						0xC0
#define CAMTASK_TEST3_1						0xC1
#define CAMTASK_TEST3_2						0xC2
#define CAMTASK_TEST3_3						0xC3
#define CAMTASK_TEST3_4						0xC4
#define CAMTASK_TEST3_5						0xC5
#define CAMTASK_TEST3_6						0xC6
#define CAMTASK_TEST3_7						0xC7
#define CAMTASK_TEST3_8						0xC8
#define CAMTASK_TEST3_9						0xC9


#define CAMTASK_STAT_JPEGOK				0xE0
#define CAMTASK_STAT_TOUCH					0xE1
#define CAMTASK_RUN							0xE3
#define CAMTASK_DBG_ON						0xEA
#define CAMTASK_DBG_OFF						0xEB
#define CAMTASK_PARAM1						0xEC
#define CAMTASK_PARAM2						0xED

// 00..40 == CAMTASKs (camtask.h)
#define SERVOTASK_INIT					0x80
#define SERVOTASK_TILT_UP				0x81
#define SERVOTASK_TILT_DOWN			0x82
#define SERVOTASK_PAN_LEFT				0x83
#define SERVOTASK_PAN_RIGHT			0x84
#define SERVOTASK_ROLL_LEFT			0x85
#define SERVOTASK_ROLL_RIGHT			0x86
#define SERVOTASK_ZOOM_IN				0x87
#define SERVOTASK_ZOOM_OUT				0x88
#define SERVOTASK_SERVO1				0x89
#define SERVOTASK_SERVO2				0x8A
#define SERVOTASK_TOUCH					0x8B
#define SERVOTASK_TILT_DUTY			0x8C

// 00..40 == CAMTASKs (camtask.h)
// 80..90 == SERVOTASKs (camtask.h)
#define CMD_START					0xB0
#define CMD_STOP					0xB1
#define CMD_PAUSE					0xB2
#define CMD_TOGGLE				0xB3
#define CMD_ALT					0xB4
#define CMD_PROFILE				0xB5
#define CMD_TASK					0xB6

#ifdef LEICA_RP_TESTS
#define CMD_TEST_RP2_DP0		0xE0
#define CMD_TEST_RP2_DPZ		0xE1
#define CMD_TEST_RP2_DM0		0xE2
#define CMD_TEST_RP2_DMZ		0xE3
#define CMD_TEST_RP2_ID0		0xE4
#define CMD_TEST_RP2_IDZ		0xE5
#define CMD_TEST_RP2_DP0DM0	0xE6
#define CMD_TEST_RP2_DP0ID0	0xE7
#define CMD_TEST_RP2_DM0ID0	0xE8
#endif

#define CAMMODE_EOS_DIALPOS_TV			1
#define CAMMODE_EOS_DIALPOS_AV			2
#define CAMMODE_EOS_DIALPOS_M				3




#endif /*CAMTASKS_H_*/
