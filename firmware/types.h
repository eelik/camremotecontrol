/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/******************************************************************************
 *
 * $RCSfile: $
 * $Revision: $
 *
 * This module defines some regularly used typedefs
 *
 *****************************************************************************/
#ifndef INC_TYPES_H
#define INC_TYPES_H


#ifndef NULL
#define NULL    ((void *)0)
#endif


#ifndef TRUE
//#define TRUE    (1)
#endif

/*  typedefs are here  */
#ifndef __CRCC_VERSION__
typedef unsigned char       uint8_t;
typedef   signed char        int8_t;
typedef unsigned short     uint16_t;
typedef   signed short      int16_t;

#endif


typedef unsigned char       UINT8;
typedef   signed char        INT8;
typedef unsigned short     UINT16;
typedef   signed short      INT16;
typedef unsigned long      UINT32;
typedef   signed long       INT32;
#ifndef __BORLANDC__
typedef unsigned long long UINT64;
typedef   signed long long  INT64;
#endif

typedef unsigned char   uint8;
typedef unsigned short  uint16;
typedef unsigned long   uint32;



#endif // INC_TYPES_H
//#endif

