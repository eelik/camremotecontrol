/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _CAMCTRL_H_
#define _CAMCTRL_H_

#include "types.h"


#define LEN_CAMNAME	32

#define ATTR_BN_UPDATE_LIVE			(1 << 0)
#define ATTR_BN_UPDATE_AT_TOUCH		(1 << 1)
#define ATTR_BN_UPDATE_AT_DETOUCH	(1 << 2)
#define ATTR_BN_TYPE_CAMITEM			(1 << 3)
#define ATTR_BN_TYPE_OTHER				(1 << 4)
#define ATTR_BN_TYPE_PTPVALUE			(1 << 5)

#define LEN_PTPIN		0x10

typedef struct _PTPPACKET
{
    UINT32	Len;
    UINT16	Type;
    UINT16	Code;
    UINT32	Id;
    UINT16	Param1;
    UINT16	Param2;
} PTPPACKET;

typedef struct _CAMITEM
{
	UINT16 Code;
	char *Str;
} CAMITEM;

typedef struct _BTNITEM
{
	UINT8 Type;
	UINT8 Cmd;
	UINT8 CmdUp;
	UINT8 CmdDn;
	UINT16 Attr;
	CAMITEM *Item;
	char *Text;
} BTNITEM;

typedef struct _BTNITEMP
{
    const BTNITEM *Btn;
} BTNITEMP;

typedef struct _CAMITEMINT16
{
    UINT8 Code;
    UINT16 Time;
} CAMITEMINT16;

typedef struct _CAMPROFILEV00 // For ucontroller
{
	UINT16 Id;
	UINT16 OffsetNext;
	UINT16 CamModel;
	UINT16 CtrlType;
	UINT16 ControlMode;
	UINT16 Options1; // Not used at the moment
	UINT16 IsoSpeed;
	UINT16 AEProgram;
	UINT16 Tv;
	UINT16 Av;
	UINT16 Focus;
	UINT16 Zoom;
	UINT16 Size;
	UINT16 Quality;
	UINT16 Repeat; // Intervalometer
	UINT16 Interval; // Intervalometer
	UINT16 Options2;
	UINT16 WhiteBalance;

	UINT8 ServoPanPos; // Used for LANC0_30 UINT16
	UINT8 ServoPanDuration;

	UINT8 ServoTilt1; // Used for LANC0_70 UINT16
	UINT8 ServoTilt2;
	UINT8 ServoTilt3; // Used for LANC1_30 UINT16
	UINT8 ServoTilt4;

	UINT16 ServoFingerIdle;  // Used for LANC2_30 UINT16
	UINT8 ServoFingerFull_Reserved; // Free!

	UINT8 ServoShootCnt;
	UINT8 ServoShootBefore;
	UINT8 ServoShootAfter;

	UINT8 Uvlo;
	UINT8 StartupDelay;
	UINT8 ServoTilt5;   // Used for LANC1_70 UINT16
	UINT8 ServoTiltDuration;

	UINT8 ServoPanStartPos; // Used for LANC3_30 UINT16
	UINT8 ServoPanSteps;

	UINT8 Pause; // Used for LANC3_70 UINT16
	UINT8 x;

	UINT16 ServoFingerFull;  // Used for LANC2_70 UINT16

	UINT16 Duration; // Intervalometer

	UINT8 Sbus12;
	UINT8 Sbus34;
	UINT8 Sbus56;
	UINT8 Sbus78; // Not used, for truncate only

	UINT16 Reserved[8];

//	void *Next;
} CAMPROFILEV00;

typedef struct _CAMPROFILEV01 // For ucontroller
{
    UINT16 Id;
    UINT16 OffsetNext;
    UINT16 CamModel;
    UINT16 CtrlType;
    UINT16 ControlMode;
    UINT16 Options1; // Not used at the moment
    UINT16 IsoSpeed;
    UINT16 AEProgram;
    UINT16 Tv;
    UINT16 Av;
    UINT16 Focus;
    UINT16 Zoom;
///	UINT16 Size;
///	UINT16 Quality;
    UINT16 TimeSet1Start; // New
    UINT16 TimeSet2Start; // New
    UINT16 Repeat; // Intervalometer
    UINT16 Interval; // Intervalometer
///	UINT16 Options2;
///	UINT16 WhiteBalance;
    UINT16 TimeSet3Start; // New
    UINT16 TimeSet1End; // New

///	UINT8 ServoPanPos; // Used for LANC0_30 UINT16
///	UINT8 ServoPanDuration;
    UINT16 TimeSet2End; // New

///	UINT8 ServoTilt1; // Used for LANC0_70 UINT16
///	UINT8 ServoTilt2;
    UINT16 TimeSet3End; // New
    UINT8 ServoTilt3; // Used for LANC1_30 UINT16
    UINT8 ServoTilt4;

    UINT16 ServoFingerIdle;  // Used for LANC2_30 UINT16
    UINT8 ServoFingerFull_Reserved; // Free!

    UINT8 ServoShootCnt;
    UINT8 ServoShootBefore;
    UINT8 ServoShootAfter;

    UINT8 Uvlo;
    UINT8 StartupDelay;
    UINT8 ServoTilt5;   // Used for LANC1_70 UINT16
    UINT8 ServoTiltDuration;
    UINT8 ServoPanStartPos; // Used for LANC3_30 UINT16
    UINT8 ServoPanSteps;

    UINT8 Pause; // Used for LANC3_70 UINT16
    UINT8 x;

    UINT16 ServoFingerFull;  // Used for LANC2_70 UINT16

    UINT16 Duration; // Intervalometer

///	UINT8 Sbus12;
///	UINT8 Sbus34;
    UINT16 TimeRadioWakeup;
    UINT8 Sbus56;
    UINT8 Sbus78; // Not used, for truncate only

    UINT16 Reserved[8];

//	void *Next;
} CAMPROFILEV01;

typedef struct _CAMDESCRIPTOR
{
	UINT16 CamID;
	char ModelName[LEN_CAMNAME];
	UINT16 Vid;
	UINT16 Pid;
	UINT16 Features;
	const CAMITEM *IsoSpeed;
	const CAMITEM *AEProgram;
	const CAMITEM *Tv;
	const CAMITEM *Av;
	const CAMITEM *Focus;
	const CAMITEM *Zoom;
	const CAMITEM *CtrlType;
	const CAMITEM *Options1;
	const CAMITEM *Options2;
	const CAMITEM *ControlMode;
	const CAMITEM *WhiteBalance;
	const CAMITEM *Size;
	const CAMITEM *Quality;
	const BTNITEM *Btn;

//	UINT16 Repeat;
//	UINT16 Interval;
} CAMDESCRIPTOR;

typedef struct _CAMPROFILES
{
	CAMPROFILEV00 Profile;
	CAMDESCRIPTOR *Desc;
} CAMPROFILES;

#define LEN_AP_NAME		32
#define LEN_AP_PASSWORD	16
typedef struct _WIFIAP
{
    UINT16 Sum;
    UINT8 Vendor;
    UINT8 Reserved;
    unsigned char Name[LEN_AP_NAME];
    unsigned char Password[LEN_AP_PASSWORD];
} WIFIAP;

#endif /*CAMCTRL_H_*/
