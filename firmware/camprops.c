#include "application.h"

#ifdef __BORLANDC__

const CAMITEM CtrlType[] =
{
	{3, "Servo:CH2"},
	{0 , NULL},
};

const CAMITEM CtrlType0[] =
{
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{0 , NULL},
};

const CAMITEM CtrlType1[] =
{
	{1, "USB/PTP"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{0 , NULL},
};

const CAMITEM CtrlType2[] =
{
	{1, "USB/PTP"},
	{2, "IR"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{0 , NULL},
};

const CAMITEM CtrlType3[] =
{
	{1, "USB/PTP"},
//	{2, "IR - todo"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{4, "USB/CHDK"},
	{10, "USB/CHDK+"},
	{0 , NULL},
};

const CAMITEM CtrlType4[] =
{
//	{3, "Servo"},
	{4, "USB/CHDK"},
	{CTRLTYPE_CHDK_PTP, "CHDK/PTP"},
	{10, "USB/CHDK+"},
	{0 , NULL},
};

const CAMITEM CtrlType5[] =
{
	{2, "IR"},
	{CTRLTYPE_LANC1, "LANC"},
	{CTRLTYPE_LANC_IR, "LANC+IR"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{CTRLTYPE_LANC0, "LANC:USB/IR"},
	{0 , NULL},
};

const CAMITEM CtrlType6[] =
{
	{1, "USB/PTP"},
	{2, "IR"},
	{8, "PORT/E3A"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
//	{4, "USB/CHDK"},
	{0 , NULL},
};

const CAMITEM CtrlType7[] =
{
	{5, "USB/CA1"},
//	{CTRLTYPE_CA1V, "USB/CA1+V"},
	{CTRLTYPE_CA1V, "SW:CH3"},
//	{CTRLTYPE_SWITCH_CH3, "SW:CH3"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{0 , NULL},
};

const CAMITEM CtrlType8[] =
{
	{1, "USB/PTP"},
	{8, "PORT/E3A"},
	{6, "PORT/E3"},
	{2, "IR"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{CTRLTYPE_USBMUX1, "USB/MUX1"},
	{0 , NULL},
};

const CAMITEM CtrlType9[] =
{
	{7, "PORT/CR"},
	{0 , NULL},
};

const CAMITEM CtrlType10[] =
{
	{CTRLTYPE_E3A, "RP/E3A"},
	{CTRLTYPE_E3B, "RP/E3B"},
	{CTRLTYPE_RP_PANA_ZOOM1, "RP/PANA-Z1"},
//	{CTRLTYPE_RP_PANA_ZOOM2, "RP/PANA-Z2"},
	{CTRLTYPE_E3, "RP/E3"},
	{0 , NULL},
};

const CAMITEM CtrlType11[] =
{
	{1, "USB/PTP"},
	{2, "IR"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{4, "USB/CHDK"},
	{10, "USB/CHDK+"},
	{5, "USB/CA1"},
	{8, "PORT/E3A"},
	{6, "PORT/E3"},
	{7, "PORT/CR"},
	{CTRLTYPE_GOPROA, "BACK-CON1"},
	{CTRLTYPE_MULTI, "MULTI"},
	{CTRLTYPE_RTC1B, "RTC1-B"},
	{0 , NULL},
};

const CAMITEM CtrlType12[] =
{
	{1, "USB/PTP"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{4, "USB/CHDK"},
	{0 , NULL},
};

const CAMITEM CtrlType13[] =
{
	{1, "USB/PTP"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{8, "PORT/E3A"},
	{6, "PORT/E3"},
	{0 , NULL},
};

const CAMITEM CtrlType14[] =
{
	{CTRLTYPE_GOPROA, "BACK-CON1"},
	{0 , NULL},
};

const CAMITEM CtrlType15[] =
{
	{CTRLTYPE_LANC1, "LANC"},
	{CTRLTYPE_LANC0, "LANC:USB/IR"},
	{0 , NULL},
};

const CAMITEM CtrlType16[] =
{
	{2, "IR"},
	{8, "PORT/E3A"},
	{3, "Servo:CH2"},
	{9, "Servo:CH3"},
	{0 , NULL},
};

const CAMITEM CtrlType17[] =
{
	{CTRLTYPE_LANC1, "LANC"},
	{CTRLTYPE_LANC_AV, "LANC+AV"},
	{CTRLTYPE_MULTI, "MULTI"},
	{CTRLTYPE_MULTI_AV, "MULTI+AV"},
	{CTRLTYPE_MULTI_PTP, "MULTI+PTP"},
	{2, "IR"},
	{1, "USB/PTP"},
	{CTRLTYPE_MULTI_MUX, "MULTI-MUX"},
	{CTRLTYPE_MULTI_CC, "MULTI-CC"},
//	{CTRLTYPE_MULTI_X, "CUSTOM:MX"},
//	{CTRLTYPE_MULTI_XT, "CUSTOM:MXT"},
	{CTRLTYPE_MULTI_ONOFF, "CUSTOM:M1"},
//	{CTRLTYPE_LANC_AVM, "LANC+AVM"}, // Multi mode LANC
	{CTRLTYPE_LANC0, "LANC:USB/IR"},
	{CTRLTYPE_WIFIM1, "WIFI-M1"},
	{0 , NULL},
};

const CAMITEM CtrlType18[] =
{
	{CTRLTYPE_RED_TRIG, "RED/TRIG"},
	{CTRLTYPE_RED_UART, "RED/UART"},
	{0 , NULL},
};

const CAMITEM CtrlType19[] =
{
	{1, "USB/PTP+RP"},
	{CTRLTYPE_RP_PANA_ZOOM1, "RP/PANA-Z1"},
	{CTRLTYPE_WIFIM1, "WIFI-M1"},
	{0 , NULL},
};

const CAMITEM CtrlType20[] =
{
	{CTRLTYPE_RTC1B, "RTC1-B"},
	{0 , NULL},
};

const CAMITEM ControlModeX[] =
{
	{1, "Externally Controllable via Channels"},
	{2, "Intervalometer"},
	{4, "Auto HoVer: 'V'-pattern motion, CH0=Pan, CH1=Tilt"},
	{5, "Auto Panorama: CH0=Pan"},
	{0 , NULL},
};

const CAMITEM Options1[] =
{
	{1, "By external RC receiver device (pulse width sensitivity)"},
	{2, "By external switches (rising edge sensitivity)"},
	{3, "By DuneCam/Kapsure (shutter/zoom control via Channel0)"},
	{0 , NULL},
};

//#define BSKINETICS
#ifdef BSKINETICS
const CAMITEM ControlMode[] =
{
	{CTRLMODE_SWITCHES, "Manual: Wired (CH0=Resistors+Switches)"},
	{CTRLMODE_CUSTOM25_BSKINETICS, "Custom25: BS-6 Switches"},
	{CTRLMODE_CUSTOM26_BSKINETICS, "Custom26: BS-13 Switches"},
	{0, NULL},
};
#else
const CAMITEM ControlMode[] =
{
	{CTRLMODE_EXTCTRL_3CH_PWM, "Manual: RC receiver (CH0=Shoot, CH1=Zoom, CH2=Alt)"},
	{CTRLMODE_EXTCTRL_RC3, "Manual: RC receiver (CH0=Execute, CH1=Function)"},
	{CTRLMODE_EXTCTRL_RC1, "Manual: RC receiver (CH0=Record/AF/Shoot, CH3=PulseOut)"},
	{CTRLMODE_EXTCTRL_3CH_PWM1, "RC receiver (CH0=Record/AF/Shoot, CH1=SmoothZoom, CH2=Alt)"},
	{CTRLMODE_EXTCTRL_3CH_PWM3, "RC receiver (CH0=Record/AF/Shoot, CH1=MF, CH2=Av/AutoAv)"},
	{CTRLMODE_EXTCTRL_2CH_PWM3, "RC receiver (CH0=Record/AF/Shoot, CH1=Record, CH3=SyncOut)"},
	{CTRLMODE_EXTCTRL_3CH_PWM2, "RC receiver (CH0=Record/AF/Shoot, CH1=Av, CH2=Tv, CH3=ISO)"},
	{CTRLMODE_EXTCTRL_3CH_PWM4, "RC receiver (CH0=Rec/Shoot, CH1=Zoom/Iris, CH2=AF, CH3=On/Off)"},
	{CTRLMODE_EXTCTRL_4CH_PWM3, "RC receiver (CH0=Record/AF/Shoot, CH1=Zoom, CH2=Av, CH3=ISO)"},
	{CTRLMODE_EXTCTRL_4CH_PWM4, "RC receiver (CH0=Record/AF/Shoot, CH1=Zoom, CH2=Tv, CH3=ISO)"},
	{CTRLMODE_EXTCTRL_3CH_PWM5, "RC receiver (CH0=Rec/AF, CH1=MF, CH2=Manual/Auto Iris)"},
	{CTRLMODE_EXTCTRL_4CH_PWM1, "RC receiver (CH0+3=Rec/Shoot, CH1+CH2=Zoom"},
//	{CTRLMODE_EXTCTRL_RC1S, "Manual: RC receiver (CH0=Record/AF/Shoot, CH2=ShootOut, CH3=RecOut)"},
	{CTRLMODE_ARDUCOPTER, "Arducopter: (CH0/CH2=Record/Shoot, CH1=Zoom/Record/CamOn+Off)"},
//	{CTRLMODE_ARDUCOPTER, "Arducopter+Radio: (CH0=Radio, CH1=Zoom, CH2=Record/Shoot)"},
	{CTRLMODE_MIKROKOPTER, "Mikrokopter: (CH0=Record/Shoot, CH1=Zoom)"},
	{CTRLMODE_PIXHAWK, "Pixhawk: (CH0=Shoot, CH1=Zoom, CH2=Record)"},
	{CTRLMODE_PARACHUTE1, "Parachute Kit: (CH0=433MHz Radio, CH1=RX, CH2=Servo)"},
	{CTRLMODE_EXTCTRL_RISE, "Manual: Rising edge sense (CH0=Shoot, CH1=Zoom+, CH2=Zoom-)"},
//	{5, "External: Falling edge sense (CH0=Shoot, CH1=Zoom+, CH2=Zoom-)"},
	{CTRLMODE_EXTCTRL_DCAM, "Manual: By DuneCam/KAPsure (CH0=Shoot/Zoom/Set, CH1=MsgOut)"},
	{CTRLMODE_EXTCTRL_RC4, "Manual: RC4 at CH0 (A=ZoomOut, B=ZoomIn, C=Shoot, D=Pause)"},
//	{CTRLMODE_EXTCTRL_RC4B, "Manual: 4-Button Radio (CH0=Radio, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_CAMCTRL1_RC433, "Manual: 8/12B-Radio+FC+RC (CH0=RX, CH1=Zoom, CH2=Rec/Shoot)"},
	{CTRLMODE_EXTCTRL_RC8, "Manual: 4/8/12-Button Radio (CH0=Radio, CH1=Tilt, CH2=Pan, CH3=Z/R)"},
	{CTRLMODE_EXTCTRL_RC8_RC, "Manual: 4/8/12b+RC Radio (CH0=Radio, CH1=RX, CH2=Pan, CH3=Tilt)"},
	{CTRLMODE_EXTCTRL_RC8_TILT, "Manual: 4/8/12-Button Radio (CH0=Radio, CH1=Tilt)"},
	{CTRLMODE_CAMCTRL_REC_RC433, "Manual: 4/12-Button RC for REC (CH0=Radio, CH3=LED Out)"},
//	{CTRLMODE_EXTCTRL_RC8_RC, "4/8/12-Button Radio (MF, ISO, Zoom, Tv, Av)"},
//	{CTRLMODE_EXTCTRL_RC8_1, "Manual: 4/8/12-Button Radio (CH0=RX, CH1=Tilt, CH2=Pan, CH3=Z/R)"},
	{CTRLMODE_SBUS1, "S.BUS: (F1=Rec/AF/Shoot, F2=Zoom, F3=Av, F4=Tv, F5=ISO, F6=MF)"},
//	{CTRLMODE_CPPM1, "CPPM: (F1=Rec/AF/Shoot, F2=Zoom, F3=Av, F4=Tv, F5=ISO, F6=?)"},
//		{CTRLMODE_SBUS_TESTER, "S.BUS Tester (CH0=SBUS)"},
//	{CTRLMODE_SBUS2, "S.BUS on CH0: (#1=Record/AF/Shoot, #2=Av, #3=Tv, #4=ISO)"},
	{CTRLMODE_EXTCTRL_SW, "Manual: Switch (CH0=Shoot, CH1=Focus+, CH2=Focus-, CH3=Record)"},
	{CTRLMODE_EXTCTRL_SW2, "Manual: Switch (CH0=Shoot, CH1=Av+, CH2=Av-, CH3=Record)"},
	{CTRLMODE_TIMER, "Auto: Intervalometer (CH3=Sync Out)"},
	{CTRLMODE_TIMER_RC, "Auto: Intervalometer+RC (CH0=4/8b-Radio, CH1=Tilt, CH2=Pan, CH3=Sync Out)"},
	{CTRLMODE_HOVER, "Auto: HoVer 'V'-pattern motion (CH0=Pan, CH1=Tilt)"},
	{CTRLMODE_PANORAMA, "Auto: Panorama (CH0=Pan)"},
	{CTRLMODE_HOVER_RC8, "Auto/RC: HoVer 'V'-pattern motion (CH0=4/8b-Radio, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_HOVER_RC2, "Auto/RC: HoVer 'V'-pattern motion (CH0=RC-Radio, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_HOVER_S, "Auto/RC: HoVer 'S'-pattern motion (CH0=RC-Radio, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_HOVER_MANUAL_RC8, "Auto/RC: HoVer+Manual (CH0=Radio, CH1=Tilt, CH2=Pan, CH3=Zoom)"},
	{CTRLMODE_TEST_RC_PWM, "PWM Signals Tester: CH0-CH2"},
	{CTRLMODE_UART1, "UART Control 115200 baud (CH0=RXD, CH1=TXD)"},
	{CTRLMODE_UART2, "UART Control 9600 baud (CH0=RXD, CH1=TXD)"},
	{CTRLMODE_SWITCHES, "Manual: Wired (CH0=Resistors+Switches)"},
	{CTRLMODE_MIKROKOPTER, "Mikrokopter FC SV2: CH0=Shoot"},
	{CTRLMODE_CUSTOM20_FIXSERVO, "Fixed Servos (CH0=Shoot Switch, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_PIR_LS, "Wired PIR with Light Sensor (CH0=PIR, CH1=Light Sensor, CH2/3=Lamp)"},
	{CTRLMODE_PIRW_LS, "Wireless PIR with Light Sensor (CH0=Radio, CH1=Light Sensor, CH2/3=Lamp)"},
	{CTRLMODE_TRAILMASTER, "TrailMaster (CH0=Radio+PIR, CH2=Shoot/Rec)"},
	{CTRLMODE_SEQUOIA, "Parrot Sequoia (CH0=Shoot, CH1=PWM, CH3=Pulse Out)"},
	{CTRLMODE_CUSTOM1, "Custom1: CH0=Shoot, CH1=Focus, CH2=Zoom"},
	{CTRLMODE_CUSTOM5, "Custom5: CH0=Radio, CH1=Tilt, CH2=Pan, CH3=Roll, OSD=Zoom"},
	{CTRLMODE_CUSTOM6_COPTER, "Custom6: HoVer 'S'-pattern motion (CH0=RC-Radio, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_CUSTOM7_EGERT, "Custom7: Manual 4/8-Button Radio (CH0=Radio)"},
	{CTRLMODE_CUSTOM9_PWM_INPUT, "Custom9: CH0=PWM Input"},
	{CTRLMODE_CUSTOM11_BRACKET, "Custom11: CH2=Shoot, CH0=RXD, CH1=TXD"},
	{CTRLMODE_CUSTOM12_SEQUENTIAL, "Custom12: CH2=Sequential Shooting"},
	{CTRLMODE_CUSTOM14_JAKUB, "Custom13: RC receiver (CH0=Shoot, CH1=Record)"},
	{CTRLMODE_CUSTOM15_TADDEI, "Custom15: Manual 4/8/12-Button Radio (CH0=Radio)"},
	{CTRLMODE_CUSTOM16_LANC, "Custom16: RC Receiver + LANC (CH0-3=Radio)"},
	{CTRLMODE_CUSTOM17_PWM_INPUT, "Custom17: RC receiver (CH0=AF/Shoot, CH1=AF/Record, CH2=Av)"},
	{CTRLMODE_EXTCTRL_RC8_CUSTOM18, "Custom18: 4/8/12-Button Radio (CH0=Radio, CH1=2-Tilt, CH2=Pan)"},
	{CTRLMODE_POT_CUSTOM19, "Custom19: Pot for Aperture (CH0=Av Pot, CH3=Full press"},
	{CTRLMODE_EXTCTRL_RC3_CUSTOM21, "Custom21: RC (CH0=Execute, CH1=Function, CH2=Rec, CH3=Shoot)"},
	{CTRLMODE_CUSTOM22_RC4_CH1_CH2, "Custom22: 4/8/12-Button Radio (CH0=Radio, CH2=PD, CH3=Pulse)"},
	{CTRLMODE_CUSTOM23_PIR, "Custom23: 4-PIR System (CH0=PIR1, CH1=PIR2, CH2=PIR3, CH3=PIR4/Sync)"},
	{CTRLMODE_CUSTOM24_8CAM, "Custom24: 8 Camera Control (CH0=Radio)"},
	{CTRLMODE_CUSTOM25_BSKINETICS, "Custom25: BS-6 Switches"},
	{CTRLMODE_CUSTOM26_BSKINETICS, "Custom26: BS-13 Switches"},
	{CTRLMODE_EXTCTRL_4CH_PWM5, "Custom27: RC-Receiver (CH0/3=Shoot, CH1/2=Zoom)"},
	{EXTCTRL_RC12_CUSTOM28, "Custom28: 12-Button Radio (CH0=Radio)"},
	{EXTCTRL_RC4_CUSTOM29, "Custom29: 4-Button Radio (A=Rec, B=Av+, C=Av-, D=?)"},
	{EXTCTRL_RC12_CUSTOM30, "Custom30: 12-Button Radio (CH0=Radio)"},
	{CTRLMODE_CUSTOM10_INTEGRATION, "Development: CH0=Av lock, CH2=Av unlock, CH2=Record"},
	{CTRLMODE_CUSTOM8_PANACR, "Development: RC Receiver (CH0=Zoom, CH1=Focus, CH2=Iris, CH3=Rec)"},
	{CTRLMODE_HOVER_Z, "Development: HoVer 'Z'-pattern motion (CH0=Pan, CH1=Tilt)"},
	{CTRLMODE_EXTCTRL_LS, "Development: Light Sensor (CH0=LS, CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_SERVOSPLIT, "Development: Servo Split (CH0=I1, CH1=I2, CH2=O1, IR=O2)"},
	{CTRLMODE_CUSTOM2, "Development: CH0=GPS"},
	{CTRLMODE_CUSTOM3, "Development: CH0=Pot:Focus, CH1=Pot:Tv, CH2=Magnification, CH3=Rec"},
	{CTRLMODE_SYNCMASTER, "Development (Master): CH0=Radio, CH1=SyncOut, CH2=Shot, CH3=Video"},
	{CTRLMODE_SYNCSLAVE, "Development (Slave): CH2=Shot, CH3=Video"},
	{CTRLMODE_INTWIFI_TCP, "Development: Internal Wifi, TCP Mode"},
	{CTRLMODE_INTWIFI_UDP, "Development: Internal Wifi, UDP Mode"},
	{CTRLMODE_WII1, "Development: Wii+DSLR"},
	{CTRLMODE_INTWIFI_DV, "Development: Wifi+DV (CH1=Tilt, CH2=Pan)"},
	{CTRLMODE_CUSTOM23_PELLET, "Development: EF (CH0=RX, CH1=Tint, CH2=Text)"},
	{CTRLMODE_TMP1, "SwitchTest: (CH0=Tv+, CH1=Tv-, CH2=Av+, CH3=Av-"},
//	{CTRLMODE_TMP2, "SwitchTest: (CH0=ISO+, CH1=ISO-, CH2=HalfPress, CH3=FullPress"},
	{CTRLMODE_TMP2, "SwitchTest: (CH0=StartRec, CH1=StopRec, CH2=On, CH3=Off"},
	{CTRLMODE_TMP3, "SwitchTest: (CH0=Half, CH1=Full, CH2=Com)"},
	{CTRLMODE_RESERVED, "Reserved"},
	// 9V rechargeable 90mA: 15:48-16:00
	// 9V rechargeable 200mA: 16:03-
	{0, NULL},
};
#endif

const CAMITEM Options2[] = {0, NULL};

#define CAM_VENDOR_CANON_PS	(0x40 << 8)
#define CAM_VENDOR_CANON_SLR	(0x80 << 8)
#define CAM_VENDOR_CANON_ELPH	(0x01 << 8)
#define CAM_VENDOR_CANON_CC	(0x02 << 8)
#define CAM_VENDOR_CANON_IR	(0x03 << 8)
#define CAM_VENDOR_NIKON		(0x08 << 8)
#define CAM_VENDOR_NIKON_USB	(0x09 << 8)
#define CAM_VENDOR_OLYMPUS		(0x0C << 8)
#define CAM_VENDOR_OLYMPUS_USB (0x0D << 8)
#define CAM_VENDOR_SONY			(0x10 << 8)
#define CAM_VENDOR_SONY_HC		(0x11 << 8)
#define CAM_VENDOR_SONY_DSLR	(0x12 << 8)
#define CAM_VENDOR_SONY_AC		(0x13 << 8)
#define CAM_VENDOR_PENTAX		(0x18 << 8)
#define CAM_VENDOR_PANASONIC	(0x1B << 8)
#define CAM_VENDOR_SAMSUNG		(0x1D << 8)
#define CAM_VENDOR_RICOH		(0x1E << 8)
#define CAM_VENDOR_SANYO		(0x1F << 8)
#define CAM_VENDOR_JVC			(0x20 << 8)
#define CAM_VENDOR_GOPRO		(0x22 << 8)
#define CAM_VENDOR_BLACKMAGIC	(0x24 << 8)
#define CAM_VENDOR_RED			(0x26 << 8)

const CAMITEM WifiCameras[] =
{
		{0, "None"},
//		{1, "Auto"},
		{CAM_VENDOR_SONY >> 8, "Sony Camera"},
		{CAM_VENDOR_SONY_CC >> 8, "Sony CamCorder"},
		{CAM_VENDOR_SONY_AC >> 8, "Sony ActionCam"},
		{CAM_VENDOR_PANASONIC >> 8, "Panasonic Camera"},
		{CAM_VENDOR_SAMSUNG >> 8, "Samsung Camera"},
		{CAM_VENDOR_GOPRO >> 8, "GoPro Camera"},
		{0, NULL},
};

const CAMITEM WhiteBalance[] =
{
	{0xFFFF, "Default" },
	{0, "Auto" },
	{1, "Daylight" },
	{2, "Cloudy" },
	{3, "Tungsten" },
	{4, "Fluorescent" },
	{7, "Fluorescent H" },
	{6, "Custom" },
	{0, NULL },
};


const CAMITEM WhiteBalanceEos[] =
{
	{0xFFFF, "Default" },
	{0, "AWB" },
	{1, "Daylight" },
	{8, "Shadow" },
	{2, "Cloudy" },
	{3, "Tungsten" },
	{4, "Fluorescent" },
	{5, "Flash" },
	{6, "Center" },
	{9, "Custom" },
	{0, NULL },
};

const CAMITEM Focus[] =
{
	{0xffff, "At shoot"},
	{1, "Re-focus and lock it at 1st shoot"},
	{4, "Re-focus and lock it at 4th shoot"},
	{8, "Re-focus and lock it at every 8th shoot"},
	{0x20, "Re-focus/lock controllable by Channel0 (pulse width sense only)"},
	{0 , NULL},
};

const CAMITEM FocusDslr[] =
{
	{0, "AF not in use"},
	{2, "AF is in use (focus time 0.2 sec)"},
	{6, "AF is in use (focus time 0.6 sec)"},
	{10, "AF is in use (focus time 1 sec)"},
	{20, "AF is in use (focus time 2 sec)"},
	{30, "AF is in use (focus time 3 sec)"},
	{40, "AF is in use (focus time 4 sec)"},
	{50, "AF is in use (focus time 5 sec)"},
/*	{2, "AF is in use (focus time 1s)"},
	{10, "AF is in use (focus time 1s)"},
	{25, "AF is in use (focus time 2.5s)"},
	{30, "AF is in use (focus time 3.0s)"},
	{35, "AF is in use (focus time 3.5s)"},
	{40, "AF is in use (focus time 4.0s)"},*/
	{0 , NULL},
};

const CAMITEM AEProgramAuto[] =
{
	{ 1, "Auto" },
	{ 0, NULL},
};

const CAMITEM AEProgram[] =
{
	{ 1, "Auto" },
	{ 2, "Shutter priority" },
	{ 3, "Aperture priority" },
//	{ 4, "Manual (Camera 'M' settings)" },
	{ 4, "'M' settings (Set on field)" },
//	{ 0xFF, "Manual (CAMremote settings)" },
	{ 0xFF, "CAMremote settings" },
	{ 0, NULL},
};

const CAMITEM AEProgram2[] =
{
	{ 1, "Auto" },
	{ 2, "Shutter priority" },
	{ 3, "Aperture priority" },
//	{ 4, "Manual (camera settings)" },
	{ 0xFF, "CAMremote settings" },
	{ 0, NULL},
};

const CAMITEM AEProgram_EOS[] =
{
	{ 1, "Program AE" },
	{ 2, "Shutter priority" },
	{ 3, "Aperture priority" },
	{ 4, "'M' settings (Set on field)" },
//	{ 5, "Bulb" },
//	{ 0xFF, "Manual (CAMremote settings)" },
	{ 0xFF, "CAMremote settings" },
//	{ 6, "A-DEP" },
//	{ 7, "DEP" },
	{ 0, NULL},
};

const CAMITEM AEProgramNikon[] =
{
	{ 2, "Program AE" },
	{ 4, "Shutter priority" },
	{ 3, "Aperture priority" },
	{ 1, "'M' settings (Set on field)" },
//	{ 5, "Bulb" },
//	{ 0xFF, "Manual (CAMremote settings)" },
	{ 0xFF, "CAMremote settings" },
//	{ 6, "A-DEP" },
//	{ 7, "DEP" },
	{ 0, NULL},
};

const CAMITEM IsoSpeed_Canon1[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0040, "50" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
	{ 0 , NULL},
};

const CAMITEM IsoSpeed_Canon2[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0040, "80" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
//	{ 0x0000, "800" }, // TODO
//	{ 0x0000, "1000" }, // TODO
	{0 , NULL},
};

const CAMITEM IsoSpeed_Canon3[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0038, "25" },
	{ 0x0040, "50" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
	{ 0x0060, "800" },
	{ 0x0068, "1600" },
	{ 0x0070, "3200" },
	{ 0x0078, "6400" },
	{ 0x0080, "12800" },
	{0 , NULL},
};

const CAMITEM IsoSpeed_NikonPana[] =
{
	{ 0, "Auto" },
	{ 100, "100" },
	{ 125, "125" },
	{ 160, "160" },
	{ 200, "200" },
	{ 250, "250" },
	{ 320, "320" },
	{ 400, "400" },
	{ 500, "500" },
	{ 640, "640" },
	{ 800, "800" },
	{ 1000, "1000" },
	{ 1250, "1250" },
	{ 1600, "1600" },
	{ 2000, "2000" },
	{ 2500, "2500" },
	{ 3200, "3200" },
	{ 4000, "4000" },
	{ 5000, "5000" },
	{ 6400, "6400" },

	{0 , NULL},
};

const CAMITEM Quality2[] =
{
	{ 0xffff, "Default" },
   { 5, "Superfine" },
   { 3, "Fine" },
	{ 2, "Normal" },
   { 0 , NULL},
};

const CAMITEM Quality21[] =
{
	{ 0xffff, "Default" },
	{ 5, "Superfine" },
   { 3, "Fine" },
   { 2, "Normal" },
   { 4, "RAW" },
	{ 0 , NULL},

}; // D007 <- 2 (kui RAW) & <- 1 (kui JPEG)

/*const CAMITEM Size2[] =
{
		{ 0xffff, "Default" },
      { 0, "Large" },
      { 1, "Medium1" },
      { 3, "Medium2" },
      { 4, "Medium3" },
      { 2, "Small" },
      { 0 , NULL},
};
*/
const CAMITEM Size3[] =
{
	{ 0xffff, "Default" },
   { 0, "Large" },
   { 1, "Medium1" },
   { 3, "Medium2" },
   { 7, "Medium3" },
   { 2, "Small" },
	{ 0 , NULL},
};

const CAMITEM Size4[] =
{
	{ 0xffff, "Default" },
   { 0x3040, "L+RAW" },
   { 0x0040, "RAW" },
   { 0x0030, "L/Fine" },
   { 0x0020, "L/Normal" },
   { 0x0031, "M/Fine" },
   { 0x0021, "M/Normal" },
   { 0x0032, "S/Fine" },
   { 0x0022, "S/Normal" },
   { 0 , NULL},
};

const CAMITEM ServoCount[] =
{
	{1, "1"},
	{2, "2"},
	{3, "3"},
	{4, "4"},
	{5, "5"},
	{6, "6"},
	{7, "7"},
	{8, "8"},
	{9, "9"},
	{10, "10"},
	{101, "100"},
	{102, "200"},
	{103, "300"},
	{104, "400"},
	{105, "500"},
	{110, "1000"},
	{0x80 + 1, "Burst"},
	{0x80 + 2, "Burst 2s"},
	{0x80 + 3, "Burst 3s"},
	{0x80 + 4, "Burst 4s"},
	{0x80 + 5, "Burst 5s"},
	{0x80 + 6, "Burst 6s"},
	{0x80 + 7, "Burst 7s"},
	{0x80 + 8, "Burst 8s"},
	{0x80 + 9, "Burst 9s"},
	{0x80 + 10, "Burst 10s"},
	{0x40 + 1, "HDR:TV �1"},
	{0xC0    , "MOVIE REC"},
#ifdef XXX
	{0x40 + 2, "HDR:TV �2"},
	{0x40 + 3, "HDR:TV �3"},
	{0x31, "HDR:AV �1"},
	{0x32, "HDR:AV �2"},
	{0x33, "HDR:AV �3"},
#endif
	{0, NULL},
};

const CAMITEM ServoDelay[] =
{
	{0, "None"},
	{1, "0.1"},
	{2, "0.2"},
	{3, "0.3"},
	{4, "0.4"},
	{5, "0.5"},
	{6, "0.6"},
	{7, "0.7"},
	{8, "0.8"},
	{9, "0.9"},
	{10, "1"},
	{15, "1.5"},
	{20, "2"},
	{25, "2.5"},
	{30, "3"},
	{40, "4"},
	{50, "5"},
	{60, "6"},
	{70, "7"},
	{80, "8"},
	{80, "8"},
	{90, "9"},
	{100, "10"},
	{110, "11"},
	{120, "12"},
	{130, "13"},
	{140, "14"},
	{150, "15"},
	{160, "16"},
	{170, "17"},
	{180, "18"},
	{190, "19"},
	{200, "20"},
	{0, NULL},
};

const CAMITEM ServoDuration[] =
{
	{10, "0.1"},
	{20, "0.2"},
	{30, "0.3"},
	{40, "0.4"},
	{50, "0.5"},
	{60, "0.6"},
	{70, "0.7"},
	{80, "0.8"},
	{90, "0.9"},
	{100, "1"},
	{125, "1.25"},
	{150, "1.5"},
	{175, "1.75"},
	{200, "2"},
	{225, "2.25"},
	{250, "2.5"},
	{0, NULL},
};

const CAMITEM ServoPosition[] =
{
	{0, "None"},
	{10, "-140"},
	{20, "-130"},
	{30, "-120"},
	{40, "-110"},
	{50, "-100"},
	{60, "-90"},
	{70, "-80"},
	{80, "-70"},
	{90, "-60"},
	{100, "-50"},
	{105, "-45"},
	{110, "-40"},
	{115, "-35"},
	{120, "-30"},
	{125, "-25"},
	{130, "-20"},
	{135, "-15"},
	{140, "-10"},
	{145, "-5"},
	{150, "0"},
	{155, "5"},
	{160, "10"},
	{165, "15"},
	{170, "20"},
	{175, "25"},
	{180, "30"},
	{185, "35"},
	{190, "40"},
	{195, "45"},
	{200, "50"},
	{205, "55"},
	{210, "60"},
	{215, "65"},
	{220, "70"},
	{225, "75"},
	{230, "80"},
	{235, "85"},
	{240, "90"},
	{245, "95"},
	{250, "100"},
	{0, NULL},
};

const CAMITEM ServoPositionMs[] =
{
	{0, "None"},
	{90, "0.90"},
	{100, "1.00"},
	{105, "1.05"},
	{110, "1.10"},
	{115, "1.15"},
	{120, "1.20"},
	{125, "1.25"},
	{130, "1.30"},
	{135, "1.35"},
	{140, "1.40"},
	{145, "1.45"},
	{150, "1.50"},
	{155, "1.55"},
	{160, "1.60"},
	{165, "1.65"},
	{170, "1.70"},
	{175, "1.75"},
	{180, "1.80"},
	{185, "1.85"},
	{190, "1.90"},
	{195, "1.95"},
	{200, "2.00"},
	{205, "2.05"},
	{210, "2.10"},
	{215, "2.15"},
	{220, "2.20"},
	{225, "2.25"},
	{230, "2.30"},
	{0, NULL},
};

const CAMITEM Pause[] = {{0, "None"},
	{(0 << 7) | (1 << 3) | (1), "5 sec after every 3 shots"},
	{(0 << 7) | (1 << 3) | (2), "10 sec after every 3 shots"},
	{(0 << 7) | (3 << 3) | (2), "10 sec after every 9 shots"},
	{(0 << 7) | (1 << 3) | (6), "30 sec after every 3 shots"},
	{(0 << 7) | (2 << 3) | (6), "30 sec after every 6 shots"},
	{(0 << 7) | (3 << 3) | (6), "30 sec after every 9 shots"},
	{PAUSE_START_STOP, "CH0: 50%=Pause, 70%=Run"},
	{PAUSE_RESTART, "CH0: 50%=Idle, 70%=Start"},
//	{(0x80 | CTRLMODE_EXTCTRL_RC8), "4/8-button Radio"},
	{0, NULL}};


const CAMITEM IvmRepeat[] = {{0, "No Limit"}, {1, "1"}, {2, "2"}, {3, "3"}, {4, "4"}, {5, "5"}, {6, "6"}, {7, "7"}, {8, "8"}, {9, "9"},
			{10, "10"}, {11, "11"}, {12, "12"}, {13, "13"}, {14, "14"}, {15, "15"},
			{16, "16"}, {17, "17"}, {18, "18"}, {19, "19"}, {20, "20"}, {21, "21"},
			{22, "22"}, {23, "23"}, {24, "24"}, {25, "25"}, {26, "26"}, {27, "27"},
			{28, "28"}, {29, "29"},
			{30, "30"}, {31, "31"}, {32, "32"}, {33, "33"}, {34, "34"}, {35, "35"},
			{36, "36"}, {37, "37"}, {38, "38"}, {39, "39"},
			{40, "40"},
			{50, "50"}, {70, "70"}, {100, "100"}, {200, "200"}, {300, "300"}, {400, "400"}, {500, "500"}, {600, "600"}, {700, "700"}, {800, "800"}, {900, "900"}, {1000, "1000"}, {10000, "10000"}, {0, NULL}};

const CAMITEM IvmInterval[] = {
			{0, "Minimum"}, {1, "1 sec"}, {2, "2 sec"}, {3, "3 sec"}, {4, "4 sec"},
			{5, "5 sec"}, {6, "6 sec"}, {7, "7 sec"}, {8, "8 sec"}, {9, "9 sec"},
			{10, "10 sec"},  {15, "15 sec"}, {20, "20 sec"}, {25, "25 sec"},
         {30, "30 sec"}, {31, "31 sec"}, {32, "32 sec"}, {33, "33 sec"}, {34, "34 sec"}, {35, "35 sec"},
         {36, "36 sec"}, {37, "37 sec"}, {38, "38 sec"}, {39, "39 sec"},
			{40, "40 sec"},         {45, "45 sec"},
			{1*60, "1 min"}, {2*60, "2 min"}, {3*60, "3 min"}, {5*60, "5 min"}, {10*60, "10 min"},
			{30*60, "30 min"},
			{1*60*60, "1 hour"}, {2*60*60, "2 hours"}, {3*60*60, "3 hours"}, {4*60*60, "4 hours"},
			{5*60*60, "5 hours"}, {6*60*60, "6 hours"}, {7*60*60, "7 hours"}, {8*60*60, "8 hours"},
			{9*60*60, "9 hours"}, {10*60*60, "10 hours"}, {11*60*60, "11 hours"}, {11.5*60*60, "11:30h"},
			{12*60*60, "12 hours"},
			{0xFF00 + 15, "1.5 sec"}, {0xFF00 + 25, "2.5 sec"},
			{0, NULL}};
//			{12*60*60, "12 hours"}, {23*60*60, "23 hours"}, {24*60*60, "24 hours"}, {0, NULL}};

const CAMITEM IvmDuration[] = {
            {0, "-"}, {1, "1 sec"}, {2, "2 sec"}, {3, "3 sec"}, {4, "4 sec"},
            {5, "5 sec"}, {6, "6 sec"}, {7, "7 sec"}, {8, "8 sec"}, {9, "9 sec"},
            {10, "10 sec"},  {15, "15 sec"}, {20, "20 sec"}, {25, "25 sec"},
            {30, "30 sec"}, {31, "31 sec"}, {32, "32 sec"}, {33, "33 sec"}, {34, "34 sec"}, {35, "35 sec"},
            {36, "36 sec"}, {37, "37 sec"}, {38, "38 sec"}, {39, "39 sec"},
            {40, "40 sec"},         {45, "45 sec"},
            {1*60, "1 min"}, {2*60, "2 min"}, {3*60, "3 min"}, {4*60, "4 min"}, {5*60, "5 min"},
            {6*60, "6 min"}, {7*60, "7 min"}, {8*60, "8 min"}, {9*60, "9 min"},
            {10*60, "10 min"}, {11*60, "11 min"}, {12*60, "12 min"},  {13*60, "13 min"}, {14*60, "14 min"},
            {15*60, "15 min"}, {16*60, "16 min"},
            {20*60, "20 min"}, {30*60, "30 min"},
            {1*60*60, "1 hour"}, {2*60*60, "2 hours"}, {3*60*60, "3 hours"}, {4*60*60, "4 hours"},
            {5*60*60, "5 hours"}, {6*60*60, "6 hours"}, {7*60*60, "7 hours"}, {8*60*60, "8 hours"},
            {0, NULL}};

const CAMITEM Uvlo[] = { {0, "None"}, {33, "3.3 V"}, {34, "3.4 V"}, {35, "3.5 V"}, {36, "3.6 V"},
			{37, "3.7 V"}, {38, "3.8 V"},
			{47, "4.7 V"}, {48, "4.8 V"}, {49, "4.9 V"},
			{70, "7.0 V"}, {71, "7.1 V"}, {72, "7.2 V"},
			{80, "8.0 V"}, {81, "8.1 V"}, {82, "8.2 V"},
			{90, "9.0 V"},
			{0, NULL}};

const CAMITEM TimeRadioWakeup[] = {{0, "None"},
			{1, "1 min"}, {2, "2 min"}, {3, "3 min"}, {4, "4 min"}, {5, "5 min"},
			{10, "10 min"}, {20, "20 min"}, {30, "30 min"},
			{1*60, "1 hour"}, {2*60, "2 hours"}, {3*60, "3 hours"}, {4*60, "4 hours"},
			{5*60, "5 hours"}, {6*60, "6 hours"}, {7*60, "7 hours"}, {8*60, "8 hours"},
			{1*60*24, "1 day"}, {2*60*24, "2 days"}, {3*60*24, "3 days"}, {4*60*24, "4 days"}, {5*60*24, "5 days"},
			{0, NULL}};

const CAMITEM StartupDelay[] = {{0, "None"},
			{1, "1 sec"}, {2, "2 sec"}, {3, "3 sec"}, {4, "4 sec"}, {5, "5 sec"},
			{10, "10 sec"}, {30, "30 sec"}, {50, "50 sec"},
			{100, "1 min"},  {120, "2 min"},  {130, "3 min"},
			{140, "4 min"}, {150, "5 min"}, {170, "7 min"}, {190, "9 min"}, {200, "10 min"},
			{201, "1 hour"},{202, "2 hours"}, {203, "3 hours"},{204, "4 hours"},
			{205, "5 hours"},{206, "6 hours"}, {207, "7 hours"},{208, "8 hours"},
			{209, "9 hours"},{210, "10 hours"}, {211, "11 hours"},
			{212, "12 hours"}, {213, "13 hours"}, {214, "14 hours"}, {215, "15 hours"},
			{216, "16 hours"}, {217, "17 hours"}, {218, "18 hours"}, {219, "19 hours"},
			{220, "20 hours"}, {221, "21 hours"}, {222, "22 hours"}, {223, "23 hours"},
			{224, "24 hours"}, {0, NULL}};


const CAMITEM TriggerEvent[] = {
	{0x00, "None"},
	{0x01, "FET On/Off"}, {0x02, "5V LDO On/Off"}, {0x06, "LDO+VBUS On/Off"},
	{0x03, "FET+LDO On/Off"},
	{0, NULL}};

/*const CAMITEM ServoPosition[] =
{
	{0, "None"},
	{10, "100"},
	{20, "200"},
	{30, "300"},
	{40, "400"},
	{50, "500"},
	{60, "600"},
	{70, "700"},
	{80, "800"},
	{90, "900"},
	{100, "1000"},
	{105, "1050"},
	{110, "1100"},
	{115, "1150"},
	{120, "1200"},
	{125, "1250"},
	{130, "1300"},
	{135, "1350"},
	{140, "1400"},
	{145, "1450"},
	{150, "1500"},
	{155, "1550"},
	{160, "1600"},
	{165, "1650"},
	{170, "1700"},
	{175, "1750"},
	{180, "1800"},
	{185, "1850"},
	{190, "1900"},
	{195, "1950"},
	{200, "2000"},
	{205, "2050"},
	{210, "2100"},
	{215, "2150"},
	{220, "2200"},
	{225, "2250"},
	{230, "2300"},
	{235, "2350"},
	{240, "2400"},
	{245, "2450"},
	{250, "2500"},
	{0, NULL},
};*/

#else

// This is uC part
// Lets save controller's memory and define only important items
const CAMITEM CtrlType[] = {{0, NULL}};
const CAMITEM CtrlType0[] = {{0, NULL}};
const CAMITEM CtrlType1[] = {{0, NULL}};
const CAMITEM CtrlType2[] = {{0, NULL}};
const CAMITEM CtrlType3[] = {{0, NULL}};
const CAMITEM CtrlType4[] = {{0, NULL}};
const CAMITEM CtrlType5[] = {{0, NULL}};
const CAMITEM CtrlType6[] = {{0, NULL}};
const CAMITEM CtrlType7[] = {{0, NULL}};
const CAMITEM CtrlType8[] = {{0, NULL}};
const CAMITEM CtrlType9[] = {{0, NULL}};
const CAMITEM CtrlType10[] = {{0, NULL}};
const CAMITEM CtrlType11[] = {{0, NULL}};
const CAMITEM CtrlType12[] = {{0, NULL}};
const CAMITEM CtrlType13[] = {{0, NULL}};
const CAMITEM CtrlType14[] = {{0, NULL}};
const CAMITEM CtrlType15[] = {{0, NULL}};
const CAMITEM CtrlType16[] = {{0, NULL}};
const CAMITEM CtrlType17[] = {{0, NULL}};
const CAMITEM CtrlType18[] = {{0, NULL}};
const CAMITEM CtrlType19[] = {{0, NULL}};
const CAMITEM CtrlType20[] = {{0, NULL}};
const CAMITEM ControlModeX[] = {{0, NULL}};
const CAMITEM Options1[] = {{0, NULL}};
const CAMITEM ControlMode[] = {{0, NULL}};
const CAMITEM Options2[] = {{0, NULL}};
const CAMITEM WhiteBalance[] = {{0, NULL}};
const CAMITEM WhiteBalanceEos[] = {{0, NULL}};
const CAMITEM Focus[] = {{0, NULL}};
const CAMITEM FocusDslr[] = {{0, NULL}};
const CAMITEM AEProgramAuto[] = {{0, NULL}};
const CAMITEM AEProgram[] = {{0, NULL}};
const CAMITEM AEProgram2[] = {{0, NULL}};
const CAMITEM AEProgram_EOS[] = {{0, NULL}};
const CAMITEM AEProgramNikon[] = {{0, NULL}};
const CAMITEM ServoCount[] = {{0, NULL}};
const CAMITEM ServoDelay[] = {{0, NULL}};
const CAMITEM ServoDuration[] = {{0, NULL}};
const CAMITEM ServoPosition[] = {{0, NULL}};
const CAMITEM Pause[] = {{0, NULL}};

const CAMITEM IsoSpeed_Canon1[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0040, "50" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
	{ 0 , NULL},
};

const CAMITEM IsoSpeed_Canon2[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0040, "80" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
	{ 0x0000, "800" },
	{ 0x0000, "1000" },
	{0 , NULL},
};

const CAMITEM IsoSpeed_Canon3[] =
{
	{ 0xffff, "Default"  },
	{ 0x0000, "Auto" },
	{ 0x0040, "80" },
	{ 0x0048, "100" },
	{ 0x0050, "200" },
	{ 0x0058, "400" },
	{ 0x0060, "800" },
	{ 0x0068, "1600" },
	{ 0x0070, "3200" },
	{ 0x0078, "6400" },
	{ 0x0080, "12800" },
	{0 , NULL},
};

const CAMITEM IsoSpeed_NikonPana[] =
{
	{ 0, "Auto" },
	{ 100, "100" },
	{ 125, "125" },
	{ 160, "160" },
	{ 200, "200" },
	{ 250, "250" },
	{ 320, "320" },
	{ 400, "400" },
	{ 500, "500" },
	{ 640, "640" },
	{ 800, "800" },
	{ 1000, "1000" },
	{ 1250, "1250" },
	{ 1600, "1600" },
	{ 2000, "2000" },
	{ 2500, "2500" },
	{ 3200, "3200" },
	{ 4000, "4000" },
	{ 5000, "5000" },
	{ 6400, "6400" },

	{0 , NULL},
};

const CAMITEM Quality2[] =
{
	{ 0xffff, "Default" },
	{ 5, "Superfine" },
	{ 3, "Fine" },
	{ 2, "Normal" },
	{ 0 , NULL},
};

const CAMITEM Quality21[] =
{
	{ 0xffff, "Default" },
   { 5, "Superfine" },
   { 3, "Fine" },
   { 2, "Normal" },
   { 4, "RAW" },
   { 0 , NULL},
}; // D007 <- 2 (kui RAW) & <- 1 (kui tagasi)

//IMGF=0000_0001<D122=0001>



/*const CAMITEM Size2[] =
{
	{ 0xffff, "Default" },
   { 0, "Large" },
   { 1, "Medium1" },
   { 3, "Medium2" },
   { 4, "Medium3" },
   { 2, "Small" },
   { 0 , NULL},
};
*/
const CAMITEM Size3[] =
{
	{ 0xffff, "Default" },
   { 0, "Large" },
   { 1, "Medium1" },
   { 3, "Medium2" },
   { 4, "Medium3" },
   { 2, "Small" },
   { 0 , NULL},
};

const CAMITEM Size4[] =
{
	{ 0xffff, "Default" },
   { 0x3640, "L+RAW" },
   { 0x0640, "RAW" },
   { 0x0000, "L/F" },
   { 0x0020, "L/N" },
   { 0x0031, "M/F" },
   { 0x0021, "M/N" },
   { 0x0032, "S/F" },
   { 0x0002, "S/N" },
   { 0 , NULL},
};

#endif

const CAMITEM Zoom_Canon6x[] =
{
	{ 0, "0"},
	{ 1, "1.2"},
	{ 2, "1.5"},
	{ 3, "2"},
	{ 4, "2.3"},
	{ 5, "3"},
	{ 6, "3.2"},
	{ 7, "4"},
	{ 8, "4.5"},
	{ 9, "5"},
	{ 10, "5.5"},
	{ 11, "6"},
	{ 0 , NULL},
};

const CAMITEM Zoom_Canon35x[] =
{
	{ 0, "0"},
	{ 1, "1.2"},
	{ 2, "1.5"},
	{ 3, "2"},
	{ 4, "2.3"},
	{ 5, "3"},
	{ 6, "3.5"},
	{ 0 , NULL},
};

const CAMITEM Zoom_Canon4x[] =
{
	{ 0, "0"},
	{ 1, "1.2"},
	{ 2, "1.5"},
	{ 3, "2"},
	{ 4, "2.3"},
	{ 5, "3"},
	{ 6, "3.2"},
	{ 7, "4.0"},
	{ 0 , NULL},
};

const CAMITEM Zoom_Canon10x[] =
{
	{ 0, "0"},
	{ 1, "1.2"},
	{ 2, "1.5"},
	{ 3, "2"},
	{ 4, "2.3"},
	{ 5, "3"},
	{ 6, "3.2"},
	{ 7, "4"},
	{ 8, "4.5"},
	{ 9, "5"},
	{ 10, "5.5"},
	{ 11, "6"},
	{ 18, "10"}, // ???? TODO
	{ 0 , NULL},
};

const CAMITEM Zoom_Canon12x[] =
{
	{ 0, "0"},
	{ 1, "1.2"},
	{ 2, "1.5"},
	{ 3, "2"},
	{ 4, "2.3"},
	{ 5, "3"},
	{ 6, "3.2"},
	{ 7, "4"},
	{ 8, "4.5"},
	{ 9, "5"},
	{ 10, "5.5"},
	{ 11, "6"},
	{ 12, "6.5"},
	{ 13, "7"},
	{ 14, "7.5"},
	{ 15, "8"},
	{ 16, "8.5"},
	{ 17, "9"},
	{ 18, "9.5"},
	{ 19, "10"},
	{ 20, "10.5"},
	{ 21, "11"},
	{ 22, "12"}, // ???? TODO
	{ 0 , NULL},
};

const CAMITEM ZoomRollServo[] =
{
	{0x0, "Default" },
	{ZOOM_SERVO_CH3, "Zoom/Roll Servo: CH3" },
	{ZOOM_SERVO_3POS_CH3, "Zoom Servo (3-pos): CH3" },
	{ZOOM_SERVO_HILOW_CH3, "Signal On/Off: CH3" },
	{ZOOM_RELAY_HILOW_CH3, "Relay On/Off: CH3" },
//	{ZOOM_SERVO_HILOW_CH2, "Signal On/Off: CH2" },
	{ZOOM_TO_MF, "-> Manual Focus Control" },
	{0, NULL },
};

const CAMITEM TvNikon[] =
{
/*
		 Tv=0001_1F40 8000
		 Tv=0001_1900 6400
		 Tv=0001_1388 5000
		 Tv=0001_0FA0 4000
		 Tv=0001_0C80 3200
		 Tv=0001_09C4 2500
		 Tv=0001_07D0 2000
		 Tv=0001_0640 1600
		 Tv=0001_04E2 1250
		 Tv=0001_03E8 1000
		 Tv=0001_0320 800
		 Tv=0001_0280 640
		 Tv=0001_01F4 500
		 Tv=0001_0190 400
		 Tv=0001_0140 320
		 Tv=0001_00FA 250
		 Tv=0001_00C8 200
		 Tv=0001_00A0 160
		 Tv=0001_007D 125
		 Tv=0001_0064 100
		 Tv=0001_0050 80
		 Tv=0001_003C 60
		 Tv=0001_0032 50
		 Tv=0001_0028 40
		 Tv=0001_001E 30
		 Tv=0001_0019 25
		 Tv=0001_0014 20
		 Tv=0001_000F 15
		 Tv=0001_000D 13
		 Tv=0001_000A 10
		 Tv=0001_0008 8
		 Tv=0001_0006 6
		 Tv=0001_0005 5
		 Tv=0001_0004 4
		 Tv=0001_0003 3
		 Tv=000A_0019 1/2.5=0.4
		 Tv=0001_0002 1/2=0.5
		 Tv=000A_0010 1/1.6=0.625
		 Tv=000A_000D 1/1.3=0.77
		 Tv=0001_0001 1"
		 Tv=000D_000A 1.3"
		 Tv=0010_000A 1.6"
		 Tv=0002_0001 2"
		 Tv=0019_000A 2.5"
		 Tv=0003_0001 3"
		 Tv=0004_0001 4"
		 Tv=0005_0001 5"
		 Tv=0006_0001 6"
		 Tv=0008_0001 8"
		 Tv=000A_0001 10"
		 Tv=000D_0001 13"
		 Tv=000F_0001 15"
		 Tv=0014_0001 20"
		 Tv=0019_0001 25"
		 Tv=001E_0001 30"
		 Tv=FFFF_FFFF bulb
		 Tv=FFFF_FFFE x250
		 */

	{ 0x11F40, "1/8000" },
	{ 0x11900, "1/6400" },
	{ 0x11388, "1/5000" },
	{ 0x10FA0, "1/4000" },
	{ 0x10C80, "1/3200" },
	{ 0x109C4, "1/2500" },
	{ 0x107D0, "1/2000" },
	{ 0x10640, "1/1600" },
	{ 0x104E2, "1/1250" },
	{ 0x103E8, "1/1000" },
	{ 0x10320, "1/800" },
	{ 0x10280, "1/640" },
	{ 0x101F4, "1/500" },
	{ 0x10190, "1/400" },
	{ 0x10140, "1/320" },
	{ 0x100FA, "1/250" },
	{ 0x100C8, "1/200" },
	{ 0x100A0, "1/160" },
	{ 0x1007D, "1/125" },
	{ 0x10064, "1/100" },
	{ 0x10050, "1/80" },
	{ 0x1003C, "1/60" },
	{ 0x10032, "1/50" },
	{ 0x10028, "1/40" },
	{ 0x1001E, "1/30" },
	{ 0x10019, "1/25" },
	{ 0x10014, "1/20" },
	{ 0x1000F, "1/15" },
	{ 0x1000D, "1/13" },
	{ 0x1000A, "1/10" },
	{ 0x10008, "1/8" },
	{ 0x10006, "1/6" },
	{ 0x10005, "1/5" },
	{ 0x10004, "1/4" },
	{ 0x10003, "1/3" },

	{ 0 , NULL},
};

const CAMITEM Tv[] =
{
//		{ 0     , "Auto" },
	{ 0x00a0, "1/8000" },
	{ 0x009d, "1/6400" },
	{ 0x009c, "1/6000" },
	{ 0x009b, "1/5000" },
	{ 0x0098, "1/4000" },
	{ 0x0095, "1/3200" },
	{ 0x0094, "1/3000" },
	{ 0x0093, "1/2500" },
	{ 0x0090, "1/2000" },
	{ 0x008d, "1/1600" },
	{ 0x008b, "1/1250" },
	{ 0x0088, "1/1000" },
	{ 0x0085, "1/800" },
	{ 0x0083, "1/640" },
	{ 0x0080, "1/500" },
	{ 0x007d, "1/400" },
	{ 0x007b, "1/320" },
	{ 0x0078, "1/250" },
	{ 0x0075, "1/200" },
	{ 0x0073, "1/160" },
	{ 0x0070, "1/125" },
	{ 0x006d, "1/100" },
	{ 0x006b, "1/80" },
	{ 0x0068, "1/60" },
	{ 0x0065, "1/50" },
	{ 0x0063, "1/40" },
	{ 0x0060, "1/30" },
	{ 0x005d, "1/25" },
	{ 0x005b, "1/20" },
	{ 0x0058, "1/15" },
	{ 0x0055, "1/13" },
	{ 0x0053, "1/10" },
	{ 0x0050, "1/8" },
	{ 0x004d, "1/6" },
	{ 0x004b, "1/5" },
	{ 0x0048, "1/4" },
	{ 0x0045, "0\"3" },
	{ 0x0043, "0\"4" },
	{ 0x0040, "0\"5" },
	{ 0x003d, "0\"6" },
	{ 0x003b, "0\"8" },
	{ 0x0038, "1\"" },
	{ 0x0035, "1\"3" },
	{ 0x0033, "1\"6" },
	{ 0x0030, "2\"" },
	{ 0x002d, "2\"5" },
	{ 0x002b, "3\"2" },
	{ 0x0028, "4\"" },
	{ 0x0025, "5\"" },
	{ 0x0023, "6\"" },
	{ 0x0020, "8\"" },
	{ 0x001d, "10\"" },
	{ 0x001b, "13\"" },
	{ 0x0018, "15\"" },
	{ 0xFFFE, "On" },

	{ 0 , NULL},
};

//0x500D = Tv (1"=10000:2710, 60"=60000:927C0, 1/50=C8(200), 1/800=0C, 1/8000=01

const CAMITEM TvPana[] =
{
//		{ 0     , "Auto" },
//	{ 0x0001, "1/8000" }, // 1=same as 1/6400
	{ 0x0001, "1/6400" },
	{ 0x0002, "1/5000" },
	{      2, "1/4000" },
	{      3, "1/3200" },
	{      4, "1/2500" },
	{      5, "1/2000" },
	{      6, "1/1600" },
	{      7, "1/1300" },
	{     10, "1/1000" },
	{     13, "1/800" },
	{     15, "1/640" },
	{     20, "1/500" },
	{     25, "1/400" },
	{     31, "1/320" },
	{     40, "1/250" },
	{     50, "1/200" },
	{     62, "1/160" },
	{     80, "1/125" },
	{    100, "1/100" },
	{    125, "1/80" },
	{    166, "1/60" },
	{    200, "1/50" },
	{    250, "1/40" },
	{    333, "1/30" },
	{    400, "1/25" },
	{    500, "1/20" },
	{    666, "1/15" },
	{    769, "1/13" },
	{   1000, "1/10" },
	{   1250, "1/8" },
	{   1666, "1/6" },
	{   2000, "1/5" },
	{   2500, "1/4" },
	{   3125, "0\"3" },
	{   4000, "0\"4" },
	{   5000, "0\"5" },
	{   6250, "0\"6" },
	{   7692, "0\"8" },
	{  10000, "1\"" },
	{  13000, "1\"3" },
	{  16000, "1\"6" },
	{  20000, "2\"" },
	{  25000, "2\"5" },
	{  30000, "3\"2" },
	{  40000, "4\"" },
	{  50000, "5\"" },
	{  60000, "6\"" },
	{ 0xFFFE, "On" },

	{ 0 , NULL},
};

const CAMITEMINT16 TvTimesCanon[] =
{
	{ 0xa0, 125 }, // 1/8000
	{ 0x9d, 156 }, // 1/6400
	{ 0x9c, 166 }, // 1/6000
	{ 0x9b, 200 }, // 1/5000
	{ 0x98, 250 }, // 1/4000
	{ 0x95, 313 }, // 1/3200
	{ 0x94, 333 }, // 1/3000
	{ 0x93, 400 }, // 1/2500
	{ 0x90, 500 }, // 1/2000
	{ 0x8d, 625 }, // 1/1600
	{ 0x8b, 800 }, // 1/1250
	{ 0x88, 1000 }, // 1/1000
	{ 0x85, 1250 }, // 1/800
	{ 0x83, 1562 }, // 1/640
	{ 0x80, 2000 }, // 1/500
	{ 0x7d, 2500 }, // 1/400
	{ 0x7b, 3125 }, // 1/320
	{ 0x78, 4000 }, // 1/250
	{ 0x75, 5000 }, // 1/200
	{ 0x73, 6250 }, // 1/160
	{ 0x70, 8000 }, // 1/125
	{ 0x6d, 10000 }, // 1/10
	{ 0x6b, 12500 }, // 1/80
	{ 0x68, 16666 }, // 1/60
	{ 0x65, 20000 }, // 1/50
	{ 0x63, 25000 }, // 1/40
	{ 0x60, 33333 }, // 1/30
	{ 0x5d, 40000 }, // 1/25
	{ 0x5b, 50000 }, // 1/20
//		----
	{ 0x58, 67 }, // 1/15
	{ 0x55, 77 }, // 1/13
	{ 0x53, 100 }, // 1/10
	{ 0x50, 125 }, // 1/8
	{ 0x4d, 167 }, // 1/6
	{ 0x4b, 200 }, // 1/5
	{ 0x48, 250 }, // 1/4
	{ 0x45, 300 }, // 0.3
	{ 0x43, 400 }, // 0.4
	{ 0x40, 500 }, // 0.5
	{ 0x3d, 600 }, // 0.6
	{ 0x3b, 800 }, // 0.8
	{ 0x38, 1000 }, // 1
	{ 0x35, 1300 }, // 1.3
	{ 0x33, 1600 }, // 1.6
	{ 0x30, 2000 }, // 2
	{ 0x2d, 2500 }, // 2.5
	{ 0x2b, 3200 }, // 3.2
	{ 0x28, 4000 }, // 4
	{ 0x25, 5000 }, // 5
	{ 0x23, 6000 }, // 6
	{ 0x20, 8000 }, // 8
	{ 0x1d, 10000 }, // 10
	{ 0x1b, 13000 }, // 13
	{ 0x18, 15000 }, // 15
	{ 0 , NULL},
};


const CAMITEM TvChdk[] =
{
	{ 0x00a0, " " },
	{ 0x0058, "1/15" },
	{ 0x0055, "1/13" },
	{ 0x0053, "1/10" },
	{ 0x0050, "1/8" },
	{ 0x004d, "1/6" },
	{ 0x004b, "1/5" },
	{ 0x0048, "1/4" },
	{ 0x0045, "0\"3" },
	{ 0x0043, "0\"4" },
	{ 0x0040, "0\"5" },
	{ 0x003d, "0\"6" },
	{ 0x003b, "0\"8" },
	{ 0x0038, "1\"" },
	{ 0x0035, "1\"3" },
	{ 0x0033, "1\"6" },
	{ 0x0030, "2\"" },
	{ 0x002d, "2\"5" },
	{ 0x002b, "3\"2" },

	{ 0 , NULL},
};

const CAMITEM AvCanon[] =
{
//	{ 0xffff, "Auto" },
	{ 0x0010, "1.4" },
	{ 0x0013, "1.6" },
	{ 0x0015, "1.8" },
	{ 0x0018, "2.0" },
	{ 0x001B, "2.2" },
	{ 0x001D, "2.5" },
	{ 0x0020, "2.8" },
	{ 0x0023, "3.2" },
	{ 0x0025, "3.5" },
	{ 0x0028, "4.0" },
	{ 0x002b, "4.5" },
	{ 0x002d, "5" },
	{ 0x0030, "5.6" },
	{ 0x0033, "6.3" },
	{ 0x0035, "7.1" },
	{ 0x0038, "8.0" },
	{ 0x003B, "9.0" },
	{ 0x003D, "10" },
	{ 0x0040, "11" },
	{ 0x0043, "13" },
	{ 0x0045, "14" },
	{ 0x0048, "16" },
	{ 0x004D, "18" },
	{ 0x0050, "22" },
	{ 0 , NULL},
};

const CAMITEM AvNikonPana[] =
{
//	{ 0xffff, "Auto" },
/*	{ 140, "1.4" },
	{ 160, "1.6" },
	{ 180, "1.8" },
	{ 200, "2.0" },
	{ 220, "2.2" },
	{ 250, "2.5" },
	{ 280, "2.8" },*/
	{ 320, "3.2" },
	{ 350, "3.5" },
	{ 400, "4.0" },
	{ 450, "4.5" },
	{ 500, "5" },
	{ 560, "5.6" },
//	{ 580, "5.8" }, // Only Pana!!!
	{ 630, "6.3" },
	{ 710, "7.1" },
	{ 800, "8.0" },
	{ 900, "9.0" },
	{ 1000, "10" },
	{ 1100, "11" },
	{ 1300, "13" },
	{ 1400, "14" },
	{ 1600, "16" },
	{ 1800, "18" },
	{ 2000, "20" },
	{ 2200, "22" },/*
	{ 2500, "25" },
	{ 2900, "29" },
	{ 3200, "32" },*/
	{ 0 , NULL},
};

const CAMITEM AfPoints[] =
{
	{0x0003, "(*)" },
	{0x0004, "( )" },
	{0x0001, " * " },
	{0x0005, "   " },
	{0 , NULL},
};

const CAMITEM TvComp[] =
{
	{0x00F0, "-2" },
	{0x00F3, "-1.6" },
	{0x00F5, "-1.3" },
	{0x00F8, "-1" },
	{0x00FB, "-0.6" },
	{0x00FD, "-0.3" },
	{0x0000, "0" },
	{0x0003, "0.3" },
	{0x0005, "0.6" },
	{0x0008, "1" },
	{0x000b, "1.3" },
	{0x000d, "1.6" },
	{0x0010, "2" },
	{0 , NULL},
};

const BTNITEM ButtonEosTv =
{
	CMDCAM, CAMTASK_SHUTTERSPEED, CAMTASK_SHUTTERSPEED_UP, CAMTASK_SHUTTERSPEED_DOWN,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM | ATTR_BN_TYPE_PTPVALUE,
	Tv, "Tv\n?"
};

const BTNITEM ButtonEosAv =
{
	CMDCAM, CAMTASK_APERTURE, CAMTASK_APERTURE_UP, CAMTASK_APERTURE_DOWN,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM | ATTR_BN_TYPE_PTPVALUE,
	AvCanon, "Av\n?"
};

const BTNITEM ButtonEosIso =
{
	CMDCAM, CAMTASK_ISO, CAMTASK_ISO_UP, CAMTASK_ISO_DOWN,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM | ATTR_BN_TYPE_PTPVALUE,
	IsoSpeed_Canon3, "ISO\n?"
};

const BTNITEM ButtonEosMF =
{
	CMDCAM, CAMTASK_FOCUS, CAMTASK_FOCUS_UP1, CAMTASK_FOCUS_DOWN1,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM /*| ATTR_BN_TYPE_PTPVALUE*/,
	NULL, "MF"
};

const BTNITEM ButtonEosAF =
{
	CMDCAM, CAMTASK_HALF_PRESS_ON, CAMTASK_HALF_PRESS_ON, CAMTASK_HALF_PRESS_ON,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM /*| ATTR_BN_TYPE_PTPVALUE*/,
	NULL, "AF"
};

const BTNITEM ButtonEosMag =
{
	CMDCAM, CAMTASK_ZOOM, CAMTASK_ZOOMIN, CAMTASK_ZOOMOUT,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM /*| ATTR_BN_TYPE_PTPVALUE*/,
	NULL, "Mag"
};

const BTNITEMP ButtonsCanonEos[] =
{
	&ButtonEosAF,
	&ButtonEosMF,
	&ButtonEosAv,
	&ButtonEosTv,
	&ButtonEosIso,
	&ButtonEosMag,
	NULL
};

const BTNITEM ButtonPsIso =
{
	CMDCAM, CAMTASK_ISO, CAMTASK_ISO_UP, CAMTASK_ISO_DOWN,
	ATTR_BN_UPDATE_LIVE | ATTR_BN_TYPE_CAMITEM | ATTR_BN_TYPE_PTPVALUE,
	IsoSpeed_Canon1, "ISO\n?"
};

const BTNITEMP ButtonsCanonPs[] =
{
	&ButtonEosTv,
	&ButtonEosAv,
	-1,
	&ButtonPsIso,
	NULL
};

const CAMDESCRIPTOR CameraDesc[] = {
{
	0xFFFF, "- Not Selected -", 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
},

{
	CAMMODEL_AUTODETECT_PTP, // 0xFFFE
	"Auto Detect (PTP cameras only)",
	0, 0, 0, NULL, NULL, NULL, NULL, NULL, ZoomRollServo, CtrlType1,
	Options1,
	Options2,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMMODEL_AUTODETECT_ANY_SERVO, // 0xFFFD
	"Any (Uses servo control only)",
	0, 0,
	CAMATTR_USB_BURST,
	NULL, NULL, Tv, NULL, NULL, ZoomRollServo, CtrlType0,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMMODEL_AUTODETECT_ANY_CHDK, // 0xFFFC
	"Any (CHDK Compatible)",
	0, 0, 0, NULL, NULL,
	TvChdk,
	NULL, NULL, NULL, CtrlType4,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMMODEL_AUTODETECT_ANY_E3, // 0xFFFC
	"Any (Remote Port Compatible)",
	0, 0, CAMATTR_USB_BURST, NULL, NULL,
	Tv,
	NULL,
	FocusDslr,
	ZoomRollServo, CtrlType10,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_BLACKMAGIC,
	"Blackmagic Cameras",
	0x0, 0x0,
	0, // Attr
	NULL,
	NULL,
	NULL,
	NULL,
	FocusDslr,
	ZoomRollServo,
	CtrlType15,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_CANON_IR,
	"Any Canon with IR",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	1 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot (Common)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM |
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType16,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	2 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot G5/G6",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM |
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType2,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality21,
	NULL,
},

{
	3 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot G7",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon6x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	4 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot G9",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon6x,
	CtrlType12,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality21,
	NULL,
},

{
	5 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot G10",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon6x,
	CtrlType13,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality21,
	&ButtonsCanonPs,
},


{
	0x11 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot A70",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgram2,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType1,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	0x13 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot A310",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgramAuto,
	NULL,//Tv,
	NULL,//Av,
	NULL,//Focus,
	NULL, // No zoom
	CtrlType1,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	0x16 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot A510/A520",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgram2,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType1,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	0x17 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot A620",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	0x18 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot A640",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon4x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	0x22 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot S1/S2/S3/S5",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon12x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},


{
	CAMERA_CANON_S70,
	"Canon PowerShot S60/70/80",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon1,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon35x,
	CtrlType2,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality21,
	NULL,
},

{
	CAMERA_CANON_S95,
	"Canon PowerShot S95/S100",
	0, 0, 0, NULL, NULL,
	TvChdk,
	NULL, NULL, ZoomRollServo, CtrlType4,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},
/*
{
	0x28 | CAM_VENDOR_CANON_PS,
	"Canon PowerShot S3IS/S5IS",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon12x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
},
*/
{
	CAMERA_CANON_SX100,
	"Canon PowerShot SX100",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_ZOOM,
	IsoSpeed_Canon2,
	AEProgram,
	Tv,
	AvCanon,
	Focus,
	Zoom_Canon10x,
	CtrlType3,
	Options1,
	Options2,
	ControlMode,
	WhiteBalance,
	Size3,
	Quality2,
	NULL,
},

{
	CAMERA_CANON_EOSPTP,
	"Canon EOS-Series",
	0x0, 0x0,
	CAMATTR_USB_SHOOT /*| CAMATTR_IR_SHOOT*/ | CAMATTR_USB_BURST,
	IsoSpeed_Canon3,
	AEProgram_EOS,
	Tv,
	AvCanon,
	FocusDslr,//Focus,
	ZoomRollServo,//Zoom
	CtrlType8,
	Options1,
	Options2,
	ControlMode,
	WhiteBalanceEos,
	Size4,
	NULL, //Quality2,
	&ButtonsCanonEos,
},

{
	CAMERA_CANON_EOSPTP2,
	"Canon EOS 5D/6D/7D-series",
	0x0, 0x0,
	CAMATTR_USB_SHOOT /*| CAMATTR_IR_SHOOT*/ | CAMATTR_USB_BURST,
	IsoSpeed_Canon3,
	AEProgram_EOS,
	Tv,
	AvCanon,
	FocusDslr,//Focus,
	ZoomRollServo,//Zoom
	CtrlType8,
	Options1,
	Options2,
	ControlMode,
	WhiteBalanceEos,
	Size4,
	NULL, //Quality2,
	&ButtonsCanonEos,
},

{
	CAMERA_CANON_CC,
	"Canon Camcorder",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_FLIR_VUE,
	"Flir Vue Pro",
	0, 0,
	CAMATTR_USB_BURST,
	NULL, NULL, Tv, NULL, NULL, ZoomRollServo, CtrlType,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_GOPRO_1BUT,
	"GoPro Heros (1-Button Mode)",
	0, 0, 0, NULL, NULL,
	Tv,
	NULL,
	NULL,//FocusDslr,
	NULL, CtrlType14,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_GOPRO2, // 0xFFFC
	"GoPro Hero1/2",
	0, 0, 0, NULL, NULL,
	Tv,
	NULL,
	NULL,//FocusDslr,
	NULL, CtrlType14,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_GOPRO3V,
	"GoPro Hero3",
	0, 0, 0, NULL, NULL,
	Tv,
	NULL,
	NULL,//FocusDslr,
	NULL, CtrlType14,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

/*
{
	CAMERA_GOPRO3P,
	"GoPro Hero3 (Photo/Movie Mode)",
	0, 0, 0, NULL, NULL,
	Tv,
	NULL,
	NULL,//FocusDslr,
	NULL, CtrlType14,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},
*/
{
	CAMERA_JVC_IR,
	"JVC Camcorders",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

/*{
	CAMERA_NIKON_IR,
	"Nikon with IR",
	0x0, 0x0,
	CAMATTR_IR_SHOOT,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
},
*/
{
	CAMERA_NIKON_DSLR_SD,
	"Nikon DSLR (SD Card)",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_IR_SHOOT,
	IsoSpeed_NikonPana,
	AEProgramNikon,
	TvNikon,
	AvNikonPana,
	FocusDslr,
	ZoomRollServo,
	CtrlType6,
	Options1,
	Options2,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_NIKON_DSLR_CF,
	"Nikon DSLR (CF Card)",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_IR_SHOOT,
	IsoSpeed_NikonPana,
	AEProgramNikon,
	TvNikon,
	AvNikonPana,
	FocusDslr,
	ZoomRollServo,
	CtrlType6,
	Options1,
	Options2,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_NIKON_PTP,
	"Nikon Coolpix (experimental)",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_IR_SHOOT,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType2,
	Options1,
	Options2,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	0x38 | CAM_VENDOR_OLYMPUS,
	"Olympus",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	TvChdk,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	0x40 | CAM_VENDOR_PENTAX,
	"Pentax",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_PANASONIC_GH4,
	"Panasonic GH4",
	0x0, 0x0,
	CAMATTR_USB_BURST | CAMATTR_USB_SHOOT,
	IsoSpeed_NikonPana,
	NULL,
	Tv, //TvPana,
	AvNikonPana,
	FocusDslr,
	ZoomRollServo,
	CtrlType19,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_PANASONIC_HD_IR,
	"Panasonic Video",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},
/*
{
	CAMERA_PANASONIC_HD_IR+1,
	"Panasonic Video (Record Mode)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_PANASONIC_HD_IR+2,
	"Panasonic Video (Record/Still)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
},*/

{
	CAMERA_PANASONIC_CR,
	"Panasonic Pro Video (BETA)",
	0x0, 0x0,
	0,//CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType9,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SEQUOIA,
	"Parrot Sequoia",
	0, 0, 0, NULL, NULL, NULL, NULL, NULL, ZoomRollServo, CtrlType1,
	Options1,
	Options2,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_RED_EPIC,
	"RED Epic/Scarlet/One",
	0x0, 0x0,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CtrlType18,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_RICOH_CA1,
	"Ricoh GX100/200",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_BURST,
	NULL,
	NULL,
	TvChdk,
	NULL,
	FocusDslr,
	ZoomRollServo,
	CtrlType7,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SAMSUNG_RP,
	"Samsung NX-Series",
	0, 0, CAMATTR_USB_BURST, NULL, NULL,
	Tv,
	NULL,
	FocusDslr,
	ZoomRollServo, CtrlType10,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SAMSUNG_SDZ,
	"Samsung SDZ-330 (Zoom)",
	0, 0, 0, NULL, NULL,
	Tv,
	NULL,
	NULL,
	NULL, CtrlType10,
	Options1,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SANYO_IR,
	"Sanyo Xacti (Photo Mode)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},


{
	CAMERA_SANYO_IR+1,
	"Sanyo Xacti (Video Mode)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SONY_IR,
	"Sony/Minolta/Konica",
	0x0, 0x0,
	CAMATTR_IR_SHOOT,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ZoomRollServo,
	CtrlType16,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SONY_HC_RMT814,
	"Sony Camcorder (RMT-814)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	FocusDslr,
	ZoomRollServo,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SONY_HC_RMT835,
	"Sony Camcorder (RMT-831/835)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	FocusDslr,
	ZoomRollServo,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_SONY_MT,
	"Sony Camera",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM | CAMATTR_USB_BURST,
	NULL,
	NULL,
	TvChdk,
	NULL,
	FocusDslr,
	ZoomRollServo,
	CtrlType17,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

/*
{
	0x33 | CAM_VENDOR_SONY_HC,
	"Sony Camcorder (RMT-835)",
	0x0, 0x0,
	CAMATTR_IR_SHOOT | CAMATTR_IR_ZOOM,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType5,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
},
*/

{
	CAMERA_EXTRAFLAME,
	"Extraflame",
	0x0, 0x0,
	CAMATTR_USB_SHOOT,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType20,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},

{
	CAMERA_DIAGNOSTICS1,
	"Troubleshooter1",
	0x0, 0x0,
	CAMATTR_USB_SHOOT | CAMATTR_USB_BURST,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Zoom_Canon4x,
	CtrlType11,
	NULL,
	NULL,
	ControlMode,
	NULL,
	NULL,
	NULL,
	NULL,
},


	{0, "", 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 	NULL}
};

