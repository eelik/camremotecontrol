/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _APPLICATION_H_
#define _APPLICATION_H_
// For GUI applications
#define __BORLANDC__
#include "camfsm.h"
#include "camtasks.h"

#define CMDCAM			1

#define CAMATTR_USB_SHOOT		(1 << 0)
#define CAMATTR_USB_ZOOM		(1 << 1)
#define CAMATTR_USB_BURST		(1 << 2)
#define CAMATTR_IR_SHOOT		(1 << 14)
#define CAMATTR_IR_ZOOM			(1 << 15)



#define SIZE_SBUS_BUF 25
#define MAX_WIFI_PROFILES 4

#define CAM_VENDOR_CANON_PS	(0x40 << 8)
#define CAM_VENDOR_CANON_SLR	(0x80 << 8)
#define CAM_VENDOR_CANON_ELPH	(0x01 << 8)
#define CAM_VENDOR_CANON_CC	(0x02 << 8)
#define CAM_VENDOR_CANON_IR	(0x03 << 8)
#define CAM_VENDOR_NIKON		(0x08 << 8)
#define CAM_VENDOR_NIKON_USB	(0x09 << 8)
#define CAM_VENDOR_OLYMPUS		(0x0C << 8)
#define CAM_VENDOR_OLYMPUS_USB (0x0D << 8)
#define CAM_VENDOR_SONY			(0x10 << 8)
#define CAM_VENDOR_SONY_CC		(0x11 << 8)
#define CAM_VENDOR_SONY_AC		(0x12 << 8)
#define CAM_VENDOR_PENTAX		(0x18 << 8)
#define CAM_VENDOR_PANASONIC	(0x1B << 8)
#define CAM_VENDOR_SAMSUNG		(0x1D << 8)
#define CAM_VENDOR_RICOH		(0x1E << 8)
#define CAM_VENDOR_SANYO		(0x1F << 8)
#define CAM_VENDOR_JVC			(0x20 << 8)
#define CAM_VENDOR_GOPRO		(0x22 << 8)
#define CAM_VENDOR_BLACKMAGIC	(0x24 << 8)
#define CAM_VENDOR_RED			(0x26 << 8)
#define CAM_VENDOR_PARROT		(0x27 << 8)
#define CAM_VENDOR_FLIR			(0x28 << 8)

#define CAMERA_CANON_A310	(0x13 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_G7		(0x03 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_G9		(0x04 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_G10	(0x05 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_A620	(0x17 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_A640	(0x18 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_S3S5	(0x22 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_S70	(0x24 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_S95	(0x25 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_SX100	(0x28 | CAM_VENDOR_CANON_PS)
#define CAMERA_CANON_EOSPTP (0x2A | CAM_VENDOR_CANON_SLR)
#define CAMERA_CANON_EOSMS (0x2B | CAM_VENDOR_CANON_SLR)
#define CAMERA_CANON_EOSPTP2 (0x2C | CAM_VENDOR_CANON_SLR)
#define CAMERA_CANON_IR		(0x2D | CAM_VENDOR_CANON_IR)
#define CAMERA_CANON_CC		(0x2E | CAM_VENDOR_CANON_CC)

#define CAMERA_SONY_HC_RMT814		(0x30 | CAM_VENDOR_SONY_CC)
#define CAMERA_SONY_HC_RMT835		(0x31 | CAM_VENDOR_SONY_CC)
#define CAMERA_SONY_IR				(0x32 | CAM_VENDOR_SONY)
#define CAMERA_SONY_MT				(0x33 | CAM_VENDOR_SONY)
#define CAMERA_NIKON_IR				(0x34 | CAM_VENDOR_NIKON)
#define CAMERA_NIKON_DSLR_SD		(0x34 | CAM_VENDOR_NIKON_USB)
#define CAMERA_NIKON_DSLR_CF		(0x36 | CAM_VENDOR_NIKON_USB)
#define CAMERA_NIKON_PTP			(0x35 | CAM_VENDOR_NIKON_USB)
#define CAMERA_OLYMPUS_IR			(0x38 | CAM_VENDOR_OLYMPUS)
#define CAMERA_PENTAX_IR			(0x40 | CAM_VENDOR_PENTAX)
#define CAMERA_PANASONIC_HD_IR	(0x42 | CAM_VENDOR_PANASONIC)
#define CAMERA_PANASONIC_CR		(0x46 | CAM_VENDOR_PANASONIC)
#define CAMERA_PANASONIC_GH4		(0x47 | CAM_VENDOR_PANASONIC)
#define CAMERA_RICOH_CA1			(0x48 | CAM_VENDOR_RICOH)
#define CAMERA_DIAGNOSTICS1		(0x4A)
#define CAMERA_EXTRAFLAME			(0x4B)
#define CAMERA_SANYO_IR				(0x4C | CAM_VENDOR_SANYO)
#define CAMERA_JVC_IR 				(0x4D | CAM_VENDOR_JVC)
#define CAMERA_GOPRO2				(0x50 | CAM_VENDOR_GOPRO)
#define CAMERA_GOPRO3V				(0x51 | CAM_VENDOR_GOPRO)
#define CAMERA_GOPRO3P				(0x56 | CAM_VENDOR_GOPRO)
#define CAMERA_GOPRO_1BUT			(0x53 | CAM_VENDOR_GOPRO)
#define CAMERA_BLACKMAGIC			(0x52 | CAM_VENDOR_GOPRO)
#define CAMERA_SAMSUNG_RP			(0x58 | CAM_VENDOR_SAMSUNG)
#define CAMERA_SAMSUNG_SDZ			(0x55 | CAM_VENDOR_SAMSUNG)
#define CAMERA_RED_EPIC				(0x57 | CAM_VENDOR_RED)
#define CAMERA_SEQUOIA				(0x59 | CAM_VENDOR_PARROT)
#define CAMERA_FLIR_VUE				(0x5B | CAM_VENDOR_FLIR)

#define CTRLMODE_EXTCTRL_3CH_PWM		1
#define CTRLMODE_EXTCTRL_RC3		2
#define CTRLMODE_EXTCTRL_RC1		3
#define CTRLMODE_EXTCTRL_RISE	4
#define CTRLMODE_EXTCTRL_FALL	5
#define CTRLMODE_EXTCTRL_DCAM	8
#define CTRLMODE_EXTCTRL_RC4		9
#define CTRLMODE_EXTCTRL_RC4B	10
#define CTRLMODE_EXTCTRL_RC8		11
#define CTRLMODE_EXTCTRL_RC8_TILT	12
#define CTRLMODE_EXTCTRL_LS		14
#define CTRLMODE_EXTCTRL_SW		15

#define CTRLMODE_TIMER				0x10
#define CTRLMODE_HOVER				0x12
#define CTRLMODE_HOVER_Z			0x13
#define CTRLMODE_PANORAMA			0x14
#define CTRLMODE_HOVER_MANUAL_RC8		0x15
#define CTRLMODE_EXTCTRL_RC8_RC 0x16

#define CTRLMODE_TIMER_RC			0x18
#define CTRLMODE_HOVER_RC8		0x19
#define CTRLMODE_HOVER_Z_RC		0x1A
#define CTRLMODE_PANORAMA_RC		0x1B
#define CTRLMODE_HOVER_RC2		0x1C
#define CTRLMODE_HOVER_S			0x1D
#define CTRLMODE_SWITCHES			0x1F

#define CTRLMODE_CUSTOM1			0x20
#define CTRLMODE_CUSTOM2			0x21
#define CTRLMODE_SERVOSPLIT		0x22
#define CTRLMODE_CUSTOM3			0x23
#define CTRLMODE_CUSTOM4			0x24
#define CTRLMODE_CUSTOM5			0x25
#define CTRLMODE_CUSTOM6_COPTER	0x26
#define CTRLMODE_CUSTOM7_EGERT	0x27
#define CTRLMODE_CUSTOM8_PANACR 0x28
#define CTRLMODE_CUSTOM9_PWM_INPUT 0x29
#define CTRLMODE_CUSTOM10_INTEGRATION 0x2A
#define CTRLMODE_CUSTOM11_BRACKET 0x2B
#define CTRLMODE_CUSTOM12_SEQUENTIAL 0x2C
#define CTRLMODE_MIKROKOPTER		0x2D
#define CTRLMODE_CUSTOM14_JAKUB	0x2E
#define CTRLMODE_CUSTOM15_TADDEI	0x2F
#define CTRLMODE_SYNCMASTER		0x30
#define CTRLMODE_SYNCSLAVE		0x31
#define CTRLMODE_UART1				0x32
#define CTRLMODE_UART2				0x33

#define CTRLMODE_INTWIFI_TCP		0x34
#define CTRLMODE_INTWIFI_UDP		0x35
#define CTRLMODE_INTWIFI_DV		0x36
#define CTRLMODE_EXTWIFI1			0x38

#define CTRLMODE_WII1				0x3A
#define CTRLMODE_CUSTOM23_PIR		0x3B

#define CTRLMODE_PIR_LS				0x3C
#define CTRLMODE_PIRW_LS			0x3D

#define CTRLMODE_TEST_RC_PWM		0x40

#define CTRLMODE_EXTCTRL_3CH_PWM1	0x41
#define CTRLMODE_EXTCTRL_3CH_PWM2	0x42
#define CTRLMODE_EXTCTRL_2CH_PWM3	0x43
#define CTRLMODE_EXTCTRL_3CH_PWM3	0x44
#define CTRLMODE_EXTCTRL_4CH_PWM1	0x45
#define CTRLMODE_EXTCTRL_4CH_PWM2	0x46
#define CTRLMODE_EXTCTRL_4CH_PWM3	0x47
#define CTRLMODE_EXTCTRL_4CH_PWM4	0x48
#define CTRLMODE_EXTCTRL_4CH_PWM5	0x49

#define CTRLMODE_EXTCTRL_SW2			0x4A

#define CTRLMODE_CAMCTRL1_RC433		0x4C
#define CTRLMODE_CAMCTRL_REC_RC433	0x4D

#define CTRLMODE_CUSTOM16_LANC				0x50
#define CTRLMODE_CUSTOM17_PWM_INPUT 		0x51
#define CTRLMODE_EXTCTRL_RC8_CUSTOM18 	0x52
#define CTRLMODE_POT_CUSTOM19				0x53
#define CTRLMODE_CUSTOM20_FIXSERVO	 		0x54
#define CTRLMODE_EXTCTRL_RC3_CUSTOM21		0x55
#define CTRLMODE_CUSTOM22_RC4_CH1_CH2		0x56
#define CTRLMODE_CUSTOM23_PELLET			0x57

#define CTRLMODE_ARDUCOPTER				0x5B
#define CTRLMODE_EXTCTRL_3CH_PWM4		0x5C
#define CTRLMODE_EXTCTRL_3CH_PWM5		0x5D
#define CTRLMODE_EXTCTRL_3CH_PWM5		0x5D

#define CTRLMODE_CUSTOM24_8CAM			0x5E

#define CTRLMODE_SBUS1						0x60
#define CTRLMODE_SBUS2						0x61

#define CTRLMODE_CUSTOM25_BSKINETICS	0x63
#define CTRLMODE_CUSTOM26_BSKINETICS	0x64

#define CTRLMODE_CPPM1						0x65
#define CTRLMODE_CPPM2						0x66

#define CTRLMODE_PARACHUTE1				0x68

#define CTRLMODE_PIXHAWK					0x6A
#define CTRLMODE_SBUS_TESTER				0x6C

#define CTRLMODE_TRAILMASTER				0x6E
#define CTRLMODE_SEQUOIA					0x6F

#define EXTCTRL_RC12_CUSTOM28				0x71
#define EXTCTRL_RC4_CUSTOM29				0x72
#define EXTCTRL_RC12_CUSTOM30				0x73

#define CTRLMODE_TMP1				0x7C
#define CTRLMODE_TMP2				0x7D
#define CTRLMODE_TMP3				0x7E

#define CTRLMODE_RESERVED			0x7F

#define ZOOM_DEFAULT					0x8000
#define ZOOM_SERVO_CH3				0x8003
#define ZOOM_SERVO_3POS_CH3		0x8033
#define ZOOM_SERVO_HILOW_CH3		0x8043
#define ZOOM_SERVO_HILOW_CH2		0x8053
#define ZOOM_TO_MF					0x8060
#define ZOOM_RELAY_HILOW_CH3		0x8063

#define CTRLTYPE_USB					1
#define CTRLTYPE_IR					2
#define CTRLTYPE_SERVO_CH2			3
#define CTRLTYPE_SERVO_CH3			9
#define CTRLTYPE_CHDK				4
#define CTRLTYPE_CHDK2				10
#define CTRLTYPE_CA1					5
#define CTRLTYPE_E3					6
#define CTRLTYPE_E3A					8
#define CTRLTYPE_E3B					12
#define CTRLTYPE_CR					7
#define CTRLTYPE_CA1V				11
#define CTRLTYPE_GOPROA				13
#define CTRLTYPE_USBMUX1			14
#define CTRLTYPE_LANC1				15
#define CTRLTYPE_LANC0				16
#define CTRLTYPE_CHDK_PTP			17
#define CTRLTYPE_MULTI			18
#define CTRLTYPE_MULTI_MUX	19
#define CTRLTYPE_MULTI_ONOFF	20
#define CTRLTYPE_MULTI_AV		21
#define CTRLTYPE_MULTI_X		22
#define CTRLTYPE_MULTI_XT		23
#define CTRLTYPE_MULTI_CC		24
#define CTRLTYPE_MULTI_PTP		25
#define CTRLTYPE_SWITCH_CH3		26
#define CTRLTYPE_RED_TRIG			30
#define CTRLTYPE_RED_UART			31
#define CTRLTYPE_RP_PANA_ZOOM1	35 // With 2.2kohm + 4.7uF x3 pins
#define CTRLTYPE_RP_PANA_ZOOM2	36 // With 2 kohm + 4.7uF x3 pins
#define CTRLTYPE_PANA_PTP			37
//#define CTRLTYPE_LANC_AV			40 // 100kohm + ID pin for shorting // Didn't work on Bernt
#define CTRLTYPE_LANC_AV			0x68 // 100kohm + ID pin for shorting
#define CTRLTYPE_LANC_AVM			41 // 2.2kohm + 4.7uF on ID pin
#define CTRLTYPE_RTC1A				46 // RTC connected to USB/IR
#define CTRLTYPE_RTC1B				47 // RTC connected to LANC/RP
#define CTRLTYPE_LANC_IR			50
#define CTRLTYPE_WIFIM1				55

#define CAMMODEL_NOT_SELECTED			0xFFFF
#define CAMMODEL_AUTODETECT_PTP		0xFFFE
#define CAMMODEL_AUTODETECT_ANY_SERVO	0xFFFD
#define CAMMODEL_AUTODETECT_ANY_CHDK	0xFFFC
#define CAMMODEL_AUTODETECT_ANY_E3		0xFFFB

#define PAUSE_RC_CTRL		0x80
#define PAUSE_START_STOP	(PAUSE_RC_CTRL | 1)
#define PAUSE_RESTART		(PAUSE_RC_CTRL | 2)




typedef struct _APP_STRUCT_V1
{
    UINT16 AppStructId;
    UINT16 Reserved;
    UINT32 State0;
    UINT32 State1;
    UINT32 State2;
    UINT32 State3;
    UINT16 RevisionPcb;
    UINT16 StatePulse;
    UINT16 FsmCamState;
    UINT16 FsmServoState;
    UINT16 FsmWifiState;
    UINT16 FsmDvState;
    UINT16 FsmLancState;
    UINT16 FsmMultiTermState;
    UINT32 Ch0Elapsed;
    UINT32 Ch1Elapsed;
    UINT32 Ch2Elapsed;
    UINT32 Ch3Elapsed;
    UINT8 SbusData[SIZE_SBUS_BUF];
    UINT32 Ch0TimeOn;
    UINT32 Ch1TimeOn;
    UINT32 Ch2TimeOn;
    UINT32 Ch3TimeOn;
    UINT32 Ch0TimeOff;
    UINT32 Ch1TimeOff;
    UINT32 Ch2TimeOff;
    UINT32 Ch3TimeOff;
    UINT32 Ch0Average;
    UINT16 Ch0AvgCounter;
    UINT16 Ch1AvgCounter;
    UINT16 Ch2AvgCounter;
    UINT32 ButtonTimeOn;
    UINT32 ButtonTimeOff;
    UINT16 CamMode;
    UINT16 CamCmd;
    UINT16 CamTask;
    UINT16 CamCmdDone;
    UINT16 ExtCmd;
    UINT16 ExtCmdDone;
    UINT16 ExtKey;
    UINT16 Cmd;
    UINT16 RxId;
    UINT16 LancCmd;
    UINT16 LancCmdCounter;
    UINT32 TimerLanc;
    UINT32 TimerLancSpace;
    UINT16 Irq;
    UINT16 Active;
    UINT16 MultiCmdCnt;
    UINT32 MultiCmd;
    UINT32 UsbTimeout;
    PTPPACKET PtpOutPkt1;
    PTPPACKET PtpOutPkt2;
    UINT8 PtpInCmd[LEN_PTPIN];
//	UINT32 TimeDatagram;
//	UINT32 TimeDatagramPrev;
    UINT16 LastShutterSpeed;
    UINT16 StateUsart;
    UINT16 LightSensorValue;
    UINT8 GoProSdaCounter;
    UINT8 *UsbBuf;
    UINT8 Zoom, Iris, Focus;
    UINT16 Tv, Av, Iso, ImageSize, Wb, FocusPos, MemCard, DialPos, CamBatteryLevel, MemCardSizeLeft;
    UINT16 TvComp, AfPoint, ZoomPosX, ZoomPosY;
    UINT32 ZoomTimer;
    UINT32 HalfPressTimer;
    UINT32 TimerRecording;
    UINT16 Id;
    UINT16 LowBatCnt;
    UINT16 PauseDelay;
    UINT16 PauseShotsTime;
    UINT16 CountShoot;
    UINT16 CountShootMax;
    UINT16 RxPtr, RxScanPtr, RxCount;
    UINT16 VideoStandard;
    UINT32 JpegSize;
    UINT32 IpAddrAp;
    UINT32 IpAddrModule;
    UINT16 WifiParseState;
    UINT16 WifiRespState;
    UINT8 WifiMsgParam;
    UINT8  CaptureMode;
    UINT8  FramesInWifi;
    UINT8 *WifiPacketPtr;
    UINT16 WifiIdx;
    UINT16 WifiIdxScan;
    UINT16 WifiPacketLen;
    UINT16 WifiQuoteSum;
    UINT16 TouchId;
    UINT16 TouchX;
    UINT16 TouchY;
    UINT16 TouchX0;
    UINT16 TouchY0;
    INT16 TouchdX;
    INT16 TouchdY;
    UINT16 ServoPanPos, ServoTiltPos, ServoRollPos, ServoZoomPos, ServoPanRatio, ServoTiltRatio;
    UINT16 ServoPanStep, ServoTiltStep, ServoRollStep, ServoZoomStep;
    UINT16 ServoPanDuration, ServoTiltDuration, ServoZoomDuration;
    UINT32 Servo2Elapsed;
    UINT16 Servo0Target, Servo0Pos;
    UINT16 Servo1Target, Servo1Pos;
    UINT16 Servo2Target, Servo2Pos;
    UINT16 Servo3Target, Servo3Pos;
//	UINT16 DeviceVID, DevicePID;
    UINT16 PtpRespCode;
    UINT8 EpBulkIn, EpBulkOut, EpInterrupt;
//	UINT16 Options;
    UINT8 AppCmdCode;
    UINT8 AppCmdData;
    UINT8 AppCmdAddr;
    UINT8 AppCmdId;
    UINT16 PwmCount;
    UINT16 PelletTemp;
    UINT16 PelletTempOld;
    UINT8 Sbus1;
    UINT8 Sbus2;
    UINT8 Sbus3;
    UINT8 Sbus4;
    UINT8 Sbus5;
    UINT8 Sbus6; // Not used
    UINT16 SbusElapsed;
    UINT16 WifiCmdId;
    UINT16 WifiQuoteCapture;
    UINT16 WifiQuoteDataIdx;
    UINT32 ButtonGlitchTime;
    WIFIAP *Ap[MAX_WIFI_PROFILES];
    CAMDESCRIPTOR* CameraDesc;
    CAMPROFILEV00 *ProfileInfo;
    CAMPROFILEV00 Profile/*[MAX_CAM_PROFILES]*/; // Copy of active profile
} APP;

#endif
