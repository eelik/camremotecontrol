/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PROFILEITEM_H
#define PROFILEITEM_H

#include <QObject>
#include <QStandardItemModel>

#include "firmware/camfsm.h"

class ProfileData;

/**
 * @brief One ProfileItem handles datamodel for one UI combobox.
 *
 * For example the list of camera models is one ProfileItem.
 * If the UI is changed, corresponding items are removed/added in data.cpp initialization.
 */
class ProfileItem : public QStandardItemModel
{
    Q_OBJECT
    Q_ENUMS(Role)
    friend class ProfileData;
    Q_PROPERTY(int currentIndex MEMBER m_selectedIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(QString currentText MEMBER m_selectedText NOTIFY currentTextChanged)
    Q_PROPERTY(QString name MEMBER name)
protected:
    int m_selectedIndex;
    QString m_selectedText;
    QString name;
    ProfileData* parentData;
    int valueSize;
    int offset;


public:  

    /**
     * @brief The Role enum Data roles (for internal use only)
     */
    enum Role {
        TextRole = Qt::UserRole+1,
        ValueRole,
        VisibleRole
    };

    explicit ProfileItem(QString name, UINT8 *valueLocation, int size, ProfileData *parent);



    int findIndexByValue(UINT16 value) const;
    UINT16 findValueByIndex(int index) const;
    void notify() const;
    virtual void setModelFromCamitem(const CAMITEM *camitem);
    void setModelFromLists(const QList<UINT16> &values, const QList<QString> &names);
    void save(int index, CAMPROFILEV00* profile);
    void save(int index, CAMPROFILEV01* profile);
    void load(const CAMPROFILEV00* profile);
    void load(const CAMPROFILEV01 *profile);

signals:
    void currentIndexChanged(int currentIndex) const;
    void currentTextChanged(QString currentText) const;
};

#endif // PROFILEITEM_H
