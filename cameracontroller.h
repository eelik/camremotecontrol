/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <QObject>

class Connection;

class CameraController : public QObject
{
    Q_OBJECT

    Connection* m_connection;

public:
    explicit CameraController(Connection* conn, QObject *parent = 0);

    // These must be the same as in connection.h OutgoingMessageType
    enum Command: int {
        ServoUp = 10,
        ServoDown,
        ServoLeft,
        ServoRight,
        CameraShoot,
        CameraRec,
        CameraStopRec,
        CameraZoomIn,
        CameraZoomOut,
        CameraTvPlus,
        CameraTvMinus,
        CameraAvPlus,
        CameraAvMinus,
        CameraIsoPlus,
        CameraIsoMinus,
        CameraMfPlus,
        CameraMfMinus
    };
    Q_ENUM(Command)
signals:

public slots:
    void sendCommand(CameraController::Command) const;
    void sendServoCommand(CameraController::Command command, float x = 0, float y = 0) const;
};

#endif // CAMERACONTROLLER_H
