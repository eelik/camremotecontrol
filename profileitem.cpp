/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <QDebug>
#include "profileitem.h"
#include "profiledata.h"
#include "tools.h"
/**
 * @brief ProfileItem::ProfileItem List of text/value pairs, represents e.g. a combobox list in the UI.
 * @param name Name of the item, taken from CAMDESCRIPTOR and/or CAMPROFILEV00. Used
 * in QML and to identify the item.
 * @param valueLocation Pointer to the value in the current profile.
 * @param size size of the value in a CAMPROFILEV00, 1 or two bytes (UINT16 or UINT8).
 * @param parent Data object, used as QObject parent and to init certain things.
 */
ProfileItem::ProfileItem(QString a_name, UINT8* a_valueLocation, int size, ProfileData *a_parent) :
    QStandardItemModel(a_parent),
    m_selectedIndex{-1},
    m_selectedText{""},
    name{a_name},
    valueSize{size}
{
    this->offset = a_valueLocation - reinterpret_cast<UINT8*>(a_parent->currentProfile);
    //qDebug() << name << "Address " << (UINT8*)valueLocation << offset << "bytes from the beginning of a profile" << (UINT8*)parent->currentProfile << ", size " << size;

    // Set the roles for text strings and corresponding values
    QHash<int, QByteArray> roleNames{};
    roleNames[ProfileItem::TextRole] = "modelData"; // UI text (QML uses this automatically)
    roleNames[ProfileItem::ValueRole] = "value";
    roleNames[ProfileItem::VisibleRole] = "show";
    setSortRole(ProfileItem::TextRole);
    setItemRoleNames(roleNames);

    a_parent->profileItems[a_name] = this;
    // The model name visible to QML is this model name in lowercase + "Model", e.g. "cameranameModel"
    a_parent->context->setContextProperty(a_name.toLower()+QString("Model"), this);
}

/**
 * @brief ProfileItem::setModelFromCamitem
 * @param camitem
 *
 * Uses the camitem (see camprops.c) to set texts and values.
 */
void ProfileItem::setModelFromCamitem(const CAMITEM* a_camitem)
{
    //qDebug() << this->name << "camitem" << (int*)camitem;
    this->clear();
    if (a_camitem == NULL) return;
    // Create Qt model item for each camitem
    // Iterates through CAMITEM[]; the last Str is always NULL in each CAMITEM (see camprops.c)
    while (a_camitem->Str != NULL) {
        QStandardItem* it{new QStandardItem()};
        it->setData(QString(a_camitem->Str), ProfileItem::TextRole);
        it->setData(a_camitem->Code, ProfileItem::ValueRole);
        it->setData(true, ProfileItem::VisibleRole);
        this->appendRow(it);
        a_camitem++;
    }
    this->m_selectedIndex = 0;
    this->m_selectedText = data(this->index(0,0), ProfileItem::TextRole).toString();
}

/*
 * Creates item using values and names in lists.
 * For items which don't have CAMITEM struct.
 */
void ProfileItem::setModelFromLists(const QList<UINT16> &a_values, const QList<QString> &a_names)
{
    //qDebug() << this->name;
    this->clear();
    for (int i = 0; i < a_values.size(); i++) {
        QStandardItem* it{new QStandardItem()};
        it->setData(a_names[i], ProfileItem::TextRole);
        it->setData(a_values[i], ProfileItem::ValueRole);
        it->setData(true, ProfileItem::VisibleRole);
        this->appendRow(it);
        //qDebug() << names[i];
    }
    m_selectedIndex = 0;
    m_selectedText = a_names[0];
    //setCurrentIndex(0);
}


/*
 * Saves a value to the given profile. Value is found from the item's model data based on index.
 */
void ProfileItem::save(int a_index, CAMPROFILEV00* a_profile)
{
    //qDebug() <<  name << "index" << index << "profile" << (int*)profile;
    this->m_selectedIndex = a_index;
    this->m_selectedText = data(this->index(a_index,0), ProfileItem::TextRole).toString();
    // Find the value from the data model
    UINT16 val = findValueByIndex(a_index);
    //qDebug() << "Value for the index: " << val;
    // Find the adress for the value in the profile, either 8 or 16 bits,
    // and copy the value to the address
    if (this->valueSize == 1) {
        UINT8 val8 = static_cast<UINT8>(val);
        UINT8* ptr = (reinterpret_cast<UINT8*>(a_profile)) + offset;
        *ptr = val8;
        //qDebug() << this->name << "saved one byte," << *ptr << "to" << ptr << "Index" << index;
    } else {
        UINT16* ptr = reinterpret_cast<UINT16*>((reinterpret_cast<UINT8*>(a_profile)) + offset);
        *ptr = val;
        //qDebug() << this->name << "saved two bytes," << *ptr << "to" << ptr << "Index" << index;
    }
    emit currentTextChanged(this->m_selectedText);
}

void ProfileItem::save(int a_index, CAMPROFILEV01 *a_profile)
{
    //qDebug();
    save(a_index, reinterpret_cast<CAMPROFILEV00*>(a_profile));
    //qDebug();
}

/*
 * Finds the index of the value which is in the profile and notifies it to listeners (UI)
 */
void ProfileItem::load(const CAMPROFILEV00* a_profile)
{
    //qDebug() << "load" << name << " from profile" << (int*)profile;

    // Find the value using the relative offset from the beginning of the profile.
    UINT16 value;
    const UINT8* ptr = (reinterpret_cast<const UINT8*>(a_profile)) + this->offset;
    if (this->valueSize == 1) {
            value = UINT16{*ptr};
    } else {
        const UINT16* ptr16 = reinterpret_cast<const UINT16*>(ptr);
        value = *ptr16;
    }
    // Find the model/UI index of the value
    int index = this->findIndexByValue(value);
    this->m_selectedIndex = index;
    this->m_selectedText = data(this->index(index,0), ProfileItem::TextRole).toString();
    //qDebug() << this->name << "Profile:" << (int*)profile << "value:" << value << "index:" << index << "Now notify.";
    notify();
}

void ProfileItem::load(const CAMPROFILEV01 *a_profile)
{
    load(reinterpret_cast<const CAMPROFILEV00*>(a_profile));
}

/*
 * Sends a signal which tells listeners (UI) that the selected item index has changed.
 */
void ProfileItem::notify() const
{
    //qDebug() << "going to notify, index" << this->m_selectedIndex << "text" << this->m_selectedText;
    emit currentIndexChanged(this->m_selectedIndex);
    emit currentTextChanged(this->m_selectedText);
}

/*
 * Finds the index of the given value in the model
 */
int ProfileItem::findIndexByValue(UINT16 a_value) const
{
    int idx = 0;
    UINT16 val;
    //qDebug() << this->name <<  "value" << value;
    for (int i = 0; i < rowCount(); i++) {
        val = UINT16(data(this->index(i,0), ProfileItem::ValueRole).toUInt());
        //qDebug() << val;
        if (a_value == val) {
            idx = i;
            //qDebug() << this->name << "found index" << idx;
            break;
        }
    }
    return idx;
}

/*
 * Finds from the model the value to which the given index points.
 */
UINT16 ProfileItem::findValueByIndex(int a_index) const
{
    //qDebug() << name << "index" << index;
    auto val_uint = this->data(this->index(a_index, 0), ProfileItem::ValueRole).toUInt();
    UINT16 val = static_cast<UINT16>(val_uint);
    return val;
}
