/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <QDebug>
#include "firmware/application.h"
#include "monitoring.h"
#include "connection.h"


Monitoring::Monitoring(Connection* a_connection, QObject *parent) :
    QObject(parent),
    m_connection{a_connection}
{
    connect(&m_timer, &QTimer::timeout, this, &Monitoring::slotTimeout);
    connect(a_connection, &Connection::monitoringDataAcquired, this, &Monitoring::slotMonitoringDataUpdated);
}

void Monitoring::slotTimeout() {
    m_connection->getMonitoringDataFromCamremote();
}

void Monitoring::startUpdatingValues(int a_interval) {
    slotTimeout(); // the first update right away
    m_timer.start(a_interval);
}

void Monitoring::stopUpdatingValues() {
    m_timer.stop();
}

void Monitoring::slotMonitoringDataUpdated(QByteArray a_data) {
    //qDebug() << "length" << data->size() <<"data"<< *data;

    QList<double> sbusdata;
    QList<double> chdata;
    double resistordata;
    auto appstruct = reinterpret_cast<APP*>(a_data.data());
    double val;

    uchar* bytePointer = reinterpret_cast<uchar*>(&(appstruct->Ch0Elapsed));
    for (int i: {0,1,2,3}) {
        // Ch0...3Elapsed data is 4 bytes in the APP struct but only the last byte is used (hence +3).
        // Values may be 0...254.
        // Dividing by 94 gives quite close approximation of the real time in milliseconds, about 0...2.7ms.
        val = static_cast<double>((bytePointer[i*4+3])) / 94;
        //qDebug() << "ch double value:" << val;
        chdata.append(val);
    }
    // The next 16 bytes are sbus values, one byte each.
    bytePointer = reinterpret_cast<uchar*>(&(appstruct->SbusData));
    for (int i = 0; i < 16; i++) {
        val = static_cast<double>((bytePointer[i])) / 94;
        //qDebug() << "sbus double value which goes to UI:" << val;
        sbusdata.append(val);
    }
    //TODO: resistor data
    resistordata = 0;
    emit this->valuesUpdated(chdata, resistordata, sbusdata);
}
