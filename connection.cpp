/*
 * Copyright (c) 2016 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "connection.h"
#include "tools.h"

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#endif

#include <QTcpSocket>
#include <QNetworkConfigurationManager>
#include <QNetworkAccessManager>
#include <QNetworkInterface>

#include <QNetworkSession>
#include <QSettings>
#include <QStandardItemModel>
#include <QMap>
#include <QDateTime>



Connection::Connection(QObject *parent) :
    QObject(parent),
    m_connected{false}
{
    qDebug() << "";
    m_networkUpdateTimer.setInterval(6000);

    Tools::setSettingInt("netwcapabilities", static_cast<int>(m_netMgr.capabilities()));
//    QList<QNetworkConfiguration> configlist = m_netMgr->allConfigurations();
//    for (auto config: configlist) {
//        config.
//    }
    // TCP socket works asynchronously and sends signals when something happens.
    connect(&m_sock, &QTcpSocket::connected, this, &Connection::slotConnectedToCamremote );
    connect(&m_sock, static_cast<void(QTcpSocket::*)(QAbstractSocket::SocketError)>(&QTcpSocket::error), this, &Connection::slotSocketError);
    connect(&m_sock, &QTcpSocket::stateChanged, this, &Connection::slotSocketStateChanged );
    connect(&m_sock, &QTcpSocket::readyRead, this, &Connection::slotSocketReadyRead );

    // See connectToCamremote()
    m_camremoteIp.setAddress(Tools::getIp());
    populateMessageMap();
    qDebug() << "";
}


/*
 * Establishes a TCP socket connection to CAMremote server. Returns immediately;
 * signal connected() is emitted when the connection has been established.
 * If the connection wasn't ready already, signal tryingToConnect() is
 * emitted so that UI can inform the user.
 */
bool Connection::connectToCamremote() {
    if (m_sock.state() == QAbstractSocket::UnconnectedState) { // ->isOpen() == false) {
        // Get ip and port from settings every time a connection is established
        m_camremoteIp.setAddress(Tools::getIp());
        m_camremotePort = static_cast<quint16>(Tools::getPort());
        m_sock.connectToHost(m_camremoteIp, m_camremotePort);
        qDebug() << "socket wasn't open";
        emit tryingToConnect();
        return false;
    } else if (m_sock.state() == QAbstractSocket::ConnectedState) {
        qDebug() << "socket was already open";
        //sendPendingMessageToCamremote();
        emit connected();
        return true;
    } else {
        qDebug() << "Socket was neither connected nor unconnected, no signal sent yet";
        return false;
    }
}

/*
 * Meant to be called after connectToCamremote() but before the connection is ready.
 */
void Connection::cancelConnecting() {
    qDebug() << "connecting was cancelled for some reason";
    m_sock.abort();
    m_outgoingMessageData.clear();
    emit connectionCancelled();
}

bool Connection::isUnconnected() const {
    return m_sock.state() == QAbstractSocket::UnconnectedState;
}
bool Connection::isConnected() const {
    return m_sock.state() == QAbstractSocket::ConnectedState;
}


/*
 * Sends message to CAMremote which tells we want profiles data from there.
 * Returns immediately; signal is emitted when the data has been received.
 * The connection to CAMremote must be established before using this method.
 */
void Connection::getProfilesFromCamremote() {
    qDebug();
    m_outgoingMessageData = createMessage(Connection::GetProfiles);
    sendPendingMessageToCamremote();
}


/*
 * Sends profile data to CAMremote. CAMremote doesn't send an answer.
 * The connection to CAMremote must be established before using this method.
 */
void Connection::setProfilesToCamremote(const QByteArray& a_profiledata) {
    qDebug();
    m_outgoingMessageData = createMessage(Connection::SetProfiles, a_profiledata);
    sendPendingMessageToCamremote();
}

void Connection::getMonitoringDataFromCamremote() {
    //qDebug();
    m_outgoingMessageData = createMessage(Connection::GetRadio);
    sendPendingMessageToCamremote();
}



/*
 * Receives error message from the socket object. Doesn't try to re-establish
 * the connection, just forwards the message string as a signal.
 */
void Connection::slotSocketError(QAbstractSocket::SocketError) {
    QString err = m_sock.errorString();
    m_sock.close();
    m_outgoingMessageData.clear();
    emit connectionError(err);
}

/*
 * Connected to the socket's signal which tells that the connection
 * to the CAMremote server is established. Forwards the signal.
 */
void Connection::slotConnectedToCamremote() {
    emit connected();
}

/*
 *
 */
void Connection::populateMessageMap() const {

//    m_messageMap.insert(OutgoingMessageType::GetProfiles, OutgoingMessageType::GetProfiles);
//    m_messageMap.insert(OutgoingMessageType::SetProfiles, OutgoingMessageType::SetProfiles);
//    m_messageMap.insert(OutgoingMessageType::StartImage, "");
//    m_messageMap.insert(OutgoingMessageType::StopImage, "");
//    m_messageMap.insert(OutgoingMessageType::GetRadio, "GM");
//    m_messageMap.insert(OutgoingMessageType::SetTime, OutgoingMessageType::SetTime);
//    m_messageMap.insert(OutgoingMessageType::CameraAvMinus, "Ca");
//    m_messageMap.insert(OutgoingMessageType::CameraAvPlus, "CA");
//    m_messageMap.insert(OutgoingMessageType::CameraIsoMinus, "Ci");
//    m_messageMap.insert(OutgoingMessageType::CameraIsoPlus, "CI");
//    m_messageMap.insert(OutgoingMessageType::CameraMfMinus, "Cm");
//    m_messageMap.insert(OutgoingMessageType::CameraMfPlus, "CM");
//    m_messageMap.insert(OutgoingMessageType::CameraRec, "CR");
//    m_messageMap.insert(OutgoingMessageType::CameraShoot, "CS");
//    m_messageMap.insert(OutgoingMessageType::CameraStopRec, "Cr");
//    m_messageMap.insert(OutgoingMessageType::CameraTvMinus, "Ct");
//    m_messageMap.insert(OutgoingMessageType::CameraTvPlus, "CT");
//    m_messageMap.insert(OutgoingMessageType::CameraZoomIn, "CZ");
//    m_messageMap.insert(OutgoingMessageType::CameraZoomOut, "Cz");
//    m_messageMap.insert(OutgoingMessageType::ServoDown, "Sd");
//    m_messageMap.insert(OutgoingMessageType::ServoUp, "Su");
//    m_messageMap.insert(OutgoingMessageType::ServoRight, "Sr");
//    m_messageMap.insert(OutgoingMessageType::ServoLeft, "Sl");
}

void Connection::sendMessage(MessageType a_type, QByteArray a_content) {
    m_outgoingMessageData = createMessage(a_type, a_content);
    sendPendingMessageToCamremote();
}

/*
 * Creates the message which will be later sent to CAMremote.
 * The message protocol between CAMremote server and client is implemented here
 * and in slotSocketReadyRead().
 */
QByteArray Connection::createMessage(MessageType a_type, const QByteArray& a_content) const {
    QByteArray data{};
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << static_cast<qint16>(a_type);
    qDebug() << data;
    //int length = (qint16)(content.length());
    qDebug() << "content length" << a_content.length();
    stream << static_cast<qint16>(a_content.length());
    qDebug() << "data this far" << data;
    qDebug() << "data lenght" << data.length();
    switch (a_type) {
    case SetProfiles:
    case SetTime:
        //stream << content;
        data.append(a_content);
        break;
    default: break;
    }
    qDebug() << "final length" << data.length();
    return data;
}

void Connection::sendPendingMessageToCamremote() {
    //qDebug();
    if (!m_outgoingMessageData.isEmpty()) {
        qDebug() << "sending message: " << m_outgoingMessageData;
        qint64 written = m_sock.write(m_outgoingMessageData);
        if (written == -1) {
            qDebug() << "ERROR writing message to socket!";
            // socket sends error message
            //emit connectionError("Couldn't write message to socket!");
        } else {
            emit this->messageSent();
        }

    }
}

void Connection::slotSocketStateChanged(QAbstractSocket::SocketState a_state) {
    qDebug() << a_state;
    if (a_state == QAbstractSocket::ConnectedState) {
        m_connected = true;
        emit connectedStatusChanged(true);
    }
    else {
        m_connected = false;
        emit connectedStatusChanged(false);
    }
}

/*
 * Something has arrived to the socket. Read it, possibly save the data
 * in appropriate form and inform other objects with appropriate signals.
 * The message protocol between CAMremote server and client is implemented here
 * and in createMessage().
 */
void Connection::slotSocketReadyRead() {
    //qDebug() <<  "Ready to read from socket";
    QByteArray message = m_sock.read(1024);
    //qDebug() << "message length"<< message.size();
    //QString mtype = message.left(2);
    //TODO: conversion
    int mtype = *( reinterpret_cast<const int*>(&(message.constData()[1])));
    qDebug() << mtype;
    qDebug() << "Incoming data, length"<< message.size() << " , message type: " << mtype;

    if (mtype == MessageType::GetProfiles) {
        // Profile data
        QByteArray profiles = message.right(message.size()-4);
        qDebug() << "Received profiles from socket";
        emit profilesAcquired(profiles);
    }
    //else if (mtype == "IM") {
        // Image data
        //save a part of image to m_incomingMessageData
        // If it's the last part, send signal imageAcquired
    //}
    else if (mtype == MessageType::GetRadio) {
        // Monitoring data
        QByteArray monitoringData = message.right(message.size()-4);
        //qDebug() << "Received monitoring data from socket"<<message.size()-2<<"bytes";
        emit monitoringDataAcquired(monitoringData);
    } else if (mtype == 1000) {
        //emit okReceived();
    }
}

void Connection::setTime()
{
    QDateTime dt = QDateTime::currentDateTime();
    QTime time{dt.time()};
    QDate date{dt.date()};

    char rawdata[6];
    rawdata[0] = static_cast<char>(time.second());
    rawdata[1] = static_cast<char>(time.minute());
    rawdata[2] = static_cast<char>(time.hour());
    rawdata[3] = static_cast<char>(date.day());
    rawdata[4] = static_cast<char>(date.month());
    rawdata[5] = static_cast<char>(date.year()-2000);
    QByteArray msg(rawdata, 6);

    sendMessage(Connection::SetTime, msg);
}

// These methods are not used at the moment, they would be needed
// if there were many available networks from which to choose

void Connection::getNetworks() {
    connect(&m_netMgr, SIGNAL(updateCompleted()), this, SLOT(slotNetworksUpdated()));
#ifdef Q_OS_ANDROID
    // Android (4.1) doesn't update automatically
    connect(&m_networkUpdateTimer, &QTimer::timeout, this, &Connection::slotNetworksUpdated);
    m_networkUpdateTimer.start();
#endif
    m_netMgr.updateConfigurations();
}

void Connection::setActiveNetwork(QString a_name) {
    qDebug() << "Set network" << a_name;

#ifdef Q_OS_ANDROID
    QAndroidJniObject wifiAP = QAndroidJniObject::fromString(a_name);
    QAndroidJniObject::callStaticMethod<void>("eu/vpsystems/cameracontrol/wifi/WifiConnection",
                                              "setActiveConnection",
                                              "(Ljava/lang/String;)V",
                                              wifiAP.object<jstring>());
#endif

}

void Connection::slotNetworksUpdated() {
    //disconnect(this->m_netMgr, SIGNAL(updateCompleted()), this, SLOT(slotNetworksUpdated()));
    //qDebug() << m_netMgr->capabilities();

    //QNetworkAccessManager accMgr(nullptr);

    //QList<QNetworkConfiguration> confList = this->m_netMgr->allConfigurations();
    //QNetworkConfiguration conf;

//    foreach (conf, confList){
//        this->networksStringList << conf.name();

//        qDebug() << conf.name() <<  conf.state() << conf.type() << (conf == accMgr.activeConfiguration());
//    }
//    QString n;
//    foreach (n, this->networksStringList){
//       // qDebug() << n;
//    }
//    QString confName = accMgr.activeConfiguration().name();
//    qDebug() << "Active configuration:" << confName;
//    qDebug() << "configuration: " << accMgr.configuration().name();
//    qDebug() << accMgr.children();
//    qDebug() << accMgr.activeConfiguration().identifier();

//    auto ifaces = QNetworkInterface::allInterfaces();
//    for (auto i: ifaces) {
//        qDebug() << i.name();
//        qDebug() << i.humanReadableName();
//    }
    QString ssid = slotGetSSID();
    qDebug() << "network info updated"  << ssid;
    bool notify = (ssid != m_wifiSSID);
    m_wifiSSID = ssid;
    if (notify) {
        emit wifiSSIDChanged();
    }
    //emit networksAcquired(this->networksStringList);
}

QString Connection::slotGetSSID() const {
#ifdef Q_OS_ANDROID
    QAndroidJniObject ssid = QAndroidJniObject::callStaticObjectMethod<jstring>("eu/vpsystems/cameracontrol/androidinterface/CamControl", "getActiveSSID");
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
        qDebug() << "Exception in java code";
        env->ExceptionDescribe(); // print to out
        env->ExceptionClear();
    }
    qDebug() << ssid.toString();
    return ssid.toString();
#else
    return "";
#endif
}
