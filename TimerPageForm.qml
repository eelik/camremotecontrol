/*
 * Copyright (c) 2016-2017 VP-Systems
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import QtQuick 2.8
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

import "components"
import "dialogs"
Control {
    id: control
    property alias comboBoxStartupDelay: comboBoxStartupDelay
    property alias comboBoxRepeat: comboBoxRepeat
    property alias comboBoxDuration: comboBoxDuration
    property alias comboBoxInterval: comboBoxInterval
    property alias comboBoxBattery: comboBoxBattery
    property alias comboBoxTimeRadioWakeup: comboBoxTimeRadioWakeup
    property alias comboBoxTimerEvent: comboBoxTimerEvent

    property alias buttonSave: buttonSave
    property alias buttonLoad: buttonLoad

    property bool testing: true

    contentItem: Flickable {
        id: flickable1

        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.AutoFlickDirection
        contentHeight: contentItem.childrenRect.height + 10

        GridLayout {
            id: gridLayoutMain
            anchors.rightMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.right: parent.right
            columns: 1
            Label {
                Layout.alignment: Qt.AlignHCenter|Qt.AlignVCenter
                text: connection.wifiSSID === "" ? "<h3>Remote Camera Control</h3>" : "Wifi: " + connection.wifiSSID
                //font.pointSize: Control.pointSize + 2
                textFormat: Text.StyledText
            }

            GroupBox {
                id: groupBoxDevice
                Layout.fillWidth: true
                RowLayout {
                    id: rowLayout2
                    anchors.right: parent.right
                    ButtonWithImage {
                        id: buttonDeviceTime
                        Layout.fillHeight: true
                        source: "/icons/ic_access_time_black_24dp.png"
                        onClicked: {
                            setTimeDialog.beginSave()
                        }

                        DeviceTimeDialog {
                            id: setTimeDialog
                            parent: ApplicationWindow.overlay
                            x: Math.floor((parent.width - width) / 2)
                            y: Math.floor((parent.height - height) / 3)

                        }

                    }

                    ButtonWithImage {
                        id: buttonLoad
                        Layout.fillHeight: true
                        text: "Load"
                        source: "/icons/ic_file_download_black_24dp.png"
                    }

                    ButtonWithImage {
                        id: buttonSave
                        text: "Save"
                        Layout.fillHeight: true
                        source: "/icons/ic_file_upload_black_24dp.png"
                    }
                }
            }

            GroupBox {
                id: groupBoxProgram
                Layout.fillWidth: true
                //title:
                GridLayout {
                    id: gridLayoutProgram
                    anchors.fill: parent
                    columns: 2

                    Label {
                        visible: !testing
                        id: labelStartupDelay
                        text: "Start delay"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        visible: !testing
                        id: comboBoxStartupDelay
                        currentIndex: startupdelayModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: startupdelayModel
                    }

                    Label {
                        visible: !testing
                        id: labelRepeat
                        text: "Repeat"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        visible: !testing
                        id: comboBoxRepeat
                        currentIndex: repeatModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: repeatModel
                    }

                    Label {
                        id: labelDuration
                        text: "Duration"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxDuration
                        currentIndex: durationModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: durationModel
                    }

                    Label {
                        //visible: !testing
                        id: labelInterval
                        text: "Interval"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        //visible: !testing
                        id: comboBoxInterval
                        currentIndex: intervalModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: intervalModel
                    }

                    Label {
                        id: labelTimeLapse
                        visible: false
                        text: "Time lapse"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxTimeLapse
                        visible: false
                        currentIndex: intervalModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: timelapseModel
                    }
                }
            }
            //Flow {
            //Layout.fillWidth: true
            //spacing: 6
            GroupBox {
                id: groupBoxSets
                title: "Set start (hh:mm) - end"
                Layout.fillWidth: true
                ColumnLayout {
                    RowLayout {
                        id: set1

                        Label {
                            id: labelSet1
                            text: qsTr("#1")
                        }
                        TimeButton {
                            id: buttonTimeSet1Start
                            hhModel: timeset1starthhModel
                            mmModel: timeset1startmmModel
                        }
                        Label {
                            text: qsTr("-")
                        }
                        TimeButton {
                            id: buttonTimeSet1End
                            hhModel: timeset1endhhModel
                            mmModel: timeset1endmmModel
                        }
                    }

                    RowLayout {
                        Label {
                            id: labelSet2
                            text: qsTr("#2")
                        }
                        TimeButton {
                            id: buttonTimeSet2Start
                            hhModel: timeset2starthhModel
                            mmModel: timeset2startmmModel
                        }
                        Label {
                            text: qsTr("-")
                        }
                        TimeButton {
                            id: buttonTimeSet2End
                            hhModel: timeset2endhhModel
                            mmModel: timeset2endmmModel
                        }
                    }
                    RowLayout {
                        visible: false
                        Label {
                            id: labelSet3
                            text: qsTr("#3")
                        }
                        TimeButton {
                            id: buttonTimeSet3Start
                            hhModel: timeset3starthhModel
                            mmModel: timeset3startmmModel
                        }
                        Label {
                            text: qsTr("-")
                        }
                        TimeButton {
                            id: buttonTimeSet3End
                            hhModel: timeset3endhhModel
                            mmModel: timeset3endmmModel
                        }
                    }
                }
            }
            GroupBox {
                id: forTesting
                Layout.fillWidth: true
                GridLayout {
                    anchors.fill: parent
                    columns: 2

                    Label {
                        id: labelTimeLaps

                        text: "Time lapse"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    Switch {
                        id: switchTimeLapse


                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        //Layout.fillWidth: true

                    }

                    Label {
                        id: labelMessage

                        text: "3G/GSM notifications"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    Switch {
                        id: switchMessage


                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        //Layout.fillWidth: true

                    }
                }
            }

            GroupBox {
                title: " "
                visible: false
                Layout.fillWidth: true
                GridLayout {
                    anchors.fill: parent
                    columns: 2

                    Label {
                        id: labelBattery
                        text: "Batt."
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxBattery
                        currentIndex: uvloModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: uvloModel
                    }

                    Label {
                        id: labelTimerEvent
                        text: "Timer event"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxTimerEvent
                        currentIndex: pauseModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: pauseModel
                    }

                    Label {
                        id: labelTimeRadioWakeup
                        text: "Wakeup"
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    ComboBox {
                        id: comboBoxTimeRadioWakeup
                        currentIndex: timeradiowakeupModel.currentIndex
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Layout.fillWidth: true
                        model: timeradiowakeupModel
                    }
                }
            }
            //} //flow
        }
    }
}
